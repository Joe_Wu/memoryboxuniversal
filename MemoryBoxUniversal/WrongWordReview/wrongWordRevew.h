//
//  wrongWordRevew.h
//  MemoryBoxRed
//
//  Created by YangJoe on 4/27/14.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Word.h"
#import "MBProgressHUD.h"

@interface wrongWordRevew : UIViewController<AVAudioPlayerDelegate,MBProgressHUDDelegate>
{
    NSMutableArray *DayWordArray;
    Word *GlobalWord;
    NSMutableArray *viewArray;
    UIView *OneViewCache;
    UIImageView *processing;
    int keyBoardMargin_;// move the keybord
    AVAudioPlayer *avAudioPlayer;
    MBProgressHUD *_progressHUD;
    UIImageView *bigImage;// used for type D
    int count;
    
    UIView *BShowAllView;
}

//- (IBAction)Back:(id)sender;
//- (IBAction)Recite:(id)sender;
- (IBAction)Back:(id)sender;
- (IBAction)Recite:(id)sender;
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end
