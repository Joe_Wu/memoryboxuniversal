//
//  ViewController.m
//  MemoryBoxUniversal
//
//  Created by YangJoe on 14-5-16.
//  Copyright (c) 2014年 YangJoe. All rights reserved.
//

#import "ViewController.h"
#import "LoginView.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize pageScroll;
@synthesize pageControl;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    pageControl.numberOfPages = 3;
    pageControl.currentPage = 0;
    pageScroll.delegate = self;
    
    // judge used in iphone or ipad
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        pageScroll.contentSize = CGSizeMake(self.view.frame.size.height * 3, self.view.frame.size.width);
    }
    else
    {
        pageScroll.contentSize = CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height);
    }
    
    UIColor* buttonColor = [UIColor colorWithRed:222.0/255 green:59.0/255 blue:47.0/255 alpha:1.0f];
    
    self.StartBut.backgroundColor = buttonColor;
    self.StartBut.layer.cornerRadius = 3.0f;
    self.StartBut.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:15.0f];
    [self.StartBut setTitle:@"开始使用" forState:UIControlStateNormal];
    [self.StartBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.StartBut setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.view.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)StartBut:(UIButton *)sender {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstLaunch"];
    
    LoginView* menuVc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
    menuVc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;	// 淡入淡出.
    [self presentModalViewController:menuVc animated:YES];
}
@end
