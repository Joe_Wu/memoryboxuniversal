//
//  DataBase.h
//  MemoryBox
//
//  Created by firefly on 13-6-24.
//
//
#define DATA_BASE_NAME    @"db.sqlite"

#import <Foundation/Foundation.h>
#import "Word.h"
#import "sqlite3.h"
#import "UserDictionary.h"


@interface DataBase : NSObject
{
    sqlite3 *database;

}

- (NSMutableArray *)getNewWord:(NSString *)userId wordCount:(NSString *)count;//从所有词库中获取新单词，25个，wordCount为要返回的单词数量

- (NSMutableArray *)getOldWord:(NSString * )uid;//根据词库获取旧单词，wordCount为要返回的单词数量

- (NSMutableArray *)getNewWord:(NSString *)userId wordCount:(NSString *)count Dictionarys:(NSArray *)dics;//Array 里面放 UserDictionary

- (NSMutableArray *)getOldWord:(NSString * )uid Dictionarys:(NSArray *)dics;//Array 里面放 UserDictionary

- (NSString *)getAllWordCount;//获取所有单词个单数

- (void)wordDayPlus:(NSString *)wordId howManyPlus:(NSString *)manyPlus userId:(NSString *)uid;//将word的day加

- (NSString *)getWordDay:(NSString *)wordId userId:(NSString *)uid;//获取单词day

- (void)wordDelayPlus:(NSString *)wordId howManyPlus:(NSString *)plus userId:(NSString *)uid;//将单词的delay在原本的数值上加

- (NSString *)getWordDelay:(NSString *)wordId userId:(NSString *)uid;//获取某个单词的DELAY

- (void)setWordNotEqualZeroPlusNumber:(NSString *)userId PlusNumber:(NSString *)plus;//给所有单词的wordday不等于0的增加

- (NSString *)getCompleteTime:(UserDictionary *)dictionary;//获取完成时间

- (void)setDictionaryCompleteTime:(NSString *)howManyTime dictioanry:(UserDictionary *)dic;//直接设置完成时间

- (void)dictionaryAddCompleteTime:(NSString *)howManyTime dictionary:(UserDictionary *)dic;//给词库增加完成时间

- (void)dictionarySubCompleteTime:(NSString *)howManyTime dictionary:(UserDictionary *)dic;//给词库减少完成时间

- (NSString *)getDictionaryWordCount:(UserDictionary *)dictioanry;//获取词库总数

- (void)setDictionaryPronounce:(UserDictionary *)dictionary whatPronounce:(NSString *)pronounce;//设置词库的发音
//发音
- (NSString *)getDictionaryPronounce:(UserDictionary *)dictionary;//获取词库的发音

- (NSString *)getCurrentDictionaryPronounce:(UserDictionary *)dictionary;//获取词库当前已经切换的发音 美音返回 US，英音 UK

- (NSString *)switchCurrentDictionaryPronounce:(UserDictionary *)dictionary;//切换词库发音

@end
