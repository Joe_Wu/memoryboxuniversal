//
//  wrongWords.m
//  MemoryBox
//
//  Created by appstone on 13-6-30.
//
//

#import "wrongWords.h"

@implementation wrongWords

- (id) initWithCoder: (NSCoder *)coder {
    if (self = [super init]){
        self.randomTag = [coder decodeObjectForKey:@"randomTag"];
        self.answare = [coder decodeObjectForKey:@"answare"];
        self.myword = [coder decodeObjectForKey:@"myword"];
    }
    return self;
}

- (void) encodeWithCoder: (NSCoder *)coder  {
    [coder encodeObject:self.answare forKey:@"randomTag"];
    [coder encodeObject:self.answare forKey:@"answare"];
    [coder encodeObject:self.myword forKey:@"myword"];
}
@end
