//
//  User.h
//  MemoryBox
//
//  Created by firefly on 13-6-22.
//
//

#import <Foundation/Foundation.h>

@interface User : NSObject
{
    
}
@property (nonatomic, retain) NSString *user_id;
@property (nonatomic, retain) NSString *user_name;
@property (nonatomic, retain) NSString *user_password;
@property (nonatomic, retain) NSString *user_email;
@property (nonatomic, retain) NSString *user_total;
@property (nonatomic, retain) NSString *user_day_rember_count;
@property (nonatomic, retain) NSString *user_day_rember_count_test ;
@property (nonatomic, retain) NSString *user_rembe_time;
//初始化
-(id)initWithUserName:(NSString *)userName;
//修改用户日背诵词汇
-(BOOL)setDay_rember_count:(NSString *)user_day_rember_count;
//修改用户积分
-(BOOL)setTotal:(NSString *)user_total;

-(BOOL)cutTotal:(NSString *)user_total;

//修改用户背诵时间
-(BOOL)setRembe_time:(NSString *)user_rembe_time;

//纪录下背诵过的单词并且同步到服务器
- (BOOL)setUserWordRecordToService:(NSString*)wid wordDay:(NSString *)wd;
- (BOOL)findPassword;
@end
