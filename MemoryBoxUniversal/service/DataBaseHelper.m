//
//  DataBaseHelper.m
//  MemoryBox
//
//  Created by firefly on 13-6-14.
//
//

#import "DataBaseHelper.h"

#import "UserDictionary.h"
#import "ServiceApi.h"
#import "UserDictionary.h"
@implementation DataBaseHelper

//得到用户词库进度信息
- (id)init{
    DB *database = [DB getDB];
    db = database.db;
    return self;
}
- (void)getUserWordRecordFromService:(NSString *)userId{
    NSString *url = [NSString stringWithFormat:@"%@getUserRecord/user_id/%@",ApiUrl,userId];
    NSDictionary *data = [ServiceApi getJSON:url];
    //    NSString *state = [data objectForKey:@"state"];
    NSString *state = [NSString stringWithFormat:@"%@",data];
    if([state compare:@"0"]==NSOrderedSame){
        return;
    }
    data = [data objectForKey:@"data"];
    [self openDB];
    sqlite3_exec(db,"BEGIN TRANSACTION",0,0,0);
    for (NSDictionary *word in data) {
        NSString *word_id = [word objectForKey:@"word_id"];
        NSString *word_day = [word objectForKey:@"word_day"];
        NSString *sql = [NSString stringWithFormat:@"INSERT INTO 'word_day' ('word_id','user_id','word_day') VALUES (%@,%@,%@)",word_id,userId,word_day];
        [self execSql:sql];
    }
    sqlite3_exec(db,"COMMIT",0,0,nil); //COMMIT
    [self close];
}

- (void)unPauseDictionary:(NSString *)dictionaryID userId:(NSString *)uid{
    NSString *sql = [NSString stringWithFormat:@"UPDATE 'have_buy_dictionary' SET 'is_stop' =0 where have_buy_dictionary_user_id=%@ and have_buy_dictionary_dictionary=%@",uid,dictionaryID];
    [self openDB];
    [self execSql:sql];
    [self close];
}//暂停某个用户的词库

- (void)pauseDictionary:(NSString *)dictionaryID userId:(NSString *)uid{
    NSString *sql = [NSString stringWithFormat:@"UPDATE 'have_buy_dictionary' SET 'is_stop' =1 where have_buy_dictionary_user_id=%@ and have_buy_dictionary_dictionary=%@",uid,dictionaryID];
    [self openDB];
    [self execSql:sql];
    [self close];
}//暂停某个用户的词库

- (BOOL)isDictionaryPause:(NSString *)dictionaryId userId:(NSString *)uid{
    [self openDB];
    
    NSString *selectSql = [NSString stringWithFormat:@"SELECT is_stop FROM have_buy_dictionary where have_buy_dictionary_user_id=%@ and have_buy_dictionary_dictionary=%@",uid,dictionaryId];
    sqlite3_stmt * statement;
    NSString *nsIdStr = nil;
    if (sqlite3_prepare_v2(db, [selectSql UTF8String], -1, &statement, nil) == SQLITE_OK) {
        
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *id = (char*)sqlite3_column_text(statement, 0);
            nsIdStr = [[NSString alloc]initWithUTF8String:id];
        }
        
    }else{
        sqlite3_finalize(statement);
        return NO;
    }
    
    sqlite3_finalize(statement);
    
    if([nsIdStr compare:@"1"] == NSOrderedSame){
        return YES;
    }else{
        return NO;
    }
    
}

- (float)getDictionaryMemoryPercent:(NSString *)dictionaryId userId:(NSString *)uid{
    float allCount = (float)[self getDictionaryCount:dictionaryId userId:uid];
    float memory = (float)[self getWordDayNotEqualZero:dictionaryId userId:uid ];
    float percent = memory/allCount;
    percent = (float)((int)(percent * 1000)) / 1000; //取小数点前两位
    return percent*100;
}

- (int)getDictionaryCompletedWords:(NSString *)dictionary_id userId:(NSString *)uid{
    int allCount = (int)[self getDictionaryCount:dictionary_id userId:uid];
    int memory = (int)[self getWordDayNotEqualZero:dictionary_id userId:uid ];
    return allCount-memory;
}

- (int)getDictionaryAllWordCount:(NSString *)dictionary_id userId:(NSString *)uid{
    
    int allCount = (float)[self getDictionaryCount:dictionary_id userId:uid];
    return allCount;
    
}//获取词库单词总数


- (int)getDictionaryCount:(NSString *)dictionaryId userId:(NSString *)uid{
    NSString * SQL = [NSString stringWithFormat:@"SELECT count(*) FROM dictionary d join dictionary_with_word dw on d.dictionary_id=dw.dictionary_id join word_day wd on wd.word_id=dw.word_id where d.dictionary_id=%@ and user_id=%@",dictionaryId,uid];
    [self openDB];
    int count=0;
    sqlite3_stmt * statement;
    if (sqlite3_prepare_v2(db, [SQL UTF8String], -1, &statement, nil) == SQLITE_OK) {
        
        while (sqlite3_step(statement) == SQLITE_ROW) {
            count = sqlite3_column_int(statement, 0);
        }
        
    }
    
    sqlite3_finalize(statement);
    
    return count;
}//获取词库所有单词总数

-(int)getWordDayNotEqualZero:(NSString *)dictionaryId userId:(NSString *)uid{
    NSString * SQL = [NSString stringWithFormat:@"SELECT count(*) FROM dictionary d join dictionary_with_word dw on d.dictionary_id=dw.dictionary_id join word_day wd on wd.word_id=dw.word_id where d.dictionary_id=%@ and user_id=%@ and wd.word_day!=0",dictionaryId,uid];
    [self openDB];
    int count=0;
    sqlite3_stmt * statement;
    if (sqlite3_prepare_v2(db, [SQL UTF8String], -1, &statement, nil) == SQLITE_OK) {
        
        while (sqlite3_step(statement) == SQLITE_ROW) {
            count = sqlite3_column_int(statement, 0);
        }
        
    }
    
    sqlite3_finalize(statement);
    
    return count;
    
}//获取word_day不等于0，也就是说已经背诵过的单词数量

- (BOOL)isDictionaryExist:(NSString *)dictionaryId{
    [self openDB];
    
    NSString *selectSql = [NSString stringWithFormat:@"SELECT * FROM dictionary where dictionary_id='%@'",dictionaryId];
    sqlite3_stmt * statement;
    NSString *nsIdStr = nil;
    if (sqlite3_prepare_v2(db, [selectSql UTF8String], -1, &statement, nil) == SQLITE_OK) {
        
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *id = (char*)sqlite3_column_text(statement, 0);
            nsIdStr = [[NSString alloc]initWithUTF8String:id];
        }
        
    }else{
        return NO;
    }
    
    if(nsIdStr!=nil){
        return YES;
    }else{
        return NO;
    }
    
}
- (void)deleteDictionaryInDB:(NSString *)dictionaryId{
    [self openDB];
    NSString *deleteDictionarySql = [NSString stringWithFormat:@"delete from dictionary where dictionary_id=%@",dictionaryId];
    NSString *deleteRelationShip = [NSString stringWithFormat:@"delete from dictionary_with_word where dictionary_id=%@",dictionaryId];
    
    [self execSql:deleteDictionarySql];
    [self execSql:deleteRelationShip];
    
    
    
}//删除数据库中的词库数据

- (BOOL)openDB{
    
    return YES;
}

- (BOOL)execSql:(NSString *)sql{
    
    char *err;
    while (YES) {
        if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {
            if(strcmp(sqlite3_errmsg(db), "database is locked")==0){
                NSLog(@"数据库操作数据失败 sleep1!:%s SQL:%@",sqlite3_errmsg(db),sql);
                
                sleep(0.3);
                continue;
            }else{
                NSLog(@"数据库操作数据失败!:%s SQL:%@",sqlite3_errmsg(db),sql);
                break;
            }
            break;
        }else{
            break;
        }
    }
    
    NSLog(@"数据库操作数据成!");
    return YES;
}
- (BOOL)execSql_Bind:(char *)sql{
    char *err;
    if (sqlite3_exec(db, sql, NULL, NULL, &err) != SQLITE_OK) {
        NSLog(@"数据库操作数据失败!:%s SQL:%s",sqlite3_errmsg(db),sql);
        return NO;
    }
    NSLog(@"数据库操作数据成!");
    return YES;
}
- (BOOL)close{
    
    return YES;
}

- (BOOL)initDictoinaryTable{
    NSString *sql = @"create table dictionary_type (dictionary_type_id   INTEGER        unique             not null,dictionary_type_name VARCHAR(50)      unique,  primary key (dictionary_type_id));create table dictionary (dictionary_id        INTEGER     unique                       not null,dictionary_type_id   INTEGER,dictionary_name      VARCHAR(50)      unique,  dictionary_abc       INTEGER,dictionary_has_voice SMALLINT,  dictionary_time      INTEGER,dictionary_has_stick SMALLINT,dictionary_price decimal(10,2),dictionary_pronounce tinyint(4)  ,dictionary_complete_time tinyint(4),count INTEGER default 0,current_pronounce tinyint(4) default 0,primary key (dictionary_id),foreign key (dictionary_type_id)      references dictionary_type (dictionary_type_id));create  index Relationship_1_FK on dictionary (dictionary_type_id ASC);create table word (word_id              INTEGER             unique          not null,word_word            VARCHAR(50)           ,       word_symbol          VARCHAR(50),word_voice           VARCHAR(100),word_pircture1       VARCHAR(100),word_pircture2       VARCHAR(100),word_word_class      VARCHAR(50),word_statement       VARCHAR(300),word_cn_statement    VARCHAR(300),word_a_explan1       VARCHAR(50),word_a_explan2       VARCHAR(50),word_a_explan3       VARCHAR(50),word_b_explana       VARCHAR(50),word_b_explanb       VARCHAR(50),word_b_explanc       VARCHAR(50),word_b_expland       VARCHAR(50),word_b_explane       VARCHAR(50),word_b_right         VARCHAR(50),word_c_explan        VARCHAR(50),word_c_answer        VARCHAR(50),word_d_picture1        VARCHAR(50),word_d_picture2    VARCHAR(400),word_analyze    TEXT,primary key (word_id));CREATE  TABLE 'dictionary_with_word' ('dictionary_with_word' INTEGER PRIMARY KEY  NOT NULL , 'dictionary_id' INTEGER NOT NULL , 'word_id' INTEGER NOT NULL );create table 'user' (user_id              INTEGER          unique             not null,user_name            VARCHAR(50)   unique,    user_password        INTEGER,user_email           VARCHAR(50)   unique,     user_total           INTEGER,user_day_rember_count INTEGER,user_day_rember_count_test INTEGER,user_rembe_time      INTEGER,primary key (user_id));create table have_buy_dictionary (have_buy_dictionary_user_id INTEGER,have_buy_dictionary_dictionary INTEGER,is_stop integer default 1,foreign key (have_buy_dictionary_user_id)      references 'user' (user_id));create  index Relationship_6_FK on have_buy_dictionary (have_buy_dictionary_user_id ASC);create table rember_record (rember_record_id     INTEGER          unique                   not null,rember_record_word_id INTEGER,rember_record_rember_count INTEGER,primary key (rember_record_id),foreign key (rember_record_id)      references 'user' (user_id));create  index Relationship_5_FK on rember_record (rember_record_id ASC);create trigger fk_Word_Delete before delete on dictionary_with_word for each row begin  delete from word where word_id = old.word_id;end ;CREATE TABLE complete_word(word_id   INTEGER        unique             not null,word_word  VARCHAR(50)      unique  not null,primary key (word_id));create table word_day (word_day_id          INTEGER       unique                 not null,word_id              INTEGER,user_id              INTEGER,word_day             INTEGER default 0,word_delay       INTEGER default 0,primary key (word_day_id)); CREATE TRIGGER insert_have_buy_dictionary BEFORE  INSERT ON have_buy_dictionary FOR EACH ROW BEGIN select raise(rollback,'the name hava already exists') where (select have_buy_dictionary_dictionary from have_buy_dictionary where         new.have_buy_dictionary_dictionary=have_buy_dictionary_dictionary and  new.have_buy_dictionary_user_id=have_buy_dictionary_user_id ); END;CREATE TRIGGER delete_word_and_delete_word_day before delete on word for each row begin  delete from word_day where word_id = old.word_id;end;";
    [self execSql:sql];
    return YES;
}
- (BOOL)importWordXMLToDB:(UserDictionary *)dictionary userId:(NSString *)uid{
    
    NSDictionary *words = [ServiceApi readLocalWordJSON:dictionary.dictionary_id];
    [self insertDictionaryType:dictionary.dictionary_type_name TypeId:dictionary.dictionary_type_id];//插入词库类型
    [self insertDictionary:dictionary.dictionary_name dicId:dictionary.dictionary_id dicTypeId:dictionary.dictionary_type_id dicAbc: dictionary.dictionary_abc dicHasVoice:dictionary.dictionary_has_voice dicTime:dictionary.dictionary_time dicStick:dictionary.dictionary_has_stick dictionary_price:dictionary.dictionary_price dictionary_pronounce:dictionary.dictionary_pronounce dictionary_complete_time:dictionary.dictionary_complete_time count:dictionary.count];//插入词库
    NSInteger count = [words count];
    int pointCount=1;
    NSMutableString *dictionary_with_word =[[NSMutableString alloc ]initWithString:@"INSERT INTO 'dictionary_with_word' ('dictionary_id','word_id') VALUES"];
    NSMutableString *insertWordDay = [[NSMutableString alloc ]initWithString:@"INSERT INTO 'word_day' ('word_id','user_id') VALUES"];
    sqlite3_stmt *stmt;
    sqlite3_exec(db, "BEGIN EXCLUSIVE TRANSACTION", 0, 0, 0);
    for (NSArray *word in words) {
        
        NSString *word_id = [word objectAtIndex:0];
        NSString *word_word = [word objectAtIndex:1];
        NSString *word_symbol = [word objectAtIndex:2];
        NSString *word_word_class = [word objectAtIndex:6];
        NSString *word_statement = [word objectAtIndex:7];
        NSString *word_cn_statement = [word objectAtIndex:8];
        NSString *word_a_explain1 = [word objectAtIndex:9];
        NSString *word_a_explain2 = [word objectAtIndex:10];
        NSString *word_a_explain3 = [word objectAtIndex:11];
        NSString *word_b_explaina = [word objectAtIndex:12];
        NSString *word_b_explainb = [word objectAtIndex:13];
        NSString *word_b_explainc = [word objectAtIndex:14];
        NSString *word_b_explaind = [word objectAtIndex:15];
        NSString *word_b_explaine = [word objectAtIndex:16];
        NSString *word_b_right = [word objectAtIndex:17];
        NSString *word_c_explain = [word objectAtIndex:18];
        NSString *word_c_answer = [word objectAtIndex:19];
        NSString *word_analyze = [word objectAtIndex:22];
        
        if (word_analyze == [NSNull null]) {
            word_analyze = @"";
        }
        
        if(word_word == [NSNull null]){
            word_word = @"";
            
        }
        
        if(word_symbol == [NSNull null]){
            word_symbol = @"";
            
        }
        
        if(word_word_class == [NSNull null]){
            word_word_class = @"";
            
        }
        
        if(word_statement == [NSNull null]){
            word_statement = @"";
            
        }
        
        if(word_cn_statement == [NSNull null]){
            word_cn_statement = @"";
            
        }
        
        if(word_a_explain1 == [NSNull null]){
            word_a_explain1 = @"";
            
        }
        
        if(word_a_explain2 == [NSNull null]){
            word_a_explain2 = @"";
            
        }
        
        if(word_a_explain3 == [NSNull null]){
            word_a_explain3 = @"";
            
        }
        
        if(word_b_explaina == [NSNull null]){
            word_b_explaina = @"";
            
        }
        
        if(word_b_explainb == [NSNull null]){
            word_b_explainb = @"";
            
        }
        
        if(word_b_explainc == [NSNull null]){
            word_b_explainc = @"";
            
        }
        
        if(word_b_explaind == [NSNull null]){
            word_b_explaind = @"";
            
        }
        
        if(word_b_explaine == [NSNull null]){
            word_b_explaine = @"";
            
        }
        
        if(word_b_right == [NSNull null]){
            word_b_right = @"";
            
        }
        
        if(word_c_explain == [NSNull null]){
            word_c_explain = @"";
            
        }
        
        if(word_c_answer == [NSNull null]){
            word_c_answer = @"";
            
        }
        
        
        
        
        char *sql = "INSERT INTO 'word'('word_id','word_word','word_symbol','word_word_class','word_statement','word_cn_statement','word_a_explan1','word_a_explan2','word_a_explan3','word_b_explana',     'word_b_explanb',       'word_b_explanc','word_b_expland',       'word_b_explane',       'word_b_right', 'word_c_explan','word_c_answer','word_analyze')VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL)== SQLITE_OK) {
            sqlite3_bind_text(stmt, 1, [word_id UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 2, [word_word UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 3, [word_symbol UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 4, [word_word_class UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 5, [word_statement UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 6, [word_cn_statement UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 7, [word_a_explain1 UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 8, [word_a_explain2 UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 9, [word_a_explain3 UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 10, [word_b_explaina UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 11, [word_b_explainb UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 12, [word_b_explainc UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 13, [word_b_explaind UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 14, [word_b_explaine UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 15, [word_b_right UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 16, [word_c_explain UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 17, [word_c_answer UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 18, [word_analyze UTF8String], -1, SQLITE_TRANSIENT);
            
            
            if(pointCount%499==0){
                [dictionary_with_word appendFormat:@"(%@,%@)",dictionary.dictionary_id,word_id];
                [insertWordDay appendFormat:@"(%@,%@)",word_id,uid];
                
                [self execSql:dictionary_with_word];
                [self execSql:insertWordDay];
                
                dictionary_with_word =[[NSMutableString alloc ]initWithString:@"INSERT INTO 'dictionary_with_word' ('dictionary_id','word_id') VALUES"];
                insertWordDay = [[NSMutableString alloc ]initWithString:@"INSERT INTO 'word_day' ('word_id','user_id') VALUES"];
            }
            
            if(pointCount==count){
                [dictionary_with_word appendFormat:@"(%@,%@)",dictionary.dictionary_id,word_id];
                [insertWordDay appendFormat:@"(%@,%@)",word_id,uid];
            }else{
                [dictionary_with_word appendFormat:@"(%@,%@),",dictionary.dictionary_id,word_id];
                [insertWordDay appendFormat:@"(%@,%@),",word_id,uid];
            }
            
            
            pointCount++;
            
        }
        
        if (sqlite3_step(stmt) != SQLITE_DONE){
            NSLog(@"Something is Wrong!:%s",sqlite3_errmsg(db));
        }
    }
    [self execSql:dictionary_with_word];
    [self execSql:insertWordDay];
    
    if (sqlite3_exec(db, "COMMIT TRANSACTION", 0, 0, 0) != SQLITE_OK) NSLog(@"SQL Error: %s",sqlite3_errmsg(db));
    sqlite3_finalize(stmt);
    
    return YES;
}
- (BOOL)importWordXMLToDB2:(UserDictionary *)dictionary userId:(NSString *)uid {
    [self openDB];
    
    ServiceApi *api = [[ServiceApi alloc]init];
    
    [self insertDictionaryType:dictionary.dictionary_type_name TypeId:dictionary.dictionary_type_id];//插入词库类型
    [self insertDictionary:dictionary.dictionary_name dicId:dictionary.dictionary_id dicTypeId:dictionary.dictionary_type_id dicAbc: dictionary.dictionary_abc dicHasVoice:dictionary.dictionary_has_voice dicTime:dictionary.dictionary_time dicStick:dictionary.dictionary_has_stick dictionary_price:dictionary.dictionary_price dictionary_pronounce:dictionary.dictionary_pronounce dictionary_complete_time:dictionary.dictionary_complete_time count:dictionary.count];//插入词库
    
    NSMutableArray *words = [api readLocalWordXml:dictionary.dictionary_id];
    
    NSInteger count = [words count];
    int pointCount=1;
    NSMutableString *dictionary_with_word =[[NSMutableString alloc ]initWithString:@"INSERT INTO 'dictionary_with_word' ('dictionary_id','word_id') VALUES"];
    NSMutableString *insertWordDay = [[NSMutableString alloc ]initWithString:@"INSERT INTO 'word_day' ('word_id','user_id') VALUES"];
    sqlite3_stmt *stmt;
    sqlite3_exec(db, "BEGIN EXCLUSIVE TRANSACTION", 0, 0, 0);
    for(NSMutableDictionary *word in words){
        
        NSString *word_id = [word objectForKey:@"word_id"];
        NSString *word_word = [word objectForKey:@"word_word"];
        NSString *word_symbol = [word objectForKey:@"word_symbol"];
        NSString *word_word_class = [word objectForKey:@"word_word_class"];
        NSString *word_statement = [word objectForKey:@"word_statement"];
        NSString *word_cn_statement = [word objectForKey:@"word_cn_statement"];
        NSString *word_a_explain1 = [word objectForKey:@"word_a_explain1"];
        NSString *word_a_explain2 = [word objectForKey:@"word_a_explain2"];
        NSString *word_a_explain3 = [word objectForKey:@"word_a_explain3"];
        NSString *word_b_explaina = [word objectForKey:@"word_b_explaina"];
        NSString *word_b_explainb = [word objectForKey:@"word_b_explainb"];
        NSString *word_b_explainc = [word objectForKey:@"word_b_explainc"];
        NSString *word_b_explaind = [word objectForKey:@"word_b_explaind"];
        NSString *word_b_explaine = [word objectForKey:@"word_b_explaine"];
        NSString *word_b_right = [word objectForKey:@"word_b_right"];
        NSString *word_c_explain = [word objectForKey:@"word_c_explain"];
        NSString *word_c_answer = [word objectForKey:@"word_c_answer"];
        
        
        
        char *sql = "INSERT INTO 'word'('word_id','word_word','word_symbol','word_word_class','word_statement','word_cn_statement','word_a_explan1','word_a_explan2','word_a_explan3','word_b_explana',     'word_b_explanb',       'word_b_explanc','word_b_expland',       'word_b_explane',       'word_b_right', 'word_c_explan','word_c_answer')VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL)== SQLITE_OK) {
            sqlite3_bind_text(stmt, 1, [word_id UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 2, [word_word UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 3, [word_symbol UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 4, [word_word_class UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 5, [word_statement UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 6, [word_cn_statement UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 7, [word_a_explain1 UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 8, [word_a_explain2 UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 9, [word_a_explain3 UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 10, [word_b_explaina UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 11, [word_b_explainb UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 12, [word_b_explainc UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 13, [word_b_explaind UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 14, [word_b_explaine UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 15, [word_b_right UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 16, [word_c_explain UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 17, [word_c_answer UTF8String], -1, SQLITE_TRANSIENT);
            
            
            if(pointCount%499==0){
                [dictionary_with_word appendFormat:@"(%@,%@)",dictionary.dictionary_id,word_id];
                [insertWordDay appendFormat:@"(%@,%@)",word_id,uid];
                
                [self execSql:dictionary_with_word];
                [self execSql:insertWordDay];
                
                dictionary_with_word =[[NSMutableString alloc ]initWithString:@"INSERT INTO 'dictionary_with_word' ('dictionary_id','word_id') VALUES"];
                insertWordDay = [[NSMutableString alloc ]initWithString:@"INSERT INTO 'word_day' ('word_id','user_id') VALUES"];
            }
            
            if(pointCount==count){
                [dictionary_with_word appendFormat:@"(%@,%@)",dictionary.dictionary_id,word_id];
                [insertWordDay appendFormat:@"(%@,%@)",word_id,uid];
            }else{
                [dictionary_with_word appendFormat:@"(%@,%@),",dictionary.dictionary_id,word_id];
                [insertWordDay appendFormat:@"(%@,%@),",word_id,uid];
            }
            
            
            pointCount++;
            
        }
        
        if (sqlite3_step(stmt) != SQLITE_DONE){
            NSLog(@"Something is Wrong!:%s",sqlite3_errmsg(db));
        }
        
    }
    [self execSql:dictionary_with_word];
    [self execSql:insertWordDay];
    
    if (sqlite3_exec(db, "COMMIT TRANSACTION", 0, 0, 0) != SQLITE_OK) NSLog(@"SQL Error: %s",sqlite3_errmsg(db));
    sqlite3_finalize(stmt);
    
    [self close];
    
    [api release];
    return YES;
}
-(void)insertDictionary:(NSString *)dicName dicId:(NSString *)dicid dicTypeId:(NSString *)typeId dicAbc:(NSString *)abc dicHasVoice:(NSString *)voice dicTime:(NSString *)time dicStick:(NSString *)stick dictionary_price:(NSString *)price dictionary_pronounce:(NSString *)pronounce dictionary_complete_time:(NSString *)complete count:(NSString *)count{
    
    
    NSString *sql = [NSString stringWithFormat:@"INSERT INTO 'dictionary' ('dictionary_id','dictionary_type_id','dictionary_name','dictionary_abc','dictionary_has_voice',  'dictionary_time','dictionary_has_stick','dictionary_price','dictionary_pronounce','dictionary_complete_time','count') VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",dicid,typeId,dicName,abc,voice,time,stick,price,pronounce,complete,count];
    [self execSql:sql];//把词库插入数据库
    
}

-(void)insertDictionaryType:(NSString *)TypeName TypeId:(NSString *)typeId{
    
    
    NSString *sql = [NSString stringWithFormat:@"INSERT INTO 'dictionary_type' ('dictionary_type_id','dictionary_type_name') VALUES (%@,'%@')",typeId,TypeName];
    [self execSql:sql];//把词库插入数据库
    
    
}

- (BOOL)sync:(NSString *)user_id{
    [self openDB];
    
    
    NSString * SQL = [NSString stringWithFormat:@"SELECT word_id,word_day FROM word_day where user_id=%@",user_id];
    int count=1;
    
    NSMutableDictionary * data = [NSMutableDictionary dictionary];
    [data setObject:user_id forKey:@"user_id"];
    
    sqlite3_stmt * statement;
    if (sqlite3_prepare_v2(db, [SQL UTF8String], -1, &statement, nil) == SQLITE_OK) {
        
        while (sqlite3_step(statement) == SQLITE_ROW) {
            count++;
            
            int word_id = sqlite3_column_int(statement, 0);
            int word_day = sqlite3_column_int(statement, 0);
            NSNumber *_word_id = [NSNumber numberWithInt:word_id];
            NSNumber *_word_day = [NSNumber numberWithInt:word_day];
            
            NSMutableDictionary *row =  [NSMutableDictionary dictionary];
            
            [row setObject:_word_id forKey:@"word_id"];
            [row setObject:_word_day forKey:@"word_day"];
            NSArray *arr = [NSArray arrayWithObject:row];
            [data setValue:arr forKey:[NSString stringWithFormat:@"%d",count]];
            
            [row release];
        }
    }
    
    sqlite3_finalize(statement);
    [self close];
    
    if (count==1) {
        return NO;
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *tempJsonString = [@"json=" stringByAppendingString:jsonString];
    
    [self postJsonToServer:[tempJsonString dataUsingEncoding:NSUTF8StringEncoding] user_id:user_id];
    [self getJsonOnServer:user_id];
    
    return YES;
}
- (BOOL)postJsonToServer:(NSData *)json user_id:(NSString *)uid{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *url = [NSString stringWithFormat:@"%@/%@",syncUploadUrl,uid];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"gzip,deflate,sdch" forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:@"zh-CN,zh;q=0.8" forHTTPHeaderField:@"Accept-Language:"];
    NSString* len = [NSString stringWithFormat:@"%d", [json length]];
    [request setValue:len forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:json];
    
    //    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    //    NSString *response = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    //    if ([response compare:@"{\"state\":1}"]==NSOrderedSame)
    //        return YES;
    [request release];
    return YES;
}
-(BOOL)getJsonOnServer:(NSString *)uid{
    [self openDB];
    sqlite3_stmt * statement;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *url = [NSString stringWithFormat:@"%@%@",syncDownloadUrl,uid];
    [request setURL:[NSURL URLWithString:url]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"gzip,deflate,sdch" forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:@"zh-CN,zh;q=0.8" forHTTPHeaderField:@"Accept-Language:"];
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    //    NSString *response = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSDictionary *data = [ServiceApi getJSONFromData:returnData];
    
    
    //    sqlite3_exec(db, "BEGIN EXCLUSIVE TRANSACTION", 0, 0, 0);
    
    for (NSDictionary *dic in data) {
        NSString *word_id = [dic objectForKey:@"word_id"];
        NSString *word_day = [dic objectForKey:@"word_day"];
        
        //判断是否已经有了
        NSString *selectSql = [NSString stringWithFormat:@"SELECT count(*) FROM word_day where word_id='%@' and word_id='%@' and user_id='%@'",word_id,word_day,uid];
        NSString *sql = [[NSString alloc]init];
        NSString *nsIdStr = nil;
        if (sqlite3_prepare_v2(db, [selectSql UTF8String], -1, &statement, nil) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                char *id = (char*)sqlite3_column_text(statement, 0);
                nsIdStr = [[NSString alloc]initWithUTF8String:id];
            }
        }
        if ([nsIdStr compare:@"0"]==NSOrderedSame) {
            sql = [NSString stringWithFormat:@"INSERT INTO 'word_day' ('word_id','user_id','word_day') VALUES (%@,%@,%@)",word_id,uid,word_day];
        }else{
            sql = [NSString stringWithFormat:@"UPDATE 'word_day' SET 'word_day' = %@ WHERE  'word_id' = %@ and 'user_id'=%@",word_day,word_id,uid];
        }
        
        [self execSql:sql];
    }
    //    if (sqlite3_exec(db, "COMMIT TRANSACTION", 0, 0, 0) != SQLITE_OK) NSLog(@"SQL Error: %s",sqlite3_errmsg(db));
    
    sqlite3_finalize(statement);
    [self close];
    
    [request release];
    return YES;
}
-(NSString *)getWordDictionary:(NSString *)word_id{
    NSString * SQL = [NSString stringWithFormat:@"SELECT dictionary_name FROM dictionary d join dictionary_with_word dw on d.dictionary_id=dw.dictionary_id join word w on w.word_id=dw.word_id where w.word_id=%@",word_id];
    [self openDB];
    NSString *RETURN = @"";
    sqlite3_stmt * statement;
    if (sqlite3_prepare_v2(db, [SQL UTF8String], -1, &statement, nil) == SQLITE_OK) {
        
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *dic_name = (char*)sqlite3_column_text(statement, 0);

            RETURN =[[NSString alloc]initWithUTF8String:dic_name];
            return  RETURN;
        }
        
    }
    sqlite3_finalize(statement);
    return RETURN;
    
}//获取单词对应的词库名称

@end
