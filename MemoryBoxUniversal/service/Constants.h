//
//  Constants.h
//  MemoryBox
//
//  Created by firefly on 13-6-12.
//
//

#import <Foundation/Foundation.h>


#define SUCCESS = 1;
#define ERROR =0;

/*
 *登陆常量
 */

#define PASSWORD_ERROR 2.2;//密码错误
#define NONAME_ERROR 2.3;//没有此用户

/*
 *注册常量
 */

#define EMAIL_EXIST 3.1;//没有此用户
#define USER_EXIST 3.2;//没有此用户

/*
 *密码修改
 */
#define NAME_OR_PASSWORD_ERRO 4.1;//用户名或者密码存在

/*
 *用户查看词库
 */

#define HAVE_NO_DICTIONARY 5.1;

/*
 *找回密码
 */
#define EMAIL_ERROR 6.1;
#define EMAIL_SEND_ERROR 6.2;

extern NSString const *ApiUrl;//URL
extern NSString const *DownloadUrl;//下载地址
extern NSString const *userInfoUrl;//下载地址
extern NSString const *taobao;//淘宝地址
extern NSString const *syncUploadUrl;//同步地址
extern NSString const *syncDownloadUrl;//同步地址
extern NSString const *BuyUrl;//购买地址
@interface Constants : NSObject

@end
