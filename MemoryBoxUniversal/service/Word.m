//
//  Word.m
//  MemoryBox
//
//  Created by firefly on 13-6-22.
//
//

#import "Word.h"
#import "DataBaseHelper.h"

@implementation Word
-(NSString *)word_pircture1{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    
    
    NSString* picture1 = [path stringByAppendingString:[NSString stringWithFormat:@"/%@/%@_d1.jpg",self.dictionary_id,self.word_word]];
    
    return picture1;
}

-(NSString *)word_pircture2{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    
    
    NSString* picture2 = [path stringByAppendingString:[NSString stringWithFormat:@"/%@/%@_d2.jpg",self.dictionary_id,self.word_word]];
    
    return picture2;
}

-(NSString *)word_us_voice1{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    
    
    NSString* voice = [path stringByAppendingString:[NSString stringWithFormat:@"/%@/%@_us_1.mp3",self.dictionary_id,self.word_id]];
    
    return voice;
}

-(NSString *)word_us_voice2{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    
    
    NSString* voice = [path stringByAppendingString:[NSString stringWithFormat:@"/%@/%@_us_2.mp3",self.dictionary_id,self.word_id]];
    
    return voice;
}

-(NSString *)word_uk_voice1{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    
    
    NSString* voice = [path stringByAppendingString:[NSString stringWithFormat:@"/%@/%@_uk_1.mp3",self.dictionary_id,self.word_id]];
    
    return voice;
}

-(NSString *)word_uk_voice2{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    
    
    NSString* voice = [path stringByAppendingString:[NSString stringWithFormat:@"/%@/%@_uk_2.mp3",self.dictionary_id,self.word_id]];
    
    return voice;
}

-(NSString *)word_d_picture1{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    
    
    NSString* voice = [path stringByAppendingString:[NSString stringWithFormat:@"/%@/%@_d1.jpg",self.dictionary_id,self.word_id]];
    
    return voice;
}

-(NSString *)word_d_picture2{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    
    
    NSString* voice = [path stringByAppendingString:[NSString stringWithFormat:@"/%@/%@_d2.jpg",self.dictionary_id,self.word_id]];
    
    return voice;
}

-(NSString *)word_dictionary{
    DataBaseHelper *dbh = [[DataBaseHelper alloc]init];
    NSString *word_dictionary = [dbh getWordDictionary:[self word_id]];
    
    return word_dictionary;
    [dbh release];
}
- (id) initWithCoder: (NSCoder *)coder {
    if (self = [super init])
    {
        self.word_id = [coder decodeObjectForKey:@"word_id"];
        self.word_word = [coder decodeObjectForKey:@"word_word"];
        self.word_symbol = [coder decodeObjectForKey:@"word_symbol"];

        self.word_us_voice1 = [coder decodeObjectForKey:@"word_us_voice1"];
        self.word_us_voice2 = [coder decodeObjectForKey:@"word_us_voice2"];
        self.word_uk_voice1 = [coder decodeObjectForKey:@"word_uk_voice1"];
        self.word_uk_voice2 = [coder decodeObjectForKey:@"word_uk_voice2"];
        
        self.word_pircture1 = [coder decodeObjectForKey:@"word_pircture1"];
        self.word_pircture2 = [coder decodeObjectForKey:@"word_pircture2"];
        
        self.word_word_class = [coder decodeObjectForKey:@"word_word_class"];
        self.word_statement = [coder decodeObjectForKey:@"word_statement"];
        self.word_cn_statement = [coder decodeObjectForKey:@"word_cn_statement"];
        
        self.word_a_explan1 = [coder decodeObjectForKey:@"word_a_explan1"];
        self.word_a_explan2 = [coder decodeObjectForKey:@"word_a_explan2"];
        self.word_a_explan3 = [coder decodeObjectForKey:@"word_a_explan3"];
        self.word_b_explana = [coder decodeObjectForKey:@"word_b_explana"];
        self.word_b_explanb = [coder decodeObjectForKey:@"word_b_explanb"];
        self.word_b_explanc = [coder decodeObjectForKey:@"word_b_explanc"];
        self.word_b_expland = [coder decodeObjectForKey:@"word_b_expland"];
        self.word_b_explane = [coder decodeObjectForKey:@"word_b_explane"];
        
        self.word_b_right = [coder decodeObjectForKey:@"word_b_right"];
        self.word_c_explan = [coder decodeObjectForKey:@"word_c_explan"];
        self.word_c_answer = [coder decodeObjectForKey:@"word_c_answer"];
        self.word_analyze = [coder decodeObjectForKey:@"word_analyze"];
        
        self.word_d_picture1 = [coder decodeObjectForKey:@"word_d_picture1"];
        self.word_d_picture2 = [coder decodeObjectForKey:@"word_d_picture2"];
        self.word_dictionary = [coder decodeObjectForKey:@"word_dictionary"];
        
        self.abc = [coder decodeObjectForKey:@"abc"];
        self.dictionary_id = [coder decodeObjectForKey:@"dictionary_id"];
    }
    return self;
}

- (void) encodeWithCoder: (NSCoder *)coder  {
    [coder encodeObject:self.word_id forKey:@"word_id"];
    [coder encodeObject:self.word_word forKey:@"word_word"];
    [coder encodeObject:self.word_symbol forKey:@"word_symbol"];
    
    [coder encodeObject:self.word_us_voice1 forKey:@"word_us_voice1"];
    [coder encodeObject:self.word_us_voice2 forKey:@"word_us_voice2"];
    [coder encodeObject:self.word_uk_voice1 forKey:@"word_uk_voice1"];
    [coder encodeObject:self.word_uk_voice2 forKey:@"word_uk_voice2"];
    
    [coder encodeObject:self.word_pircture1 forKey:@"word_pircture1"];
    [coder encodeObject:self.word_pircture2 forKey:@"word_pircture2"];
    
    [coder encodeObject:self.word_word_class forKey:@"word_word_class"];
    [coder encodeObject:self.word_statement forKey:@"word_statement"];
    [coder encodeObject:self.word_cn_statement forKey:@"word_cn_statement"];
    
    [coder encodeObject:self.word_a_explan1 forKey:@"word_a_explan1"];
    [coder encodeObject:self.word_a_explan2 forKey:@"word_a_explan2"];
    [coder encodeObject:self.word_a_explan3 forKey:@"word_a_explan3"];
    [coder encodeObject:self.word_b_explana forKey:@"word_b_explana"];
    [coder encodeObject:self.word_b_explanb forKey:@"word_b_explanb"];
    [coder encodeObject:self.word_b_explanc forKey:@"word_b_explanc"];
    [coder encodeObject:self.word_b_expland forKey:@"word_b_expland"];
    [coder encodeObject:self.word_b_explane forKey:@"word_b_explane"];
    
    [coder encodeObject:self.word_b_right forKey:@"word_b_right"];
    [coder encodeObject:self.word_c_explan forKey:@"word_c_explan"];
    [coder encodeObject:self.word_c_answer forKey:@"word_c_answer"];
    [coder encodeObject:self.word_analyze forKey:@"word_analyze"];
    
    [coder encodeObject:self.word_d_picture1 forKey:@"word_d_picture1"];
    [coder encodeObject:self.word_d_picture2 forKey:@"word_d_picture2"];
    [coder encodeObject:self.word_dictionary forKey:@"word_dictionary"];
    
    [coder encodeObject:self.abc forKey:@"abc"];
    [coder encodeObject:self.dictionary_id forKey:@"dictionary_id"];

}
@end
