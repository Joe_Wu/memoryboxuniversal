//
//  Dictionary.h
//  MemoryBox
//
//  Created by firefly on 13-6-22.
//
//

#import <Foundation/Foundation.h>

@interface Dictionary : NSObject
@property (nonatomic, retain) NSString *dictionary_id;
@property (nonatomic, retain) NSString *dictionary_type_id;
@property (nonatomic, retain) NSString *dictionary_name;
@property (nonatomic, retain) NSString *dictionary_abc;
@property (nonatomic, retain) NSString *dictionary_has_voice;
@property (nonatomic, retain) NSString *dictionary_time;
@property (nonatomic, retain) NSString *dictionary_has_stick;
@property (nonatomic, retain) NSString *buy;
@property (nonatomic, retain) NSString *dictionary_price;
@property (nonatomic, retain) NSString *dictionary_pronounce;
@property (nonatomic, retain) NSString *dictionary_complete_time;

@end
