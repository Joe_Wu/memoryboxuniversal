//
//  ServiceApi.h
//  MemoryBox
//
//  Created by firefly on 13-6-12.
//
//
#import "Constants.h"
#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "ZipArchive.h"
#import "User.h"
#import "UserDictionary.h"
#import "secondList.h"
@interface ServiceApi : NSObject{
    NSArray *ui;
}

@property(nonatomic,retain) ASINetworkQueue *netWorkQueue;
@property(nonatomic,retain)NSArray *downloadArray;
@property(nonatomic,retain)UILabel *label;

/*
 *与服务器交互Api
 */
//- (BOOL)login:(NSString *) userName userPassword:(NSString *)p ;//登陆
//
//- (BOOL)register: (NSString *)userName userEmail:(NSString *)e userPssowrd:(NSString *)p;//注册
//
//- (BOOL)userModifyPassword:(NSString *)userName userPassword:(NSString *)p userModifyPassword:(NSString *)m;

- (NSMutableArray *) getUserHadBuyDictionary:(NSString *)userName userId:(NSString *)uid;//获取用户购买的词库，返回NSMutableArray数组，数组包含UserDictionary对象

+ (NSDictionary *)getJSON:(NSString *)url;//从 URL 获取 JSON
+ (NSDictionary *)getJSONFromData:(NSData *)json;//从 String 获取

//反回的NSMutableArray包含多个 secondlist
- (NSMutableArray *) getUserHadBuyDictionary2SencondList:(NSString *)userName userId:(NSString *)uid;

/*
 *工具方法
 */

- (BOOL)upZip:(NSString *)fileName;//解压，解压文件存放在Document/Dictionary下，文件夹以词典ID命名
- (BOOL)downloadDictionary:(NSString *)dictionaryUrl;//下载,第一个参数是下载的URL，第二个参数是保存zip的名字,下载保存在Document下，以词典ID命名
- (BOOL)downloadDictionary:(NSString *)dictionaryUrl processView:(UIProgressView *)process Label:(UILabel *)label myArray:(NSArray *)ViewArray;//下载,第一个参数是下载的URL，第二个参数是保存zip的名字,下载保存在Document下，以词典ID命名

- (void)removeDictionary:(NSString *)dictionaryId;//删除词典目录

/*
 *下载回调
 */
- (void)requestDone:(ASIHTTPRequest *)request;//下载成功回调函数
- (void)requestWentWrong:(ASIHTTPRequest *)request;//下载失败回调函数

- (NSMutableArray *)readLocalWordXml:(NSString *)id;//把document下以词库ID命名的文件夹下的xml单词数据，返回包含多个NSMutableDictionary的NSMutableArray
+ (NSDictionary *)readLocalWordJSON:(NSString *)id;
//同步

//购买
- (BOOL)buyDictionary:(UserDictionary *)dictionary UserId:(NSString *)user_id;//购买成功返回 YES，反之亦然
@end
