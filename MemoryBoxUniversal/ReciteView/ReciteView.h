//
//  ReciteView.h
//  MemoryBoxRed
//
//  Created by YangJoe on 3/16/14.
//
//

#import <UIKit/UIKit.h>
#import "Word.h"
#import <AVFoundation/AVFoundation.h>
#import "MBProgressHUD.h"

@interface ReciteView : UIViewController<AVAudioPlayerDelegate,MBProgressHUDDelegate,UITextFieldDelegate>{
    NSMutableArray *DayWordArray;
    Word *GlobalWord;
    NSMutableArray *viewArray;
    UIView *OneViewCache;
    UIImageView *processing;
    int keyBoardMargin_;// move the keybord
    AVAudioPlayer *avAudioPlayer;
    MBProgressHUD *_progressHUD;
    UIImageView *bigImage;// used for type D
    
    UIView *BShowAllView;
}

- (IBAction)Back:(UIButton *)sender;
- (IBAction)Recite:(UIButton *)sender;
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end
