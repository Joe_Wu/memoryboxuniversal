//
//  datetool.m
//  MemoryBoxRed
//
//  Created by YangJoe on 3/15/14.
//
//

#import "datetool.h"

@implementation datetool
- (id) initWithCoder: (NSCoder *)coder {
    if (self = [super init]){
        self.loginyear = [coder decodeObjectForKey:@"login_year"];
        self.loginmonth = [coder decodeObjectForKey:@"login_month"];
        self.loginday = [coder decodeObjectForKey:@"login_day"];
    }
    return self;
}

- (void) encodeWithCoder: (NSCoder *)coder  {
    [coder encodeObject:self.loginyear forKey:@"login_year"];
    [coder encodeObject:self.loginmonth forKey:@"login_month"];
    [coder encodeObject:self.loginday forKey:@"login_day"];
}
@end
