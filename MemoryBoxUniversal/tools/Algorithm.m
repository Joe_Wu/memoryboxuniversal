//
//  Algorithm.m
//  MemoryBoxRed
//
//  Created by YangJoe on 3/16/14.
//
//

#import "Algorithm.h"
#import "getWordArray.h"
#import "wordStructure.h"
#import "tools.h"
#import "Init.h"
#import "DataBase.h"
#import "secondList.h"

@implementation Algorithm

// 背诵所有词库的时候初始化20个单词的数组
-(NSMutableArray*)DynamicArray{
    Init *init = [[Init alloc]init];
    NSMutableArray *wordArray = [init GetTodayWords];
    NSMutableArray *ArrayWithTag = [[NSMutableArray alloc]init];
    if ([wordArray count] >= 20) {
        for (int i = 0; i<20; i++) {
            wordStructure *wordTag = [[wordStructure alloc]init];
            NSString *index = [[NSString alloc]initWithFormat:@"%d",i];
            wordTag.wordNum = index;
            NSString *right = [[NSString alloc]initWithFormat:@"%d",0];
            wordTag.right = right;
            wordTag.word = [wordArray objectAtIndex:i];
            [ArrayWithTag addObject:wordTag];
        }
    }else{
        for (int i = 0; i<[wordArray count]; i++) {
            wordStructure *wordTag = [[wordStructure alloc]init];
            NSString *index = [[NSString alloc]initWithFormat:@"%d",i];
            wordTag.wordNum = index;
            NSString *right = [[NSString alloc]initWithFormat:@"%d",0];
            wordTag.right = right;
            wordTag.word = [wordArray objectAtIndex:i];
            [ArrayWithTag addObject:wordTag];
        }
    }
    return ArrayWithTag;
}

// 背诵单个词库的时候初始化20个单词的数组
-(NSMutableArray*)DynamicOneArray{
    tools *tool = [[tools alloc]init];
    
    UserDictionary *dic = [tool indexToDic];
    Init *init = [[Init alloc]init];
    NSMutableArray *wordArray = [init GetTodayWords];
    NSMutableArray *ArrayWithTag = [[NSMutableArray alloc]init];
    for (int i = 0; i < [wordArray count]; i++) {
        Word *word = [wordArray objectAtIndex:i];
        
        if ([word.word_dictionary compare:dic.dictionary_name]==NSOrderedSame) {
            wordStructure *wordTag = [[wordStructure alloc]init];
            NSString *index = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[ArrayWithTag count]];
            wordTag.wordNum = index;
            NSString *right = [[NSString alloc]initWithFormat:@"%d",0];
            wordTag.right = right;
            wordTag.word = word;
            [ArrayWithTag addObject:wordTag];
            if ([ArrayWithTag count] >=20) {
                break;
            }
        }
    }
    return ArrayWithTag;
}

// 正确-背诵同一词库更新单词算法
-(NSMutableArray*)UpdateOneDicArray:(NSMutableArray*)oldArray word:(wordStructure*)wordNum2{
    NSMutableArray *newArray = [[NSMutableArray alloc]init];

    for (wordStructure *word in oldArray) {
        NSLog(@"o->-%@",word.word.word_word);
    }
    // 将数据重新排序
    [oldArray removeObject:wordNum2];
    for (int i = 0; i < [oldArray count]; i++) {
        wordStructure *word = [[wordStructure alloc]init];
        NSString *index = [[NSString alloc]initWithFormat:@"%d",i];
        word.wordNum = index;
        word.right = [[oldArray objectAtIndex:i] right];
        word.word = [[oldArray objectAtIndex:i] word];
        [newArray addObject:word];
    }
    
    // 如果单词的上次正确记录是0,就将单词放到数组的最后
    if ([wordNum2.right compare:@"0"]==NSOrderedSame) {
        wordStructure *newWord = [[wordStructure alloc]init];
        NSString *index = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[newArray count]];
        newWord.wordNum = index;
        newWord.right = @"1";
        newWord.word = wordNum2.word;
        [newArray addObject:newWord];
    }else{
        //如果单词的word_day 为'0' 就将其变为'1'。其他的不做变化
        Algorithm *alg = [[Algorithm alloc]init];
        [alg ChangeTheWordDayToOne:wordNum2.word];
        
        // 在当日需要背诵的缓存数组中删除已完成的单词。
        Init *init = [[Init alloc]init];
        NSMutableArray *oldAllWordArray = [[NSMutableArray alloc]init];
        oldAllWordArray = [init GetTodayWords];
        NSArray *newallArray = [NSArray arrayWithArray:oldAllWordArray];
        for (Word *anObject in newallArray) {
            if ([anObject.word_word compare:wordNum2.word.word_word]==NSOrderedSame) {
                [oldAllWordArray removeObject:anObject];
            }
        }
        [init SaveTodayWords:oldAllWordArray];
        
    // 从当日需要背诵的词库中获得一个合法的新单词。
    for (Word *word in oldAllWordArray) {
        wordStructure *addOneWord = [[wordStructure alloc]init];
        BOOL inArray = false;
        for (wordStructure *struc in newArray) {
            if ([struc.word.word_word compare:word.word_word]==NSOrderedSame) {
                inArray =true;
                break;
            }
        }
        if (!inArray) {
            if([wordNum2.word.word_dictionary compare:word.word_dictionary]==NSOrderedSame){
                inArray =false;
                NSString *index = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[newArray count]];
                addOneWord.wordNum = index;
                NSString *right = [[NSString alloc]initWithFormat:@"%d",0];
                addOneWord.right =right;
                addOneWord.word = word;
                [newArray addObject:addOneWord];
                break;
            }
        }
    }
        
    }
    for (wordStructure *word in newArray) {
        NSLog(@"n->-%@",word.word.word_word);
    }
    return newArray;
}

// 正确-背诵所有词库更新单词算法
-(NSMutableArray*)UpdateAllDicArray:(NSMutableArray*)oldArray word:(wordStructure*)wordNum2{
    NSMutableArray *newArray = [[NSMutableArray alloc]init];

    // 将数据重新排序
    [oldArray removeObject:wordNum2];
    for (int i = 0; i < [oldArray count]; i++) {
        wordStructure *word = [[wordStructure alloc]init];
        NSString *index = [[NSString alloc]initWithFormat:@"%d",i];
        word.wordNum = index;
        word.right = [[oldArray objectAtIndex:i] right];
        word.word = [[oldArray objectAtIndex:i] word];
        [newArray addObject:word];
    }

    // 如果单词的上次正确记录是0
    if ([wordNum2.right compare:@"0"]==NSOrderedSame) {
        wordStructure *newWord = [[wordStructure alloc]init];
        NSString *index = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[newArray count]];
        newWord.wordNum = index;
        newWord.right = @"1";
        newWord.word = wordNum2.word;
        [newArray addObject:newWord];
    }else{
    // 在当日需要背诵的缓存数组中删除已完成的单词。
    Init *init = [[Init alloc]init];
    NSMutableArray *oldAllWordArray = [init GetTodayWords];
    
    for (int i = 0; i < [oldAllWordArray count]; i++) {
        Word *word = [oldAllWordArray objectAtIndex:i];
        if ([word.word_word compare:wordNum2.word.word_word]==NSOrderedSame) {
            [oldAllWordArray removeObject:word];
            break;
        }
    }
    [init SaveTodayWords:oldAllWordArray];
    
    // 从当日需要背诵的词库中获得一个合法的新单词。
    for (Word *word in oldAllWordArray) {
        wordStructure *addOneWord = [[wordStructure alloc]init];
        BOOL inArray = false;
        for (wordStructure *struc in newArray) {
            if ([struc.word.word_word compare:word.word_word]==NSOrderedSame) {
                inArray =true;
                break;
            }
        }
        if (!inArray) {
            inArray =false;
            NSString *index = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[newArray count]];
            addOneWord.wordNum = index;
            NSString *right = [[NSString alloc]initWithFormat:@"%d",0];
            addOneWord.right =right;
            addOneWord.word = word;
            [newArray addObject:addOneWord];
            break;
        }
    }
    }
    return newArray;
}

// 背诵错误后的处理
-(NSMutableArray*)WrongUpdateWordArray:(NSMutableArray*)oldArray word:(wordStructure*)wordNum2{
    NSMutableArray *newArray = [[NSMutableArray alloc]init];
    for (wordStructure *word in oldArray) {
        NSLog(@"o->-%@",word.word.word_word);
    }
    // 将数据重新排序
    [oldArray removeObject:wordNum2];
    
    for (int i = 0; i < [oldArray count]+1; i++) {
        if ([oldArray count] >= 4) {
            if (i < 4) {
                wordStructure *word = [[wordStructure alloc]init];
                NSString *index = [[NSString alloc]initWithFormat:@"%d",i];
                word.wordNum = index;
                word.right = [[oldArray objectAtIndex:i] right];
                word.word = [[oldArray objectAtIndex:i] word];
                [newArray addObject:word];
            }else if(i == 4){
                wordStructure *word = [[wordStructure alloc]init];
                NSString *index = [[NSString alloc]initWithFormat:@"%d",i];
                NSString *right = [[NSString alloc]initWithFormat:@"%d",0];
                word.wordNum = index;
                word.right = right;
                word.word = wordNum2.word;
                [newArray addObject:word];
            }else{
                wordStructure *word = [[wordStructure alloc]init];
                NSString *index = [[NSString alloc]initWithFormat:@"%d",i];
                word.wordNum = index;
                word.right = [[oldArray objectAtIndex:i-1] right];
                word.word = [[oldArray objectAtIndex:i-1] word];
                [newArray addObject:word];
            }
            
        }else{
            if (i == [oldArray count]) {
                wordStructure *word = [[wordStructure alloc]init];
                NSString *index = [[NSString alloc]initWithFormat:@"%d",i];
                word.wordNum = index;
                word.right = wordNum2.right;
                word.word = wordNum2.word;
                [newArray addObject:word];
            }else{
                wordStructure *word = [[wordStructure alloc]init];
                NSString *index = [[NSString alloc]initWithFormat:@"%d",i];
                word.wordNum = index;
                word.right = [[oldArray objectAtIndex:i] right];
                word.word = [[oldArray objectAtIndex:i] word];
                [newArray addObject:word];
            }
        }
    }
    for (wordStructure *word in newArray) {
        NSLog(@"n->-%@",word.word.word_word);
    }
    return newArray;
}

-(float)CountDailyPlan{
    //获得用户信息接口
    tools *tool = [[tools alloc]init];
    NSMutableArray *dics = [tool readArray:@"doubleStruct"];// get saved dictionary structure
    
    float planCount = 0;
    for (int i = 0; i < [dics count]; i++) {
        secondList *list = [dics objectAtIndex:i];
        NSMutableArray *array = list.secondLevel;
        for (int j = 0; j < [array count]; j++) {
            UserDictionary *OneDic = [[UserDictionary alloc]init];
            OneDic = [array objectAtIndex:j];
            
            NSMutableArray *OneArray = [[NSMutableArray alloc]init];
            [OneArray addObject:OneDic];
            
            DataBase *database = [[DataBase alloc]init];
            NSString *days = [database getCompleteTime:OneDic];// 获得完成天数
            if (days.length != 0) {
                NSString *dayWords = [self dayWords:OneDic finishDay:days];
                planCount += [dayWords intValue];
            }
        }
    }
    return planCount;
}
-(NSString*)dayWords:(UserDictionary*) mydic finishDay: (NSString *)finish_day{
    DataBase *wordCount =[[DataBase alloc]init];
    NSString *AllWordscount = [wordCount getDictionaryWordCount:mydic];//得到词库所有单词数量
    int intString = [AllWordscount intValue];
    int finishday = [finish_day intValue];// 完成的天数
    if (finishday == 0) {
        return 0;
    }else{
        if (finishday <= 15) {
            finishday = 15;
        }
        int day = intString/(finishday-14); // 日背诵量 = (总单词数量)/(计划完成的天数-14)
        NSString *days = [NSString stringWithFormat:@"%d",day];
        return days;
    }
    
}

-(void)ChangeTheWordDayToOne:(Word*)word{
    DataBase *database = [[DataBase alloc]init];
    tools *tool = [[tools alloc]init];
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    NSString *days = [database getWordDay:word.word_id userId:user_id];
    NSLog(@"%@",days);
    if ([days compare:@"0"]==NSOrderedSame) {
        NSString *text = [NSString stringWithFormat:@"%@",word.word_id];
        [database wordDayPlus:text howManyPlus:@"1" userId:user_id];
    }
}

-(void)DeleteWordFromArray:(UserDictionary*)dic{
    Init *init = [[Init alloc]init];
    NSMutableArray *oldAllWordArray = [init GetTodayWords];
    NSMutableArray *newAllWordArray = [[NSMutableArray alloc]init];
    for (Word *word in oldAllWordArray) {
        if ([word.word_dictionary compare:dic.dictionary_name]==NSOrderedSame) {
            [newAllWordArray addObject:word];
        }
    }
    [oldAllWordArray removeObjectsInArray:newAllWordArray];
    [init SaveTodayWords:oldAllWordArray];
}

// 有新的词库后就会更新当日背诵词库
-(void)UpdateDailyWordArray:(NSMutableArray*)array{
    // get old word array
    Init *init = [[Init alloc]init];
    NSMutableArray *oldArray = [init GetTodayWords];
    NSMutableArray *newArray = [[NSMutableArray alloc]init];
    newArray = oldArray;
    for (UserDictionary *dic in array) {
        BOOL repeart = false;
        for (Word *word in oldArray) {
            if ([word.word_dictionary compare:dic.dictionary_name]==NSOrderedSame) {
                repeart = true;
                break;
            }
        }
        if (!repeart) {
            NSMutableArray *oneDicArray;
            NSMutableArray *OneArray = [[NSMutableArray alloc]init];
            [OneArray addObject:dic];
            tools *tool = [[tools alloc]init];
            NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
            DataBase *database = [[DataBase alloc]init];
            NSString *days = [database getCompleteTime:dic];// 获得完成天数
            NSString *dayWords = [self dayWords:dic finishDay2:days];
            oneDicArray = [database getNewWord:user_id wordCount:dayWords Dictionarys:OneArray];
            [newArray addObjectsFromArray:oneDicArray];
        }
    }
    [init SaveTodayWords:newArray];
}
-(NSString*)dayWords:(UserDictionary*) mydic finishDay2:(NSString *)finish_day{
    DataBase *wordCount =[[DataBase alloc]init];
    NSString *AllWordscount = [wordCount getDictionaryWordCount:mydic];//得到词库所有单词数量
    int intString = [AllWordscount intValue];
    int finishday = [finish_day intValue];// 完成的天数
    if (finishday == 0) {
        return 0;
    }else{
        if (finishday <= 15) {
            finishday = 15;
        }
        int day = intString/(finishday-14); // 日背诵量 = (总单词数量)/(计划完成的天数-14)
        NSString *days = [NSString stringWithFormat:@"%d",day];
        return days;
    }
    
}

-(float)CountOneDicProgress:(NSMutableArray*)array word:(Word*)word{
    
    float result;
    
    // 1 获得今天应该背诵的单词个数
    NSString *planSize = [[NSUserDefaults standardUserDefaults] stringForKey:@"plansize"];
    int planCount = [planSize intValue];
    
    // 2 获得现在还剩下多少单词
    Init *init = [[Init alloc]init];
    NSMutableArray *wordsArray = [init GetTodayWords];
    NSMutableArray *newWordArray = [[NSMutableArray alloc]init];
    for (Word *wstr in wordsArray) {
        if ([wstr.word_dictionary compare:word.word_dictionary]==NSOrderedSame) {
            [newWordArray addObject:wstr];
        }
    }
    
    // 3 计算已完成的单词
    float dely;
    if (planCount >= [newWordArray count]) {
        dely = planCount - [newWordArray count];
    }else{
        dely = 0;
    }
    
    float all = planCount*2;
    int count0 = 0;
    int count1 = 0;
    for (wordStructure *str in array) {
        if ([str.right compare:@"0"]==NSOrderedSame) {
            count0++;
        }else{
            count1++;
        }
    }
    
    result = (count1+2*dely)/all;
    if (result > 1) {
        result = 1;
    }
    return result;
}

-(float)CountAllDicProgress:(NSMutableArray*)array{
    //获得用户信息接口
    float result = 0;
    
    // 1 获得今天应该背诵的单词个数
    NSString *planSize = [[NSUserDefaults standardUserDefaults] stringForKey:@"plansize"];
    int planCount = [planSize intValue];
    
    // 2 获得现在还剩下多少单词
    Init *init = [[Init alloc]init];
    NSMutableArray *wordsArray = [init GetTodayWords];
    
    // 获得已完成的单词
    float dely;
    if (planCount >= [wordsArray count]) {
        dely = planCount - [wordsArray count];
    }else{
        dely = 0;
    }
    
    float all = planCount*2;
    int count0 = 0;
    int count1 = 0;
    for (wordStructure *str in array) {
        if ([str.right compare:@"0"]==NSOrderedSame) {
            count0++;
        }else{
            count1++;
        }
    }
    
    result = (count1+2*dely)/all;
    result = (count1+2*dely)/all;
    if (result > 1) {
        result = 1;
    }
    return result;
}

-(void)CountOnePlan{
    //    // 1 计算出该词库应该背诵多少个单词
    //    tools *tool = [[tools alloc]init];
    //    NSMutableArray *dics = [tool readArray:@"doubleStruct"];
    //    int planCount = 0;
    //    for (int i = 0; i < [dics count]; i++) {
    //        secondList *list = [dics objectAtIndex:i];
    //        NSMutableArray *array = list.secondLevel;
    //        for (int j = 0; j < [array count]; j++) {
    //            UserDictionary *OneDic = [[UserDictionary alloc]init];
    //            OneDic = [array objectAtIndex:j];
    //
    //            if ([OneDic.dictionary_name compare:word.word_dictionary]==NSOrderedSame) {
    //                NSMutableArray *OneArray = [[NSMutableArray alloc]init];
    //                [OneArray addObject:OneDic];
    //
    //                DataBase *database = [[DataBase alloc]init];
    //                NSString *days = [database getCompleteTime:OneDic];// 获得完成天数
    //                if (days.length != 0) {
    //                    NSString *dayWords = [self dayWords:OneDic finishDay:days];
    //                    planCount = [dayWords intValue];
    //                }
    //
    //                [OneArray release];
    //                [database release];
    //            }
    //            [OneDic release];
    //        }
    //
    //    }
}
@end
