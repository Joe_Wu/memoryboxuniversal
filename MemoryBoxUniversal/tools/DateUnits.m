//
//  DateUnits.m
//  MemoryBoxRed
//
//  Created by YangJoe on 3/16/14.
//
//

#import "DateUnits.h"
#import "tools.h"

@implementation DateUnits

// get the systme date
-(NSDateComponents*)GetSystemDate{
    NSDate *  senddate=[NSDate date];
    NSCalendar  * cal=[NSCalendar  currentCalendar];
    NSUInteger  unitFlags=NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit;
    NSDateComponents * conponent= [cal components:unitFlags fromDate:senddate];
    return conponent;
}

// save the date of this login time
-(void)SaveLoginDate{
    NSString *y = [[NSString alloc]initWithFormat:@"%ld",(long)[[self GetSystemDate] year]];
    NSString *m = [[NSString alloc]initWithFormat:@"%ld",(long)[[self GetSystemDate] month]];
    NSString *d = [[NSString alloc]initWithFormat:@"%ld",(long)[[self GetSystemDate] day]];
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    [userDefaultes setObject:y forKey:@"login_year"];
    [userDefaultes setObject:m forKey:@"login_month"];
    [userDefaultes setObject:d forKey:@"login_day"];
}

// calculate the lag days from now to last login
-(int)CalculateDateLag{

    NSString *m = [[NSString alloc]initWithFormat:@"%ld",(long)[[self GetSystemDate] month]];
    NSString *d = [[NSString alloc]initWithFormat:@"%ld",(long)[[self GetSystemDate] day]];
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    NSString *lastmonth = [userDefaultes stringForKey:@"login_month"];
    NSString *lastday = [userDefaultes stringForKey:@"login_day"];
    int LagDays = 30*([m intValue] - [lastmonth intValue]) + ([d intValue] - [lastday intValue]);
    return LagDays;
}
@end
