//
//  wordStructure.m
//  MemoryBoxRed
//
//  Created by YangJoe on 3/15/14.
//
//

#import "wordStructure.h"

@implementation wordStructure

- (id) initWithCoder: (NSCoder *)coder {
    if (self = [super init]){
        self.wordNum = [coder decodeObjectForKey:@"wordNum"];
        self.right = [coder decodeObjectForKey:@"right"];
        self.word = [coder decodeObjectForKey:@"word"];
    }
    return self;
}

- (void) encodeWithCoder: (NSCoder *)coder  {
    [coder encodeObject:self.wordNum forKey:@"wordNum"];
    [coder encodeObject:self.right forKey:@"right"];
    [coder encodeObject:self.word forKey:@"word"];
}
@end
