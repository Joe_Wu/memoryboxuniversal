//
//  Init.m
//  MemoryBoxRed
//
//  Created by YangJoe on 3/19/14.
//
//

#import "Init.h"
#import "DataBaseHelper.h"
#import "DateUnits.h"
#import "DataBase.h"
#import "getWordArray.h"
#import "DateUnits.h"

@implementation Init

-(void)Initdatabase{
    /* init database*/
    DataBaseHelper *db = [[DataBaseHelper alloc]init];
    [db initDictoinaryTable];//initialize the table structure
    [db release];
}

-(void)changeWordDay{
    /*init word_day*/
    DateUnits *dataunit = [[DateUnits alloc]init];
    int days = [dataunit CalculateDateLag];
    NSString *LagDays = [[NSString alloc]initWithFormat:@"%d",days];
    
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [userDefaultes stringForKey:@"user_id"];
    DataBase *database = [[DataBase alloc]init];
    [database setWordNotEqualZeroPlusNumber:user_id PlusNumber:LagDays];
    [database release];
}

-(void)InitTodayWords{
    getWordArray *getwords = [[getWordArray alloc]init];
    NSMutableArray *todayWords = [getwords getAllWord];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray * myarray = [[NSArray alloc] initWithArray:todayWords];
    NSData *udObject = [NSKeyedArchiver archivedDataWithRootObject:myarray];
    [userDefaults setObject:udObject forKey:@"TodayWordArray"];
}

-(NSMutableArray*)GetTodayWords{
    NSMutableArray *todayWords;
    // get saved word array for today
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *newArray = [userDefaults objectForKey:@"TodayWordArray"];
    NSArray *getNewArray = [NSKeyedUnarchiver unarchiveObjectWithData:newArray];
    todayWords =  [NSMutableArray arrayWithArray:getNewArray];
    return todayWords;
}

-(void)SaveTodayWords:(NSMutableArray*)array{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray * myarray = [[NSArray alloc] initWithArray:array];
    NSData *udObject = [NSKeyedArchiver archivedDataWithRootObject:myarray];
    [userDefaults setObject:udObject forKey:@"TodayWordArray"];
}
@end
