//
//  tools.m
//  MemoryBox
//
//  Created by YangJoe on 10/28/13.
//
//

#import "tools.h"
#import "ServiceApi.h"
#import "DataBaseHelper.h"
#import "UserDictionary.h"
#import "secondList.h"
#import "Init.h"

@implementation tools
NSUserDefaults *userDefaultes;

// 使用 NSUserDefaults 保存数组
-(void)saveArray:(NSMutableArray*)array saveKey:(NSString*)key{
    userDefaultes = [NSUserDefaults standardUserDefaults]; // 初始化 NSUserDefaults
    NSArray * myarray = [[NSArray alloc] initWithArray:array];
    NSData *udObject = [NSKeyedArchiver archivedDataWithRootObject:myarray];
    [userDefaultes setObject:udObject forKey:key];
    [myarray release];
}

// 使用 NSUserDefaults 读取数组
-(NSMutableArray*)readArray:(NSString*)key{
    userDefaultes = [NSUserDefaults standardUserDefaults]; // 初始化 NSUserDefaults
    NSData *udObject = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    NSArray *myArray = [NSKeyedUnarchiver unarchiveObjectWithData:udObject];
    NSMutableArray *myArray2 =  [NSMutableArray arrayWithArray:myArray];
    return myArray2;
}

// 使用 NSUserDefaults 保存对象
-(void)saveObject:(NSObject*)object saveKey:(NSString*)key{
    userDefaultes = [NSUserDefaults standardUserDefaults]; // 初始化 NSUserDefaults
    [userDefaultes setObject:object forKey:key];
}

// 使用 NSUserDefaults 读取对象
-(NSObject*)readObkect:(NSString*)key{
    userDefaultes = [NSUserDefaults standardUserDefaults]; // 初始化 NSUserDefaults
    NSObject *object = [userDefaultes stringForKey:key];
    return object;
}

// 判断网络是否存在
- (BOOL)isExistenceNetwork {
	BOOL isExistenceNetwork;
	Reachability *r = [Reachability reachabilityWithHostName:@"www.apple.com"];
    switch ([r currentReachabilityStatus]) {
        case NotReachable:
			isExistenceNetwork=FALSE;
            //NSLog(@"没有网络");
            break;
        case ReachableViaWWAN:
			isExistenceNetwork=TRUE;
            //NSLog(@"正在使用3G网络");
            break;
        case ReachableViaWiFi:
			isExistenceNetwork=TRUE;
            //NSLog(@"正在使用wifi网络");
            break;
    }
	return isExistenceNetwork;
}

// 更新词库使用停止状态
-(void)refreshDicsStatus{
    NSMutableArray *doubleStruct = [self readArray:@"doubleStruct"];
    NSMutableArray *dicPause = [[NSMutableArray alloc]init];
    NSString *user_id = [userDefaultes stringForKey:@"user_id"];// 获得用户名
    for (int i =0; i < [doubleStruct count]; i++) {
        secondList *list = [doubleStruct objectAtIndex:i];
        for (int j = 0; j < [list.secondLevel count]; j++) {
            UserDictionary *dic = [list.secondLevel objectAtIndex:j];
            DataBaseHelper *PrecentNum = [[DataBaseHelper alloc]init];
            BOOL use = [PrecentNum isDictionaryPause:dic.dictionary_id userId:user_id];
            [PrecentNum release];
            if (use) {
                [dicPause addObject:@"1"];
            }else{
                [dicPause addObject:@"0"];
            }
        }
    }
    // -------获得保存的词库和 词库的启用状态信息-------start
    
    [self saveArray:dicPause saveKey:@"usedDicsArray"];// 保存词库的使用和停止状态
    [dicPause release];
}

// 删除沙盒的所有文件
-(void)deleteAllData{
    NSFileManager *fileMgr = [[[NSFileManager alloc] init] autorelease];
    NSError *error = nil;
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    NSArray *directoryContents = [fileMgr contentsOfDirectoryAtPath:path error:&error];
    if (error == nil) {
        for (NSString *path2 in directoryContents) {
            if ([path2 compare:@"db.sqlite"]==NSOrderedSame) {
                
            }
            NSString *fullPath = [path stringByAppendingPathComponent:path2];
            BOOL removeSuccess = [fileMgr removeItemAtPath:fullPath error:&error];
            if (!removeSuccess) {
                // Error handling
            }
        }
    } else {
        // Error handling
    }
}

-(NSString*)CountDays{
    //获得系统当前时间
    NSDate *  senddate=[NSDate date];
    NSCalendar  * cal=[NSCalendar  currentCalendar];
    NSUInteger  unitFlags=NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit;
    NSDateComponents * conponent= [cal components:unitFlags fromDate:senddate];
    NSInteger nowMonth=[conponent month];
    NSInteger nowDay=[conponent day];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    //获得上次登录时间
    NSString *lastmoth = [userDefaults stringForKey:@"countmonth"];
    NSString *lastday = [userDefaults stringForKey:@"countdays"];
    
    int oldmonth = [lastmoth intValue];
    int oldday = [lastday intValue];
    
    //计算两次的时间差
    int time = 30*(nowMonth - oldmonth) + (nowDay - oldday);
    NSString *countdays = [[NSString alloc]initWithFormat:@"%d",time];
    
    NSString *savelastday = [[NSString alloc]initWithFormat:@"%d",nowDay];
    NSString *savelastmonth = [[NSString alloc]initWithFormat:@"%d",nowMonth];
    
    if (time >= 3) {
        // 保存本次刷新的时间
        [userDefaults setObject:savelastmonth forKey:@"countmonth"];
        [userDefaults setObject:savelastday forKey:@"countdays"];
    }
    
    return countdays;
}

// change the word index to UserDictionary object
-(UserDictionary*)indexToDic{
    userDefaultes = [NSUserDefaults standardUserDefaults]; // 初始化 NSUserDefaults
    // 获得词库数组中位置
    NSString *dicNum = [userDefaultes stringForKey:@"dicNum"];
    int dicIndex = [dicNum intValue];
    
    NSMutableArray *dics = [self readArray:@"doubleStruct"];
    UserDictionary *OneDic = [[UserDictionary alloc]init];
    int index = 0;
    for (int i = 0; i < [dics count]; i++) {
        secondList *list = [dics objectAtIndex:i];
        NSMutableArray *array = list.secondLevel;
        for (int j = 0; j < [array count]; j++) {
            if (index == dicIndex) {
                OneDic = [array objectAtIndex:j];
                //                    NSLog(@"i= %d,j=%d,%@",i,j,OneDic.dictionary_name);
                index++;
                break;
            }
            index++;
            //                NSLog(@"xx-i= %d,j=%d,%@",i,j,OneDic.dictionary_name);
        }
    }
    return OneDic;
}
@end
