//
//  LoginView.h
//  MemoryBoxUniversal
//
//  Created by YangJoe on 14-5-16.
//  Copyright (c) 2014年 YangJoe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginView : UIViewController<UITextFieldDelegate>{
    int keyBoardMargin_; // transposite the when input
}

@property (weak, nonatomic) IBOutlet UIButton *LoginBut;
@property (weak, nonatomic) IBOutlet UIButton *RegisterBut;
- (IBAction)Login:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *passWord;
@end
