//
//  RegisterView.h
//  MemoryBoxUniversal
//
//  Created by YangJoe on 14-5-16.
//  Copyright (c) 2014年 YangJoe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterView : UIViewController<UITextFieldDelegate>{
    int keyBoardMargin_; // transposite the when input
}

@property (weak, nonatomic) IBOutlet UIButton *Register;

@property (weak, nonatomic) IBOutlet UIButton *CancelBut;
- (IBAction)Register:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *passWord;
@property (weak, nonatomic) IBOutlet UITextField *rePassWord;
@property (weak, nonatomic) IBOutlet UITextField *email;

@end
