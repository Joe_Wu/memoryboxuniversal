//
//  HomeView.h
//  MemoryBoxUniversal
//
//  Created by YangJoe on 14-5-16.
//  Copyright (c) 2014年 YangJoe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIDownloadBar.h"
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "UserDictionary.h"
#import "EGORefreshTableHeaderView.h"
#import "HeadView.h"
#import "MBProgressHUD.h"

@interface HomeView : UIViewController<UITableViewDelegate,UITableViewDataSource,UIApplicationDelegate,EGORefreshTableHeaderDelegate,EGORefreshTableHeaderDelegate,MBProgressHUDDelegate>{
    UIView *leftBackgroundView,*rightBackgroundView;
    UIView *iphoneBackgroundView,*iphoneTableDetailView;
    BOOL isShow;
    
    
    NSMutableArray *dicPause;
    
    //下拉视图
    EGORefreshTableHeaderView * _refreshHeaderView;
    //刷新标识，是否正在刷新过程中
    BOOL _reloading;
    
    // 二级菜单
    NSInteger _currentSection;
    NSInteger _currentRow;
    id record;
    
    UIImageView *bigImage;
    
    UILabel *date;
    UILabel *count;
    
    MBProgressHUD *_progressHUD;
    
    UIView *view;// 时间设置的view

}
@property(nonatomic,strong) UIImageView *pad_start_dialog;
@property(nonatomic,strong) UIImageView *iphone_dialog;


@property(nonatomic, retain) NSMutableArray* doubleStruct;
@property(nonatomic, retain) NSMutableArray* headViewArray;

@property(nonatomic, retain) UITableView* tableView;

@property(nonatomic,retain)UIImageView *barView;// 列表下拉刷新


@property (nonatomic, retain) MBProgressHUD *progressHUD;


-(void)downLoadSuccess:(NSArray*)viewArray;
-(void)downLoadFailed:(NSArray*)viewArray;
-(void)changeDicToBuyState:(NSArray *)ViewArray;


@end
