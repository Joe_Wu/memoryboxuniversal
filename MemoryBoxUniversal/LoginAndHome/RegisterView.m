//
//  RegisterView.m
//  MemoryBoxUniversal
//
//  Created by YangJoe on 14-5-16.
//  Copyright (c) 2014年 YangJoe. All rights reserved.
//

#import "RegisterView.h"
#import "LoginView.h"
#import "Constants.h"

@interface RegisterView ()

@end

@implementation RegisterView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIColor* buttonColor = [UIColor colorWithRed:222.0/255 green:59.0/255 blue:47.0/255 alpha:1.0f];
    
    self.Register.backgroundColor = buttonColor;
    self.Register.layer.cornerRadius = 3.0f;
    self.Register.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:15.0f];
    [self.Register setTitle:@"注册" forState:UIControlStateNormal];
    [self.Register setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.Register setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
    
    self.CancelBut.backgroundColor = buttonColor;
    self.CancelBut.layer.cornerRadius = 3.0f;
    self.CancelBut.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:15.0f];
    [self.CancelBut setTitle:@"取消" forState:UIControlStateNormal];
    [self.CancelBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.CancelBut setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
    
    //对输入框进行监听
    _userName.delegate=self;
    _passWord.delegate=self;
    _rePassWord.delegate=self;
    _email.delegate=self;
    
    _passWord.secureTextEntry = YES; //密码
    _rePassWord.secureTextEntry = YES; //密码
    
    // hide the virtual keyboad
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGr];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Register:(id)sender {
    if (![self.userName.text compare:nil]==NSOrderedSame && ![self.userName.text compare:@""]==NSOrderedSame) {
        
        if ([self.passWord.text compare:self.rePassWord.text] == NSOrderedSame) {//两次密码一致
            NSString *responseState = [[NSString alloc]initWithFormat:@"%@",[self connectToService]];//获得服务器返回的注册结果
            NSLog(@"--->%@",responseState);
            if ([responseState isEqualToString:@"1"]) {//注册成功
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                //保存注册成功的用户名和密码
                [userDefaults setObject:self.userName.text forKey:@"userName"];
                [userDefaults setObject:self.passWord.text forKey:@"passWord"];
                [userDefaults synchronize];
                
                [self dismissModalViewControllerAnimated:NO];
            }else if([responseState compare:@"3.1"] == NSOrderedSame){
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"注册提示" message:@"邮箱已被注册，请使用其他邮箱！" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
                [alert show];
            }else if([responseState compare:@"3.2"] == NSOrderedSame){
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"注册提示" message:@"很抱歉，用户名已被注册！" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
                [alert show];
            }
        }else{//两次输入密码不一致提示
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"注册提示" message:@"两次输入密码不一致，请从新输入!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
        }
    }else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"注册提示" message:@"用户名为空，请输入用户名!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
    }
}

// 链接服务器进行注册
-(NSString *)connectToService{
    NSError *error;
    NSString *re_useName = [[NSString alloc]initWithFormat:@"%@",self.userName.text];
    NSString *re_email = _email.text;
    NSString *re_passWord = [[NSString alloc]initWithFormat:@"%@",self.passWord.text];
    NSString *url = [[NSString alloc]initWithFormat:@"%@userRegisterWithEmail/user_name/%@/user_email/%@/user_password/%@",ApiUrl, re_useName,re_email,re_passWord];//api 登陆接口
    NSLog(@"%@",url);
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSDictionary *weatherDic = [NSJSONSerialization JSONObjectWithData:response  options:NSJSONReadingMutableLeaves error:&error];//Json字典
    return [weatherDic objectForKey:@"state"];//解析出返回的状态参数
}

-(void)viewTapped:(UITapGestureRecognizer*)tapGr
{
    [_userName resignFirstResponder];
    [_passWord resignFirstResponder];
    [_rePassWord resignFirstResponder];
}

// change the location when inputing
- (void)moveView:(UITextField *)textField leaveView:(BOOL)leave{
    float screenHeight,keyboardHeight;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        screenHeight = 1496; //屏幕尺寸，如果屏幕允许旋转，可根据旋转动态调整
        keyboardHeight = 748; //键盘尺寸，如果屏幕允许旋转，可根据旋转动态调整
    }else{
        screenHeight = 1136; //屏幕尺寸，如果屏幕允许旋转，可根据旋转动态调整
        keyboardHeight = 430; //键盘尺寸，如果屏幕允许旋转，可根据旋转动态调整
    }
    float statusBarHeight,NavBarHeight,tableCellHeight,textFieldOriginY,textFieldFromButtomHeigth;
    int margin;
    statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height; //屏幕状态栏高度
    NavBarHeight = self.navigationController.navigationBar.frame.size.height; //获取导航栏高度
    
    UITableViewCell *tableViewCell=(UITableViewCell *)textField.superview;
    tableCellHeight = tableViewCell.frame.size.height; //获取单元格高度
    
    CGRect fieldFrame=[self.view convertRect:textField.frame fromView:tableViewCell];
    textFieldOriginY = fieldFrame.origin.y; //获取文本框相对本视图的y轴位置。
    
    //计算文本框到屏幕底部的高度（屏幕高度-顶部状态栏高度-导航栏高度-文本框的的相对y轴位置-单元格高度）
    textFieldFromButtomHeigth = screenHeight - statusBarHeight - NavBarHeight - textFieldOriginY - tableCellHeight;
    
    if(!leave) {
        if(textFieldFromButtomHeigth < keyboardHeight) { //如果文本框到屏幕底部的高度 < 键盘高度
            margin = keyboardHeight - textFieldFromButtomHeigth; // 则计算差距
            keyBoardMargin_ = margin; //keyBoardMargin_ 为成员变量，记录上一次移动的间距,用户离开文本时恢复视图高度
        } else {
            margin= 0;
            keyBoardMargin_ = 0;
        }
    }
    
    float movementDuration = 0.3f; // 动画时间
    
    int movement;
    UIInterfaceOrientation deviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (deviceOrientation == 0) {
        movement = (leave ? keyBoardMargin_ : -margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }else{
        movement = (leave ? -keyBoardMargin_ : margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }
    
    [UIView beginAnimations: @"textFieldAnim" context: nil]; //添加动画
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        self.view.frame = CGRectOffset(self.view.frame, movement/8, 0);
    }else{
        self.view.frame = CGRectOffset(self.view.frame, 0, -movement/2);
    }
    [UIView commitAnimations];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self moveView:textField leaveView:NO];
}

- (void)textFieldDidEndEditing:(UITextField *)textField;{
    [self moveView:textField leaveView:YES];
}

// 禁止屏幕旋转
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;//只支持这一个方向(正常的方向)
}
@end
