//
//  HomeView.m
//  MemoryBoxUniversal
//
//  Created by YangJoe on 14-5-16.
//  Copyright (c) 2014年 YangJoe. All rights reserved.
//

#import "HomeView.h"
#import "tools.h"
#import "ServiceApi.h"
#import "DataBaseHelper.h"
#import <AudioToolbox/AudioToolbox.h>// 播放声音
#import <QuartzCore/QuartzCore.h>
#import "CBiOSStoreManager.h" // 引入词库购买类
#import "DataBase.h"
#import "Algorithm.h"
#import "DateUnits.h"
#import "Init.h"
#import "ReciteView.h"
#import "wrongWordRevew.h"

@interface HomeView ()

@end

@implementation HomeView

@synthesize pad_start_dialog;// pad的dialog
@synthesize iphone_dialog; // iphone的dialog
@synthesize headViewArray;
@synthesize doubleStruct;//词库的二级结构
@synthesize progressHUD = _progressHUD;
ServiceApi *api;
UserDictionary *dic;
tools *tool;
NSString *userName;
User *user;
BOOL _doneAnything = false;// 购买操作标记

NSMutableArray *viewArray1,*viewArray2,*viewArray3;

int num;//词库编号标记

NSMutableArray *newDownloadDictionaryArray;

bool isPurchase;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initData];// 保存数组
    
    UIColor* backgroundColor = [UIColor colorWithRed:204.0/255 green:41.0/255 blue:19.0/255 alpha:1.0f];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        // 左边整个背景
        leftBackgroundView = [[UIView alloc]initWithFrame:CGRectMake(0,0,300,768)];
        leftBackgroundView.backgroundColor=[UIColor whiteColor];
        
        // 左边上面bar
        UIView *left_top_bar = [[UIView alloc]initWithFrame:CGRectMake(0,0,300,65)];
        left_top_bar.backgroundColor = backgroundColor;
        
        // 左边-上面-刷新
        UIButton *left_top_refresh=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        left_top_refresh.frame=CGRectMake(6,28,45,30);
        left_top_refresh.tag = 0;
        UIImage *refresh_image =[UIImage imageNamed:@"left_top_refresh"];
        [left_top_refresh setBackgroundImage:refresh_image forState:UIControlStateNormal];
        [left_top_refresh addTarget:self action:@selector(refreshTable:) forControlEvents:UIControlEventTouchUpInside];
        [left_top_bar addSubview:left_top_refresh];
        
        UILabel *bar_title = [[UILabel alloc]initWithFrame:CGRectMake(90.0, 32, 200, 30)];
        bar_title.font = [UIFont fontWithName:@"Avenir-Black" size:20.0f];
        bar_title.text = @"词库管理";
        [bar_title setTextColor:[UIColor whiteColor]];
        [left_top_bar addSubview:bar_title];
        
        [leftBackgroundView addSubview:left_top_bar];
        [self.view addSubview:leftBackgroundView];
        
        
        // 右边背景view
        rightBackgroundView = [[UIView alloc]initWithFrame:CGRectMake(300,0,724,768)];
        rightBackgroundView.backgroundColor=[UIColor blackColor];
        
        // 右边上面bar
        UIView *right_top_bar = [[UIView alloc]initWithFrame:CGRectMake(0,0,724,65)];
        right_top_bar.backgroundColor = backgroundColor;
        
        // 右边上面bar中的文字 “记忆盒子”
        UILabel *right_bar_title = [[UILabel alloc]initWithFrame:CGRectMake(280.0, 32, 200, 30)];
        right_bar_title.font = [UIFont fontWithName:@"Avenir-Black" size:20.0f];
        right_bar_title.text = @"记忆盒子";
        [right_bar_title setTextColor:[UIColor whiteColor]];
        [right_top_bar addSubview:right_bar_title];
        
        // 对话框---用户中心
        UIButton *right_top_setting=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        right_top_setting.frame=CGRectMake(600,32,25,25);
        right_top_setting.tag = 1;
        UIImage *setting_image =[UIImage imageNamed:@"top_right_setting"];
        [right_top_setting setBackgroundImage:setting_image forState:UIControlStateNormal];
        [right_top_setting addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
        [right_top_bar addSubview:right_top_setting];
        
        // 对话框---设置中心
        UIButton *right_top_user=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        right_top_user.frame=CGRectMake(550,32,25,25);
        right_top_user.tag = 2;
        UIImage *user_image =[UIImage imageNamed:@"top_right_user"];
        [right_top_user setBackgroundImage:user_image forState:UIControlStateNormal];
        [right_top_user addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
        [right_top_bar addSubview:right_top_user];
        
        // 对话框---背诵中心
        UIButton *right_top_start_learn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        right_top_start_learn.frame=CGRectMake(650,32,25,25);
        right_top_start_learn.tag = 3;
        UIImage *start_learn_image =[UIImage imageNamed:@"top_right_start_learn"];
        [right_top_start_learn setBackgroundImage:start_learn_image forState:UIControlStateNormal];
        [right_top_start_learn addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
        [right_top_bar addSubview:right_top_start_learn];
        [rightBackgroundView addSubview:right_top_bar];
        
        // 右边-中心背景
        UIImageView *right_background = [[UIImageView alloc] initWithFrame:CGRectMake(0,65,724,703)];
        UIImage *R_background = [UIImage imageNamed:@"right_background"];
        [right_background initWithImage:R_background];
        
        // 右边logo
        UIImageView *right_logo = [[UIImageView alloc] initWithFrame:CGRectMake(150,250,420,181)];
        UIImage *logo_view = [UIImage imageNamed:@"right_logo"];
        [right_logo initWithImage:logo_view];
        [right_background addSubview:right_logo];
        
        // 记忆盒子链接
        UILabel *dictionary_name = [[UILabel alloc]initWithFrame:CGRectMake(600.0, 660.0, 100.0, 35.0)];
        dictionary_name.text = @"使用帮助";
        dictionary_name.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        dictionary_name.textColor = [UIColor blackColor];
        dictionary_name.font = [UIFont fontWithName:@"Helvetica" size:18];
        UITapGestureRecognizer *tapRecognizer1=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(linkUrl:)];
        dictionary_name.userInteractionEnabled=YES;
        [dictionary_name addGestureRecognizer:tapRecognizer1];
        [right_background addSubview:dictionary_name];
        [right_background bringSubviewToFront:dictionary_name];
        [right_background setUserInteractionEnabled:YES];
        
        [rightBackgroundView addSubview:right_background];
        
        [self.view addSubview:rightBackgroundView];
        
        [self DataPackgeList];
    }else{
        
        // 设置背景view
        iphoneBackgroundView = [[UIView alloc]initWithFrame:CGRectMake(0,0,320,self.view.frame.size.height)];
        iphoneBackgroundView.backgroundColor = [UIColor whiteColor];
        
        // 产生顶部bar
        UIView *top_bar_background = [[UIView alloc]initWithFrame:CGRectMake(0,0,320,70)];
        top_bar_background.backgroundColor=backgroundColor;
        
        // bar上面的title
        UILabel *bar_title = [[UILabel alloc]initWithFrame:CGRectMake(120.0, 30, 200, 30)];
        bar_title.font = [UIFont fontWithName:@"Avenir-Black" size:20.0f];
        bar_title.text = @"记忆盒子";
        [bar_title setTextColor:[UIColor whiteColor]];
        [top_bar_background addSubview:bar_title];
        
        // 顶部上面bar resfreh
        UIButton *top_refresh=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        top_refresh.frame=CGRectMake(280,35,16,18);
        UIImage *setting_image =[UIImage imageNamed:@"refresh"];
        [top_refresh setBackgroundImage:setting_image forState:UIControlStateNormal];
        [top_refresh addTarget:self action:@selector(refreshTable:) forControlEvents:UIControlEventTouchUpInside];
        [top_bar_background addSubview:top_refresh];
        
        [iphoneBackgroundView addSubview:top_bar_background];
        
        //产生底部bar
        UIView *bottom_bar_background = [[UIView alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-42,320,42)];
        bottom_bar_background.backgroundColor=backgroundColor;
        
        // 底部---个人中心
        UIButton *right_top_setting=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        right_top_setting.frame=CGRectMake(50,9,25,25);
        right_top_setting.tag = 1;
        UIImage *user_image =[UIImage imageNamed:@"tabBar_0"];
        [right_top_setting setBackgroundImage:user_image forState:UIControlStateNormal];
        [right_top_setting addTarget:self action:@selector(IphoneButtom:) forControlEvents:UIControlEventTouchUpInside];
        [bottom_bar_background addSubview:right_top_setting];
        
        // 底部---设置中心
        UIButton *right_top_user=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        right_top_user.frame=CGRectMake(150,9,25,25);
        right_top_user.tag = 2;
        UIImage *set_image =[UIImage imageNamed:@"tabBar_1"];
        [right_top_user setBackgroundImage:set_image forState:UIControlStateNormal];
        [right_top_user addTarget:self action:@selector(IphoneButtom:) forControlEvents:UIControlEventTouchUpInside];
        [bottom_bar_background addSubview:right_top_user];
        
        // 底部---开始背诵
        UIButton *right_top_start_learn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        right_top_start_learn.frame=CGRectMake(250,9,25,25);
        right_top_start_learn.tag = 3;
        UIImage *start_learn_image =[UIImage imageNamed:@"tabBar_2"];
        [right_top_start_learn setBackgroundImage:start_learn_image forState:UIControlStateNormal];
        [right_top_start_learn addTarget:self action:@selector(IphoneButtom:) forControlEvents:UIControlEventTouchUpInside];
        [bottom_bar_background addSubview:right_top_start_learn];

        [iphoneBackgroundView addSubview:bottom_bar_background];
        [self.view addSubview:iphoneBackgroundView];
        [self DataPackgeList];
    }
}

-(void)initData{
    

    
    api =[[ServiceApi alloc]init];     // 初始化数据库操作类
    tool = [[tools alloc]init];// 初始化工具类
    userName = [[NSUserDefaults standardUserDefaults] stringForKey:@"userName"];// 获得用户名
    user =[[User alloc]init]; // 初始化user对象
    NSString *user_name = (NSString*)[tool readObkect:@"user_name"];// 读取用户信息
    if (user_name == nil) {
        [user initWithUserName:userName]; // 初始化user
        [tool saveObject:user.user_name saveKey:@"user_name"];// 保存用户名
        [tool saveObject:user.user_id saveKey:@"user_id"];// 保存用ID
    }
    doubleStruct = [[NSMutableArray alloc]init];// 初始化有二级结构的词库保存数组
    viewArray1=[[NSMutableArray alloc]init];// 初始化左边列表空间元素储存数组
    viewArray2=[[NSMutableArray alloc]init];// 初始化左边列表空间元素储存数组
    newDownloadDictionaryArray = [[NSMutableArray alloc]init];
    dicPause = [[NSMutableArray alloc]init];// 初始化词库暂停使用状态数组
}

-(void)RightTopButton:(id) sender{
    int i = [sender tag];
    switch (i) {
        case 1:
            if (!isShow) {
                [pad_start_dialog removeFromSuperview];
                [self pad_dialog_setting];
                isShow = TRUE;
                pad_start_dialog.alpha = 0.0;
                [UIView animateWithDuration:0.5
                                 animations:^{
                                     pad_start_dialog.alpha = 1.0;
                                 }
                                 completion:^(BOOL finished){
                                     
                                 }];
            }else{
                [pad_start_dialog removeFromSuperview];
                isShow = FALSE;
            }
            break;
        case 2:
            if (!isShow) {
                [pad_start_dialog removeFromSuperview];
                [self pad_user_setting];
                isShow = TRUE;
                pad_start_dialog.alpha = 0.0;
                [UIView animateWithDuration:0.5
                                 animations:^{
                                     pad_start_dialog.alpha = 1.0;
                                 }
                                 completion:^(BOOL finished){
                                     
                                 }];
            }else{
                [pad_start_dialog removeFromSuperview];
                isShow = FALSE;
            }
            break;
        case 3:
            if (!isShow) {
                [pad_start_dialog removeFromSuperview];
                [self pad_dialog_start_learn];
                isShow = TRUE;
                pad_start_dialog.alpha = 0.0;
                [UIView animateWithDuration:0.5
                                 animations:^{
                                     pad_start_dialog.alpha = 1.0;
                                 }
                                 completion:^(BOOL finished){
                                     
                                 }];
            }else{
                [pad_start_dialog removeFromSuperview];
                isShow = FALSE;
            }

            break;
        case 4:
            [self ipad_releaseDialog];
            NSLog(@"恢复购买请求");
            [[CBiOSStoreManager sharedInstance] initialStore];
            [[CBiOSStoreManager sharedInstance] rePurchase];
            break;
        case 5:
            [self ipad_releaseDialog];
            [self dismissModalViewControllerAnimated:NO];
            break;
        case 6:
            [self ipad_releaseDialog];
            break;
        case 7:
            [self ipad_releaseDialog];
            [self Help_Image];
            break;
        case 8:
            [self ipad_releaseDialog];
            // 标记背诵全部词库
            [[NSUserDefaults standardUserDefaults] setObject:@"all-dic" forKey:@"noe-dic"];
            
            // call method to initialize the word for today
            DateUnits *lag = [[DateUnits alloc]init];
            int day = [lag CalculateDateLag]; // 'day' is dely days
            [lag SaveLoginDate];
            
            Init *init = [[Init alloc]init];
            NSMutableArray *array = [init GetTodayWords];
            if (day >0 || [array count]==0) {
                [self Loading2];
                NSThread* myThread = [[NSThread alloc] initWithTarget:self
                                                             selector:@selector(myThreadMainMethod2:)
                                                               object:nil];
                [myThread start];  // Actually create the thread
                
            }else{
                if ([newDownloadDictionaryArray count] > 0) {
                    Algorithm *alg = [[Algorithm alloc]init];
                    [alg UpdateDailyWordArray:newDownloadDictionaryArray];
                    
                    NSString *dailyplan = [[NSString alloc]initWithFormat:@"%f",[alg CountDailyPlan]];
                    tools *tool = [[tools alloc]init];
                    [tool saveObject:dailyplan saveKey:@"dailyplan"];
                }
                
                ReciteView* menuVc = [self.storyboard instantiateViewControllerWithIdentifier:@"ReciteView"];
                menuVc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;	// 淡入淡出.
                [self presentModalViewController:menuVc animated:YES]; //新视图压入到栈中
                
                // 2 获得现在还剩下多少单词
                NSMutableArray *wordsArray2 = [init GetTodayWords];
                NSString *intString = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[wordsArray2 count]];
                [[NSUserDefaults standardUserDefaults] setObject:intString forKey:@"plansize"];
            }

            break;
        case 9:
            [self ipad_releaseDialog];
            wrongWordRevew* menuVc = [self.storyboard instantiateViewControllerWithIdentifier:@"wrongWordRevew"];
            menuVc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;	// 淡入淡出.
            [self presentModalViewController:menuVc animated:YES]; //新视图压入到栈中
            
    }
}

-(void)IphoneButtom:(id) sender{
    long i = [sender tag];
    switch (i) {
        case 1:
            
            if (!isShow) {
                [iphone_dialog removeFromSuperview];
                [self iphone_user_dialog];
                isShow = TRUE;
                iphone_dialog.alpha = 0.0;
                [UIView animateWithDuration:0.5
                                 animations:^{
                                     iphone_dialog.alpha = 1.0;
                                 }
                                 completion:^(BOOL finished){
                                     
                                 }];
            }else{
                [iphone_dialog removeFromSuperview];
                isShow = FALSE;
            }

            break;
        case 2:
            if (!isShow) {
                [iphone_dialog removeFromSuperview];
                [self iphone_dialog_setting];
                isShow = TRUE;
                iphone_dialog.alpha = 0.0;
                [UIView animateWithDuration:0.5
                                 animations:^{
                                     iphone_dialog.alpha = 1.0;
                                 }
                                 completion:^(BOOL finished){
                                     
                                 }];
            }else{
                [iphone_dialog removeFromSuperview];
                isShow = FALSE;
            }

            break;
        case 3:
            if (!isShow) {
                [iphone_dialog removeFromSuperview];
                [self iphone_dialog_remember];
                isShow = TRUE;
                iphone_dialog.alpha = 0.0;
                [UIView animateWithDuration:0.5
                                 animations:^{
                                     iphone_dialog.alpha = 1.0;
                                 }
                                 completion:^(BOOL finished){
                                     
                                 }];
                
            }else{
                [iphone_dialog removeFromSuperview];
                isShow = FALSE;
            }

            break;
        case 4:
            [self iphone_releaseDialog];
            NSLog(@"恢复购买请求");
            [[CBiOSStoreManager sharedInstance] initialStore];
            [[CBiOSStoreManager sharedInstance] rePurchase];
            break;
        case 5:
            [self iphone_releaseDialog];
            [self dismissModalViewControllerAnimated:NO];
            break;
        case 6:
            [self iphone_releaseDialog];
            [self Help_Image];
            break;
        case 7:
            [self iphone_releaseDialog];
            break;
        case 8:
            [self iphone_releaseDialog];
            // 标记背诵全部词库
            [[NSUserDefaults standardUserDefaults] setObject:@"all-dic" forKey:@"noe-dic"];
            
            // call method to initialize the word for today
            DateUnits *lag = [[DateUnits alloc]init];
            int day = [lag CalculateDateLag]; // 'day' is dely days
            [lag SaveLoginDate];
            
            Init *init = [[Init alloc]init];
            NSMutableArray *array = [init GetTodayWords];
            if (day >0 || [array count]==0) {
                [self Loading2];
                NSThread* myThread = [[NSThread alloc] initWithTarget:self
                                                             selector:@selector(myThreadMainMethod2:)
                                                               object:nil];
                [myThread start];  // Actually create the thread
                
            }else{
                if ([newDownloadDictionaryArray count] > 0) {
                    Algorithm *alg = [[Algorithm alloc]init];
                    [alg UpdateDailyWordArray:newDownloadDictionaryArray];
                    
                    NSString *dailyplan = [[NSString alloc]initWithFormat:@"%f",[alg CountDailyPlan]];
                    tools *tool = [[tools alloc]init];
                    [tool saveObject:dailyplan saveKey:@"dailyplan"];
                }
                
                ReciteView* menuVc = [self.storyboard instantiateViewControllerWithIdentifier:@"ReciteView"];
                menuVc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;	// 淡入淡出.
                [self presentModalViewController:menuVc animated:YES]; //新视图压入到栈中
                
                // 2 获得现在还剩下多少单词
                NSMutableArray *wordsArray2 = [init GetTodayWords];
                NSString *intString = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[wordsArray2 count]];
                [[NSUserDefaults standardUserDefaults] setObject:intString forKey:@"plansize"];
            }
            break;
        case 9:
            [self iphone_releaseDialog];
            wrongWordRevew* menuVc = [self.storyboard instantiateViewControllerWithIdentifier:@"wrongWordRevew"];
            menuVc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;	// 淡入淡出.
            [self presentModalViewController:menuVc animated:YES]; //新视图压入到栈中
            
            break;
    }
}

-(void)linkUrl:(id)sender
{
    NSString* path=[NSString stringWithFormat:@"http://jiyihezi.net"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:path]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 对话框---用户设置弹出框
-(void)pad_user_setting{
    // 判断是不是IOS7版本
    pad_start_dialog = [[UIImageView alloc] initWithFrame:CGRectMake(605,68,332,164)];
    UIImage *start = [UIImage imageNamed:@"user_setting"];
    [pad_start_dialog initWithImage:start];
    
    // button-1
    UIButton *synchronization_but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    synchronization_but.frame=CGRectMake(11,50,306,44);
    synchronization_but.tag = 4;
    UIImage *all_image =[UIImage imageNamed:@"but_background2"];
    [synchronization_but setBackgroundImage:all_image forState:UIControlStateNormal];
    [synchronization_but addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable.text = [NSString stringWithFormat:@"恢复购买"];
    lable.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable.textColor = [UIColor blackColor];
    lable.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [synchronization_but addSubview:lable];
    
    UILabel *lable2 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable2.text = [NSString stringWithFormat:@"恢复已购买的词库"];
    lable2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable2.textColor = [UIColor grayColor];
    lable2.font = [UIFont fontWithName:@"Helvetica" size:13];
    [synchronization_but addSubview:lable2];
    
    //---添加文字--end
    
    //button-1 添加到view中
    [pad_start_dialog addSubview:synchronization_but];
    [pad_start_dialog bringSubviewToFront:synchronization_but];
    [pad_start_dialog setUserInteractionEnabled:YES];
    //    [synchronization_but release];
    // button-3
    UIButton *sing_out=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    sing_out.frame=CGRectMake(11,100,306,44);
    sing_out.tag = 5;
    UIImage *sing_out_image =[UIImage imageNamed:@"but_background"];
    [sing_out setBackgroundImage:sing_out_image forState:UIControlStateNormal];
    [sing_out addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable3 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable3.text = [NSString stringWithFormat:@"退出登录"];
    lable3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable3.textColor = [UIColor blackColor];
    lable3.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [sing_out addSubview:lable3];
    
    UILabel *lable4 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable4.text = [NSString stringWithFormat:@"退出登录，或者切换用户名"];
    lable4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable4.textColor = [UIColor grayColor];
    lable4.font = [UIFont fontWithName:@"Helvetica" size:13];
    [sing_out addSubview:lable4];
   
    //---添加文字--end
    
    [pad_start_dialog addSubview:sing_out];
    [pad_start_dialog bringSubviewToFront:sing_out];
    [pad_start_dialog setUserInteractionEnabled:YES];
    //    [sing_out release];
    [self.view addSubview:pad_start_dialog];
}

// 对话框---设置弹出框
-(void)pad_dialog_setting{
    // 判断是不是IOS7版本
    pad_start_dialog = [[UIImageView alloc] initWithFrame:CGRectMake(655,68,332,300)];
    UIImage *start = [UIImage imageNamed:@"system_setting"];
    [pad_start_dialog initWithImage:start];
    
    // button-1
    UIButton *learn_all_but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_all_but.frame=CGRectMake(11,81,306,44);
    learn_all_but.tag = 6;
    UIImage *all_image =[UIImage imageNamed:@"but_background2"];
    [learn_all_but setBackgroundImage:all_image forState:UIControlStateNormal];
    
    // 开关转换器
    UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(215, 6, 20, 10)];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"notification"]) {
        [switchButton setOn:YES];
    }else{
        [switchButton setOn:NO];
    }
    [switchButton addTarget:self action:@selector(notifition:) forControlEvents:UIControlEventValueChanged];
    [learn_all_but addSubview:switchButton];
    [learn_all_but bringSubviewToFront:switchButton];
    [learn_all_but setUserInteractionEnabled:YES];
    
    //---添加文字--start
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable.text = [NSString stringWithFormat:@"通知"];
    lable.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable.textColor = [UIColor blackColor];
    lable.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_all_but addSubview:lable];
    UILabel *lable2 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable2.text = [NSString stringWithFormat:@"打开或关闭提醒"];
    lable2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable2.textColor = [UIColor grayColor];
    lable2.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_all_but addSubview:lable2];
    //---添加文字--end
    
    [pad_start_dialog addSubview:learn_all_but];
    
    // button-2
    UIButton *learn_choice_but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_choice_but.frame=CGRectMake(11,132,306,44);
    UIImage *choice_image =[UIImage imageNamed:@"but_background2"];
    [learn_choice_but setBackgroundImage:choice_image forState:UIControlStateNormal];
    //开发转换器
    UISwitch *switchButton2 = [[UISwitch alloc] initWithFrame:CGRectMake(215, 6, 20, 10)];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sound"]) {
        [switchButton2 setOn:YES];
    }else{
        [switchButton2 setOn:NO];
    }
    [switchButton2 addTarget:self action:@selector(sound:) forControlEvents:UIControlEventValueChanged];
    [learn_choice_but addSubview:switchButton2];
    [learn_choice_but bringSubviewToFront:switchButton2];
    [learn_choice_but setUserInteractionEnabled:YES];
    
    //---添加文字--start
    UILabel *lable3 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable3.text = [NSString stringWithFormat:@"声音"];
    lable3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable3.textColor = [UIColor blackColor];
    lable3.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_choice_but addSubview:lable3];
    
    UILabel *lable4 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable4.text = [NSString stringWithFormat:@"打开或关闭声音"];
    lable4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable4.textColor = [UIColor grayColor];
    lable4.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_choice_but addSubview:lable4];
    
    //---添加文字--end
    
    [pad_start_dialog addSubview:learn_choice_but];
    
    // button-3
    UIButton *learn_wrong_words=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_wrong_words.frame=CGRectMake(11,181,306,44);
    UIImage *wrong_image =[UIImage imageNamed:@"but_background2"];
    [learn_wrong_words setBackgroundImage:wrong_image forState:UIControlStateNormal];
    //开关转换器
    UISwitch *switchButton3 = [[UISwitch alloc] initWithFrame:CGRectMake(215, 6, 20, 10)];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"autoplay"]) {
        [switchButton3 setOn:YES];
    }else{
        [switchButton3 setOn:NO];
    }
    [switchButton3 addTarget:self action:@selector(autoplay:) forControlEvents:UIControlEventValueChanged];
    [learn_wrong_words addSubview:switchButton3];
    [learn_wrong_words bringSubviewToFront:switchButton3];
    [learn_wrong_words setUserInteractionEnabled:YES];
    
    //---添加文字--start
    UILabel *lable5 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable5.text = [NSString stringWithFormat:@"声音自动播放"];
    lable5.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable5.textColor = [UIColor blackColor];
    lable5.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_wrong_words addSubview:lable5];
    
    UILabel *lable6 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable6.text = [NSString stringWithFormat:@"打开此项，背诵时将自动播放音频"];
    lable6.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable6.textColor = [UIColor grayColor];
    lable6.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_wrong_words addSubview:lable6];
    
    //---添加文字--end
    
    [pad_start_dialog addSubview:learn_wrong_words];
    // button-4
    UIButton *setting_help=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    setting_help.frame=CGRectMake(11,238,306,25.5);
    setting_help.tag = 7;
    UIImage *help_image =[UIImage imageNamed:@"but_background"];
    [setting_help setBackgroundImage:help_image forState:UIControlStateNormal];
    [setting_help addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable7 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 3.0, 200.0, 20.0)];
    lable7.text = [NSString stringWithFormat:@"使用帮助"];
    lable7.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable7.textColor = [UIColor blackColor];
    lable7.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [setting_help addSubview:lable7];
    
    //---添加文字--end
    
    [pad_start_dialog addSubview:setting_help];
    [pad_start_dialog bringSubviewToFront:setting_help];
    [pad_start_dialog setUserInteractionEnabled:YES];
    //    [setting_help release];
    [self.view addSubview:pad_start_dialog];
}

// 对话框---开始学习弹出框
-(void)pad_dialog_start_learn{
    // 判断是不是IOS7版本
    pad_start_dialog = [[UIImageView alloc] initWithFrame:CGRectMake(670,68,332,183)];
    UIImage *start = [UIImage imageNamed:@"dialog_start"];
    [pad_start_dialog initWithImage:start];
    
    // button-1
    UIButton *learn_all_but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_all_but.frame=CGRectMake(11,71,306,44);
    learn_all_but.tag = 8;
    UIImage *all_image =[UIImage imageNamed:@"but_background2"];
    [learn_all_but setBackgroundImage:all_image forState:UIControlStateNormal];
    [learn_all_but addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable1 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable1.text = [NSString stringWithFormat:@"背诵全部"];
    lable1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable1.textColor = [UIColor blackColor];
    lable1.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_all_but addSubview:lable1];
    UILabel *lable2 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable2.text = [NSString stringWithFormat:@"将背诵所有已经下载并启用的单词"];
    lable2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable2.textColor = [UIColor grayColor];
    lable2.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_all_but addSubview:lable2];
    //---添加文字--end
    
    [pad_start_dialog addSubview:learn_all_but];
    [pad_start_dialog bringSubviewToFront:learn_all_but];
    [pad_start_dialog setUserInteractionEnabled:YES];
    
    // button-2
    UIButton *learn_wrong_words=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_wrong_words.frame=CGRectMake(11,122,306,44);
    learn_wrong_words.tag = 9;
    UIImage *wrong_image =[UIImage imageNamed:@"but_background"];
    [learn_wrong_words setBackgroundImage:wrong_image forState:UIControlStateNormal];
    [learn_wrong_words addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable3 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable3.text = [NSString stringWithFormat:@"翻阅当日错词"];
    lable3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable3.textColor = [UIColor blackColor];
    lable3.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_wrong_words addSubview:lable3];
    
    UILabel *lable4 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable4.text = [NSString stringWithFormat:@"将只翻阅当日背诵错误的单词"];
    lable4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable4.textColor = [UIColor grayColor];
    lable4.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_wrong_words addSubview:lable4];
    
    //---添加文字--end
    
    [pad_start_dialog addSubview:learn_wrong_words];
    [pad_start_dialog bringSubviewToFront:learn_wrong_words];
    [pad_start_dialog setUserInteractionEnabled:YES];
    [self.view addSubview:pad_start_dialog];
    
}

// 注销弹出框方法
-(void)ipad_releaseDialog{
    if (pad_start_dialog) {
        [pad_start_dialog removeFromSuperview];// 取消弹出框
        isShow = FALSE;// 标记显示状态
    }
}



// iphone---对话框---用户设置弹出框
-(void)iphone_user_dialog{
    
    // 判断是不是IOS7版本
    iphone_dialog = [[UIImageView alloc] initWithFrame:CGRectMake(15,self.view.frame.size.height-177,277,134)];
    UIImage *start = [UIImage imageNamed:@"iphone_user_dialog"];
    [iphone_dialog initWithImage:start];
    
    // button-1
    UIButton *synchronization_but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    synchronization_but.frame=CGRectMake(9,15,261,44);
    synchronization_but.tag = 4;
    UIImage *all_image =[UIImage imageNamed:@"but_background2"];
    [synchronization_but setBackgroundImage:all_image forState:UIControlStateNormal];
    [synchronization_but addTarget:self action:@selector(IphoneButtom:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable.text = [NSString stringWithFormat:@"恢复购买"];
    lable.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable.textColor = [UIColor blackColor];
    lable.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [synchronization_but addSubview:lable];
    
    UILabel *lable2 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable2.text = [NSString stringWithFormat:@"恢复已购买的词库"];
    lable2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable2.textColor = [UIColor grayColor];
    lable2.font = [UIFont fontWithName:@"Helvetica" size:13];
    [synchronization_but addSubview:lable2];
    
    //---添加文字--end
    
    //button-1 添加到view中
    [iphone_dialog addSubview:synchronization_but];
    [iphone_dialog bringSubviewToFront:synchronization_but];
    [iphone_dialog setUserInteractionEnabled:YES];

    // button-2
    UIButton *sing_out=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    sing_out.frame=CGRectMake(11,65,277,44);
    sing_out.tag = 5;
    UIImage *sing_out_image =[UIImage imageNamed:@"but_background"];
    [sing_out setBackgroundImage:sing_out_image forState:UIControlStateNormal];
    [sing_out addTarget:self action:@selector(IphoneButtom:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable3 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable3.text = [NSString stringWithFormat:@"退出登录"];
    lable3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable3.textColor = [UIColor blackColor];
    lable3.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [sing_out addSubview:lable3];
    
    UILabel *lable4 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable4.text = [NSString stringWithFormat:@"退出登录，或者切换用户名"];
    lable4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable4.textColor = [UIColor grayColor];
    lable4.font = [UIFont fontWithName:@"Helvetica" size:13];
    [sing_out addSubview:lable4];
    
    //---添加文字--end
    [iphone_dialog addSubview:sing_out];
    [iphone_dialog bringSubviewToFront:sing_out];
    [iphone_dialog setUserInteractionEnabled:YES];
    //    [sing_out release];
    [self.view addSubview:iphone_dialog];
}

// iphone---对话框---设置弹出框
-(void)iphone_dialog_setting{
    // 判断是不是IOS7版本
    iphone_dialog = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-128,self.view.frame.size.height-349,267,305)];
    UIImage *start = [UIImage imageNamed:@"iphone_setting_dialog"];
    [iphone_dialog initWithImage:start];
    
    // button-1
    UIButton *learn_all_but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_all_but.frame=CGRectMake(8,11,250,44);
    learn_all_but.tag = 6;
    UIImage *all_image =[UIImage imageNamed:@"but_background2"];
    [learn_all_but setBackgroundImage:all_image forState:UIControlStateNormal];
    
    // 开关转换器
    UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(190, 6, 20, 10)];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"notification"]) {
        [switchButton setOn:YES];
    }else{
        [switchButton setOn:NO];
    }
    [switchButton addTarget:self action:@selector(notifition:) forControlEvents:UIControlEventValueChanged];
    [learn_all_but addSubview:switchButton];
    [learn_all_but bringSubviewToFront:switchButton];
    [learn_all_but setUserInteractionEnabled:YES];
    
    //---添加文字--start
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable.text = [NSString stringWithFormat:@"通知"];
    lable.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable.textColor = [UIColor blackColor];
    lable.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_all_but addSubview:lable];
    UILabel *lable2 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable2.text = [NSString stringWithFormat:@"打开或关闭提醒"];
    lable2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable2.textColor = [UIColor grayColor];
    lable2.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_all_but addSubview:lable2];
    //---添加文字--end
    
    [iphone_dialog addSubview:learn_all_but];
    
    // button-2
    UIButton *learn_choice_but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_choice_but.frame=CGRectMake(8,62,250,44);
    learn_choice_but.tag = 7;
    UIImage *choice_image =[UIImage imageNamed:@"but_background2"];
    [learn_choice_but setBackgroundImage:choice_image forState:UIControlStateNormal];
    //开发转换器
    UISwitch *switchButton2 = [[UISwitch alloc] initWithFrame:CGRectMake(190, 6, 20, 10)];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sound"]) {
        [switchButton2 setOn:YES];
    }else{
        [switchButton2 setOn:NO];
    }
    [switchButton2 addTarget:self action:@selector(sound:) forControlEvents:UIControlEventValueChanged];
    [learn_choice_but addSubview:switchButton2];
    [learn_choice_but bringSubviewToFront:switchButton2];
    [learn_choice_but setUserInteractionEnabled:YES];
    
    //---添加文字--start
    UILabel *lable3 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable3.text = [NSString stringWithFormat:@"声音"];
    lable3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable3.textColor = [UIColor blackColor];
    lable3.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_choice_but addSubview:lable3];
    
    UILabel *lable4 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable4.text = [NSString stringWithFormat:@"打开或关闭声音"];
    lable4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable4.textColor = [UIColor grayColor];
    lable4.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_choice_but addSubview:lable4];
    
    //---添加文字--end
    
    [iphone_dialog addSubview:learn_choice_but];
    
    // button-3
    UIButton *learn_wrong_words=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_wrong_words.frame=CGRectMake(8,111,250,44);
    learn_wrong_words.tag = 8;
    UIImage *wrong_image =[UIImage imageNamed:@"but_background2"];
    [learn_wrong_words setBackgroundImage:wrong_image forState:UIControlStateNormal];
    //开关转换器
    UISwitch *switchButton3 = [[UISwitch alloc] initWithFrame:CGRectMake(190, 6, 20, 10)];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"autoplay"]) {
        [switchButton3 setOn:YES];
    }else{
        [switchButton3 setOn:NO];
    }
    [switchButton3 addTarget:self action:@selector(autoplay:) forControlEvents:UIControlEventValueChanged];
    [learn_wrong_words addSubview:switchButton3];
    [learn_wrong_words bringSubviewToFront:switchButton3];
    [learn_wrong_words setUserInteractionEnabled:YES];
    
    //---添加文字--start
    UILabel *lable5 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable5.text = [NSString stringWithFormat:@"声音自动播放"];
    lable5.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable5.textColor = [UIColor blackColor];
    lable5.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_wrong_words addSubview:lable5];
    
    UILabel *lable6 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable6.text = [NSString stringWithFormat:@"打开此项，背诵时将自动播放音频"];
    lable6.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable6.textColor = [UIColor grayColor];
    lable6.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_wrong_words addSubview:lable6];
    
    //---添加文字--end
    
    [iphone_dialog addSubview:learn_wrong_words];
    
    // button-4
    UIButton *setting_help=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    setting_help.frame=CGRectMake(8,168,250,30);
    setting_help.tag = 6;
    UIImage *help_image =[UIImage imageNamed:@"but_background2"];
    [setting_help setBackgroundImage:help_image forState:UIControlStateNormal];
    [setting_help addTarget:self action:@selector(IphoneButtom:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable7 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 3.0, 200.0, 20.0)];
    lable7.text = [NSString stringWithFormat:@"使用帮助"];
    lable7.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable7.textColor = [UIColor blackColor];
    lable7.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [setting_help addSubview:lable7];
    
    //---添加文字--end
    
    // button-5
    UIButton *request_join=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    request_join.frame=CGRectMake(8,208,250,50);
    request_join.tag = 7;
    UIImage *john_image =[UIImage imageNamed:@"but_background"];
    [request_join setBackgroundImage:john_image forState:UIControlStateNormal];
    [request_join addTarget:self action:@selector(IphoneButtom:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable8 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 3.0, 250.0, 20.0)];
    lable8.text = [NSString stringWithFormat:@"申请进入“记忆盒子”综合平台"];
    lable8.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable8.textColor = [UIColor blackColor];
    lable8.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
    [request_join addSubview:lable8];
    
    UILabel *lable9 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 25.0, 240.0, 25.0)];
    lable9.text = [NSString stringWithFormat:@"该平台利用最新技术，旨在全方位提高学生记忆知识\n和学习技术，现阶段仅接受申请和邀请加入"];
    lable9.lineBreakMode = UILineBreakModeWordWrap;
    lable9.numberOfLines = 0;
    lable9.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable9.textColor = [UIColor grayColor];
    lable9.font = [UIFont fontWithName:@"Helvetica" size:10];
    [request_join addSubview:lable9];
    //---添加文字--end
    
    
    [iphone_dialog addSubview:setting_help];
    [iphone_dialog bringSubviewToFront:setting_help];
    [iphone_dialog addSubview:request_join];
    [iphone_dialog bringSubviewToFront:request_join];
    [iphone_dialog setUserInteractionEnabled:YES];
    //    [setting_help release];
    [self.view addSubview:iphone_dialog];
}

// 对话框---开始学习弹出框
-(void)iphone_dialog_remember{
    // 判断是不是IOS7版本
    iphone_dialog = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-263-20,self.view.frame.size.height-177,267,133)];
    UIImage *start = [UIImage imageNamed:@"iphone_memory_dialog"];
    [iphone_dialog initWithImage:start];
    
    // button-1
    UIButton *learn_all_but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_all_but.frame=CGRectMake(10,10,251,44);
    learn_all_but.tag = 8;
    UIImage *all_image =[UIImage imageNamed:@"but_background2"];
    [learn_all_but setBackgroundImage:all_image forState:UIControlStateNormal];
    [learn_all_but addTarget:self action:@selector(IphoneButtom:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable1 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable1.text = [NSString stringWithFormat:@"背诵全部"];
    lable1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable1.textColor = [UIColor blackColor];
    lable1.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_all_but addSubview:lable1];
    UILabel *lable2 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable2.text = [NSString stringWithFormat:@"将背诵所有已经下载并启用的单词"];
    lable2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable2.textColor = [UIColor grayColor];
    lable2.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_all_but addSubview:lable2];
    //---添加文字--end
    
    [iphone_dialog addSubview:learn_all_but];
    [iphone_dialog bringSubviewToFront:learn_all_but];
    [iphone_dialog setUserInteractionEnabled:YES];
    
    // button-2
    UIButton *learn_wrong_words=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_wrong_words.frame=CGRectMake(10,60,290,44);
    learn_wrong_words.tag = 9;
    UIImage *wrong_image =[UIImage imageNamed:@"but_background"];
    [learn_wrong_words setBackgroundImage:wrong_image forState:UIControlStateNormal];
    [learn_wrong_words addTarget:self action:@selector(IphoneButtom:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable3 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable3.text = [NSString stringWithFormat:@"翻阅当日错词"];
    lable3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable3.textColor = [UIColor blackColor];
    lable3.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_wrong_words addSubview:lable3];
    
    UILabel *lable4 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable4.text = [NSString stringWithFormat:@"将只翻阅当日背诵错误的单词"];
    lable4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable4.textColor = [UIColor grayColor];
    lable4.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_wrong_words addSubview:lable4];
    
    //---添加文字--end
    
    [iphone_dialog addSubview:learn_wrong_words];
    [iphone_dialog bringSubviewToFront:learn_wrong_words];
    [iphone_dialog setUserInteractionEnabled:YES];
    [self.view addSubview:iphone_dialog];
    
}


// 点击后图片放大
-(void)Help_Image{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        UIImage *image = [UIImage imageNamed:@"pad_help_guide"];
        bigImage = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.height,self.view.frame.size.width)];
        [bigImage initWithImage:image];
    }else{
        UIImage *image = [UIImage imageNamed:@"iphone_help_guide"];
        bigImage = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
        [bigImage initWithImage:image];
    }
    
    
    bigImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(UesrClicked:)];
    [bigImage addGestureRecognizer:singleTap];
    [singleTap release];
    NSArray *selfViewArray = [self.view subviews];
    for (int i =0; i <[selfViewArray count]; i++) {
        [[selfViewArray objectAtIndex:i] setHidden:YES];
    }
    [self.view addSubview:bigImage];
}

// 取消图片放大
-(void)UesrClicked:(id)sender{
    [bigImage removeFromSuperview];
    NSArray *selfViewArray = [self.view subviews];
    for (int i =0; i <[selfViewArray count]; i++) {
        [[selfViewArray objectAtIndex:i] setHidden:NO];
    }
}



// 通知开关事件
-(void)notifition:(id) sender{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"notification"];
    }else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"notification"];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
}

// 声音开关事件
-(void)sound:(id) sender{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"sound"];
    }else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"sound"];
    }
}

// 自动播放声音
-(void)autoplay:(id) sender{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"autoplay"];
    }else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"autoplay"];
    }
}

// 注销弹出框方法
-(void)iphone_releaseDialog{
    if (iphone_dialog) {
        [iphone_dialog removeFromSuperview];// 取消弹出框
        isShow = FALSE;// 标记显示状态
    }
}





// 创建左边的列表视图－－－－iPhone
-(void) DataPackgeList{
    dicPause = [[NSMutableArray alloc]init];// 初始化词库暂停使用状态数组
    // -------获得保存的词库和 词库的启用状态信息-------start
    tools *tool2 = [[tools alloc]init];
    doubleStruct = [tool2 readArray:@"doubleStruct"];
    NSString *user_name = (NSString*)[tool2 readObkect:@"user_name"];
    NSString *user_id = (NSString*)[tool2 readObkect:@"user_id"];
    
    // 获得上次刷新的天数
    NSString *refreshDays = [tool2 CountDays];
    int refreshday = [refreshDays intValue];
    
    if ([doubleStruct count] != 0 && refreshday < 3) {
    }else {
        // 获得有二级结构的词库列表
        doubleStruct = [api getUserHadBuyDictionary2SencondList:user_name userId:user_id];
        [tool2 saveArray:doubleStruct saveKey:@"doubleStruct"];// 保存二级菜单数组
    }
    
    for (int i =0; i < [doubleStruct count]; i++) {
        secondList *list = [doubleStruct objectAtIndex:i];
        for (int j = 0; j < [list.secondLevel count]; j++) {
            dic = [list.secondLevel objectAtIndex:j];
            DataBaseHelper *PrecentNum = [[DataBaseHelper alloc]init];
            BOOL use = [PrecentNum isDictionaryPause:dic.dictionary_id userId:user_id];
            if (use) {
                [dicPause addObject:@"1"];
            }else{
                [dicPause addObject:@"0"];
            }
        }
    }
    
    // -------获得保存的词库和 词库的启用状态信息-------start
    
    [tool2 saveArray:dicPause saveKey:@"usedDicsArray"];// 保存词库的使用和停止状态
    
    
    // ---------二级菜单
    [self loadModel];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 65,300,leftBackgroundView.frame.size.height-70) style:UITableViewStyleGrouped];
    }else{
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 70,self.view.frame.size.width,self.view.frame.size.height-42-70) style:UITableViewStyleGrouped];
    }
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorColor = [UIColor clearColor];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [leftBackgroundView addSubview:_tableView];
    }else{
        [iphoneBackgroundView addSubview:_tableView];
    }
    
    
    // --------二级菜单
    self.view.backgroundColor = [UIColor blackColor];
    
    // --------列表刷新
    if (_refreshHeaderView == nil) {
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height-70)];
		view.delegate = self;
		[self.tableView addSubview:view];
		_refreshHeaderView = view;
	}
    // --------列表刷新
}






// ----------------------------二级菜单---start
- (void)loadModel {
    _currentRow = -1;
    headViewArray = [[NSMutableArray alloc]init ];
    for(int i = 0;i< [doubleStruct count] ;i++) {
		HeadView* headview = [[HeadView alloc] init];
        headview.delegate = self;
		headview.section = i;
        
        //获得文件名
        UILabel *dictionary_name = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 5.0, self.view.frame.size.width-50, 35.0)];
        secondList *list = [doubleStruct objectAtIndex:i];
        dictionary_name.text = [NSString stringWithFormat:@"%@",list.firstLevel];
        dictionary_name.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        dictionary_name.textColor = [UIColor blackColor];
        dictionary_name.font = [UIFont fontWithName:@"Avenir-Black" size:17];
        [headview.backBtn addSubview:dictionary_name];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10,13,20,20)];
        UIImage *image = [UIImage imageNamed:@"book_icon"];
        [imageView initWithImage:image];
        [headview.backBtn addSubview:imageView];
        
        // -----添加内容结束
        [self.headViewArray addObject:headview];
	}
}



#pragma mark - TableViewdelegate&&TableViewdataSource

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    HeadView* headView = [self.headViewArray objectAtIndex:indexPath.section];
    
    return headView.open?45:0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [self.headViewArray objectAtIndex:section];
}

// 设置每个二级列表的数量
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    HeadView* headView = [self.headViewArray objectAtIndex:section];
    
    tools *tool = [[tools alloc]init];
    NSMutableArray *wordsArray =  [tool readArray:@"doubleStruct"];
    secondList *list = [wordsArray objectAtIndex:section];
    int intString = [list.secondLevel count];
    return headView.open?intString:0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.headViewArray count];
}

// 添加二级菜单的内容
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *indentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
        UIButton* backBtn=  [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 45)];
        backBtn.tag = 20000;
        [backBtn setBackgroundImage:[UIImage imageNamed:@"btn_on"] forState:UIControlStateHighlighted];
        backBtn.userInteractionEnabled = NO;
        [cell.contentView addSubview:backBtn];
    }
    UIButton* backBtn = (UIButton*)[cell.contentView viewWithTag:20000];
    HeadView* view = [self.headViewArray objectAtIndex:indexPath.section];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"btn_2_nomal"] forState:UIControlStateNormal];
    
    if (view.open) {
        if (indexPath.row == _currentRow) {
            [backBtn setBackgroundImage:[UIImage imageNamed:@"btn_nomal"] forState:UIControlStateNormal];
        }
    }
    
    tools *tool = [[tools alloc]init];
    NSMutableArray *wordsArray =  [tool readArray:@"doubleStruct"];
    
    int i = 0;
    for (int j = 0; j < [wordsArray count]; j++) {
        secondList *second = [wordsArray objectAtIndex:j];
        NSMutableArray *secondDic = second.secondLevel;
        for (int k = 0 ; k < [secondDic count]; k++) {
            if (j == indexPath.section && k == indexPath.row) {
                break;
                NSLog(@"same");
            }else{
                i++;
            }
        }
        
        if (j == indexPath.section) {
            break;
        }
    }
    
    
    
    secondList *second = [wordsArray objectAtIndex:indexPath.section];
    NSMutableArray *secondDic = second.secondLevel;
    dic = [secondDic objectAtIndex:indexPath.row];
    
    // 清楚 cell 中的内容
    NSArray *uis = [cell.contentView subviews];
    for (int i = 0; i <[uis count]; i++) {
        [[uis objectAtIndex:i] removeFromSuperview];
    }
    // 添加列表文字
    UILabel *dictionary_name = [[UILabel alloc]initWithFrame:CGRectMake(45.0, 5.0, self.view.frame.size.width-50, 35.0)];
    dictionary_name.text = [NSString stringWithFormat:@"%@",dic.dictionary_name];
    dictionary_name.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    dictionary_name.textColor = [UIColor blackColor];
    dictionary_name.font = [UIFont fontWithName:@"Helvetica" size:15];
    [cell.contentView addSubview:dictionary_name];
    dicPause = [tool readArray:@"usedDicsArray"];
    NSLog(@"--%d; -%@",i,[dicPause objectAtIndex:i]);
    // 是否启用图标
    NSString *useString = [NSString stringWithFormat:@"%@",[dicPause objectAtIndex:i]];
    if ([useString compare:@"1"]==NSOrderedSame) {
        // 图标
        UIButton *left_list_item_logo=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        left_list_item_logo.frame=CGRectMake(15,15,21,21);
        UIImage *refresh_image =[UIImage imageNamed:@"dic_unusered"];
        left_list_item_logo.tag = (long)dic;
        [left_list_item_logo setBackgroundImage:refresh_image forState:UIControlStateNormal];
        [left_list_item_logo addTarget:self action:@selector(DicState:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:left_list_item_logo];
    }else{
        // 图标
        UIButton *left_list_item_logo=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        left_list_item_logo.frame=CGRectMake(15,15,21,21);
        UIImage *refresh_image =[UIImage imageNamed:@"left_list_logo"];
        left_list_item_logo.tag = (long)dic;
        [left_list_item_logo setBackgroundImage:refresh_image forState:UIControlStateNormal];
        [left_list_item_logo addTarget:self action:@selector(DicState:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:left_list_item_logo];
    }
    // 添加分界线
    UIImageView* line = [[UIImageView alloc]initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, 1)];
    line.backgroundColor = [UIColor grayColor];
    [cell.contentView addSubview:line];
    return cell;
}

// 二级菜单选中cell 的监听事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _currentRow = indexPath.row;
    [_tableView reloadData];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    tools *tool2 = [[tools alloc]init];
    NSMutableArray *wordsArray =  [tool2 readArray:@"doubleStruct"];
    
    int i = 0;
    for (int j = 0; j < [wordsArray count]; j++) {
        secondList *second = [wordsArray objectAtIndex:j];
        NSMutableArray *secondDic = second.secondLevel;
        for (int k = 0 ; k < [secondDic count]; k++) {
            if (j == indexPath.section && k == indexPath.row) {
                break;
            }else{
                i++;
            }
        }
        
        if (j == indexPath.section) {
            break;
        }
    }
    
    secondList *second = [wordsArray objectAtIndex:indexPath.section];
    NSMutableArray *secondDic = second.secondLevel;
    dic = [secondDic objectAtIndex:indexPath.row];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [leftBackgroundView setHidden:YES];
        [self DataPackgeDetailPad:i uderDic:dic];
    }else{
        [iphoneBackgroundView setHidden:YES];
        [self DataPackgeDetail:i uderDic:dic];
    }
}

// 一级菜单响应事件
#pragma mark - HeadViewdelegate
-(void)selectedWith:(HeadView *)view{
    [self iphone_releaseDialog];// 取消dialog显示
    _currentRow = -1;
    if (view.open) {
        for(int i = 0;i<[headViewArray count];i++) {
            HeadView *head = [headViewArray objectAtIndex:i];
            head.open = NO;
            [head.backBtn setBackgroundImage:[UIImage imageNamed:@"btn_momal"] forState:UIControlStateNormal];
        }
        [_tableView reloadData];
        return;
    }
    _currentSection = view.section;
    [self reset];
}

//界面重置
- (void)reset {
    for(int i = 0;i<[headViewArray count];i++)
    {
        HeadView *head = [headViewArray objectAtIndex:i];
        
        if(head.section == _currentSection)
        {
            head.open = YES;
            [head.backBtn setBackgroundImage:[UIImage imageNamed:@"btn_nomal"] forState:UIControlStateNormal];
            
        }else {
            [head.backBtn setBackgroundImage:[UIImage imageNamed:@"btn_momal"] forState:UIControlStateNormal];
            
            head.open = NO;
        }
    }
    [_tableView reloadData];
    
}
// ----------------------------二级菜单---end




// 创建词库详情视图
-(void) DataPackgeDetail:(int) dicId uderDic:(UserDictionary*)mydic{
    
    dic = mydic;

    UIColor* buttonColor = [UIColor colorWithRed:204.0/255 green:41.0/255 blue:19.0/255 alpha:1.0f];
    
    num = dicId;
    NSString *dicNum = [NSString stringWithFormat:@"%d",num];
    [[NSUserDefaults standardUserDefaults] setObject:dicNum forKey:@"dicNum"];
    
    // 1---view---设置背景view
    iphoneTableDetailView = [[UIView alloc]initWithFrame:CGRectMake(0,0,320,self.view.frame.size.height)];
    iphoneTableDetailView.backgroundColor = [UIColor whiteColor];
    
    UIColor* backgroundColor = [UIColor colorWithRed:204.0/255 green:41.0/255 blue:19.0/255 alpha:1.0f];
    
    
    
    // 2---View---产生顶部bar
    UIView *top_bar_background = [[UIView alloc]initWithFrame:CGRectMake(0,0,320,70)];
    top_bar_background.backgroundColor=backgroundColor;
    
    // bar上面的title
    UILabel *bar_title = [[UILabel alloc]initWithFrame:CGRectMake(120.0, 30, 200, 30)];
    bar_title.font = [UIFont fontWithName:@"Avenir-Black" size:20.0f];
    bar_title.text = @"记忆盒子";
    [bar_title setTextColor:[UIColor whiteColor]];
    [top_bar_background addSubview:bar_title];
    
    // 顶部上面bar back button
    UIButton *top_refresh=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    top_refresh.frame=CGRectMake(10,30,50,30);
    UIImage *setting_image =[UIImage imageNamed:@"bac_top"];
    [top_refresh setBackgroundImage:setting_image forState:UIControlStateNormal];
    [top_refresh addTarget:self action:@selector(back_to_first:) forControlEvents:UIControlEventTouchUpInside];
    [top_bar_background addSubview:top_refresh];
    
    [iphoneTableDetailView addSubview:top_bar_background];
    
    
    
 
    // 3---View---详情背景
    UIView *left_table_background = [[UIView alloc]initWithFrame:CGRectMake(0,70,self.view.frame.size.width,self.view.frame.size.height-70)];
    left_table_background.backgroundColor=[UIColor whiteColor];
    
    
    //显示词库名
    UILabel *dictionary_name = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 10.0, self.view.frame.size.width-20, 30.0)];
    dictionary_name.text = [NSString stringWithFormat:@"%@",mydic.dictionary_name];
    dictionary_name.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    dictionary_name.textColor = [UIColor blackColor];
    dictionary_name.font = [UIFont fontWithName:@"Avenir-Black" size:20];
    [left_table_background addSubview:dictionary_name];
    [viewArray2 addObject:dictionary_name];
    
    
    // 词库总数量
    UILabel *dictionary_count = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 40.0, self.view.frame.size.width-20, 20.0)];
    dictionary_count.text = [NSString stringWithFormat:@"词库总数量:%@",mydic.count];
    dictionary_count.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    dictionary_count.textColor = [UIColor blackColor];
    dictionary_count.font = [UIFont fontWithName:@"Helvetica" size:17];
    [left_table_background addSubview:dictionary_count];
    [viewArray2 addObject:dictionary_count];
    
    
    //已完成数量
    UILabel *dictionary_finished_num = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 60.0, self.view.frame.size.width-20, 20.0)];
    DataBaseHelper *helper = [[DataBaseHelper alloc]init];
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    int finishWords = [helper getWordDayNotEqualZero:mydic.dictionary_id userId:user_id];
    dictionary_finished_num.text = [NSString stringWithFormat:@"已完成数量:%d",finishWords];
    dictionary_finished_num.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    dictionary_finished_num.textColor = [UIColor blackColor];
    dictionary_finished_num.font = [UIFont fontWithName:@"Helvetica" size:17];
    [left_table_background addSubview:dictionary_finished_num];
    [viewArray2 addObject:dictionary_finished_num];
    
    /*
     *判断词库文件夹路径是否存在
     */
    NSString *documentsDirectory2 = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    NSFileManager *fileManager2 = [NSFileManager defaultManager];
    NSString *filePath2 =  [documentsDirectory2 stringByAppendingPathComponent:mydic.dictionary_id];
    BOOL isDirExist = [fileManager2 fileExistsAtPath:filePath2];
    
    
    // 判断是否购买
    if (![mydic.buy compare:@"1"] == NSOrderedSame && !isDirExist) {// 没有购买

        // 显示词库信息
        UILabel *dictionary_information = [[UILabel alloc]initWithFrame:CGRectMake(10, 80, self.view.frame.size.width-20, self.view.frame.size.height-370)];
        NSString *dicDetial;
        if ([mydic.dictionary_detail compare:nil]==NSOrderedSame) {
            dicDetial = @" ";
        }else{
            dicDetial = mydic.dictionary_detail;
        }
        dictionary_information.text = [NSString stringWithFormat:@"%@",dicDetial];
        [dictionary_information setNumberOfLines:0];
        dictionary_information.lineBreakMode = UILineBreakModeWordWrap;
        dictionary_information.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        dictionary_information.textColor = [UIColor blackColor];
        dictionary_information.font = [UIFont fontWithName:@"Helvetica" size:15];
        [left_table_background addSubview:dictionary_information];
        [viewArray2 addObject:dictionary_information];
        
        
        // 购买
        UIButton *BuyDicBut=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        BuyDicBut.frame=CGRectMake(10,self.view.frame.size.height-250,self.view.frame.size.width-20,43);
        BuyDicBut.tag = (long)mydic;
        NSString *price = [NSString stringWithFormat:@"￥%@",mydic.dictionary_price];
        [BuyDicBut setTitle:price forState:UIControlStateNormal];
        
        BuyDicBut.backgroundColor = buttonColor;
        BuyDicBut.layer.cornerRadius = 3.0f;
        BuyDicBut.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
        [BuyDicBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [BuyDicBut setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
        [BuyDicBut addTarget:self action:@selector(Purchase:) forControlEvents:UIControlEventTouchUpInside];
        
        [left_table_background addSubview:BuyDicBut];
        [viewArray2 addObject:BuyDicBut];
        
        
        
        //-------隐藏--start----
        // 下载
        UIButton *download=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        download.frame=CGRectMake(10,self.view.frame.size.height-250,self.view.frame.size.width-20,43);
        download.tag = (long)mydic;
        download.backgroundColor = buttonColor;
        download.layer.cornerRadius = 3.0f;
        download.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
        [download setTitle:@"下载" forState:UIControlStateNormal];
        [download setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [download setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
        [download addTarget:self action:@selector(downloaddic:) forControlEvents:UIControlEventTouchUpInside];
        [download setHidden:YES];
        [left_table_background addSubview:download];
        [viewArray2 addObject:download];
        
        
//        // 设置发音
//        UIImageView *set_tone = [[UIImageView alloc] initWithFrame:CGRectMake(15,200,273,50)];
//        UIImage *tone = [UIImage imageNamed:@"set_tone"];
//        [set_tone initWithImage:tone];
//        
//        DataBase *pronucations = [[DataBase alloc]init];
//        NSString *pronucation = [pronucations getCurrentDictionaryPronounce:mydic];
//        // 转换器-设置发音
//        UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(190, 12, 20, 10)];
//        [set_tone setHidden:YES];
//        if ([pronucation compare:@"US"] == NSOrderedSame) {
//            [switchButton setOn:YES];
//        }else{
//            [switchButton setOn:NO];
//        }
//        [switchButton addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
//        [set_tone addSubview:switchButton];
//        [set_tone bringSubviewToFront:switchButton];
//        [set_tone setUserInteractionEnabled:YES];
//        [left_table_background addSubview:set_tone];
//        [viewArray2 addObject:set_tone];
        
        // 设置完成时间
        UIImageView *setFinishTime = [[UIImageView alloc] initWithFrame:CGRectMake(10,self.view.frame.size.height-280,self.view.frame.size.width-20,90)];
        UIImage *time = [UIImage imageNamed:@"set_finish_time"];
        [setFinishTime initWithImage:time];
        
        //获得日期和词汇量
        DataBase *dateAndWord = [[DataBase alloc]init];
        NSString *days = [dateAndWord getCompleteTime:mydic];
        
        // 时间
        date = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 8.0, 260.0, 35.0)];
        
        //日背诵量
        count = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 50.0, 260.0, 35.0)];
        
        if ([days compare:nil]!=NSOrderedSame && [days compare:@""]!=NSOrderedSame && [days compare:@"0"]!=NSOrderedSame) {
            NSString *dayWords = [self dayWords:mydic finishDay:days];
            count.text = [NSString stringWithFormat:@"日背诵量: %@",dayWords];
            NSString *finishTime = [self getFinishDate:days];
            date.text = [NSString stringWithFormat:@"预计完成时间: %@",finishTime];
        }else{
            count.text = [NSString stringWithFormat:@"日背诵量: 25"];
            date.text = [NSString stringWithFormat:@"预计完成时间: %@",[self SuggestDate:dic]];
        }
        
        date.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        date.textColor = [UIColor blackColor];
        date.font = [UIFont fontWithName:@"Helvetica" size:18];
        [setFinishTime addSubview:date];
        
        count.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        count.textColor = [UIColor blackColor];
        count.font = [UIFont fontWithName:@"Helvetica" size:18];
        [setFinishTime addSubview:count];
        
        setFinishTime.userInteractionEnabled = YES;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ImageViewClick)];
        [setFinishTime addGestureRecognizer:singleTap];
        [singleTap release];
        
        [setFinishTime setHidden:YES];
        [left_table_background addSubview:setFinishTime];
        [viewArray2 addObject:setFinishTime];
        
        
        // 背诵当个词库
        UIButton *skim=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        skim.frame=CGRectMake(10,self.view.frame.size.height-170,self.view.frame.size.width-20,40);
        skim.tag = (long)mydic;
        skim.backgroundColor = buttonColor;
        skim.layer.cornerRadius = 3.0f;
        skim.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
        [skim setTitle:@"开始背诵" forState:UIControlStateNormal];
        [skim setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [skim setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
        [skim addTarget:self action:@selector(skimDic:) forControlEvents:UIControlEventTouchUpInside];
        [skim setHidden:YES];
        [left_table_background addSubview:skim];
        [viewArray2 addObject:skim];
        
        
        
        // 删除
        UIButton *delete=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        delete.frame=CGRectMake(10,self.view.frame.size.height-120,self.view.frame.size.width-20,40);
        delete.tag = (long)mydic;
        delete.backgroundColor = buttonColor;
        delete.layer.cornerRadius = 3.0f;
        delete.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
        [delete setTitle:@"删除" forState:UIControlStateNormal];
        [delete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [delete setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
        [delete addTarget:self action:@selector(Delete:) forControlEvents:UIControlEventTouchUpInside];
        [delete setHidden:YES];
        [left_table_background addSubview:delete];
        [viewArray2 addObject:delete];
        
        
        //-------隐藏--结束------
        
    }else{// 已经购买
        /*
         *获得文件名
         */
        NSString *dicName = [NSString stringWithFormat:@"%@.zip",mydic.dictionary_id];
        
        /*
         *判断词库文件夹路径是否存在
         */
        NSString *documentsDirectory2 = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
        NSFileManager *fileManager2 = [NSFileManager defaultManager];
        NSString *filePath2 =  [documentsDirectory2 stringByAppendingPathComponent:mydic.dictionary_id];
        BOOL isDirExist = [fileManager2 fileExistsAtPath:filePath2];
        
        /*
         *获得压缩文件是否存在
         */
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:dicName];
        BOOL isFileExist = [fileManager fileExistsAtPath:filePath];
        if((!isFileExist)&&(!isDirExist)) //如果压缩文件不存在并且词库文件夹不在
        {
            
            // 显示词库信息
            UILabel *dictionary_information = [[UILabel alloc]initWithFrame:CGRectMake(10, 80, self.view.frame.size.width-20, self.view.frame.size.height-370)];
            NSString *dicDetial;
            if ([mydic.dictionary_detail compare:nil]==NSOrderedSame) {
                dicDetial = @" ";
            }else{
                dicDetial = mydic.dictionary_detail;
            }
            dictionary_information.text = [NSString stringWithFormat:@"%@",dicDetial];
            [dictionary_information setNumberOfLines:0];
            dictionary_information.lineBreakMode = UILineBreakModeWordWrap;
            dictionary_information.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            dictionary_information.textColor = [UIColor blackColor];
            dictionary_information.font = [UIFont fontWithName:@"Helvetica" size:15];
            [left_table_background addSubview:dictionary_information];
            [viewArray2 addObject:dictionary_information];
            
            
            
            
            // 下载
            UIButton *download=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            download.frame=CGRectMake(10,self.view.frame.size.height-250,self.view.frame.size.width-20,43);
            download.tag = (long)mydic;
            download.backgroundColor = buttonColor;
            download.layer.cornerRadius = 3.0f;
            download.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
            [download setTitle:@"下载" forState:UIControlStateNormal];
            [download setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [download setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
            [download addTarget:self action:@selector(downloaddic:) forControlEvents:UIControlEventTouchUpInside];
            
            [left_table_background addSubview:download];
            [viewArray2 addObject:download];
            
            
            
            // 设置完成时间
            UIImageView *setFinishTime = [[UIImageView alloc] initWithFrame:CGRectMake(10,self.view.frame.size.height-280,self.view.frame.size.width-20,90)];
            UIImage *time = [UIImage imageNamed:@"set_finish_time"];
            [setFinishTime initWithImage:time];
            
            //获得日期和词汇量
            DataBase *dateAndWord = [[DataBase alloc]init];
            NSString *days = [dateAndWord getCompleteTime:mydic];
            
            // 时间
            date = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 8.0, 260.0, 35.0)];
            
            //日背诵量
            count = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 50.0, 260.0, 35.0)];
            
            if ([days compare:nil]!=NSOrderedSame && [days compare:@""]!=NSOrderedSame && [days compare:@"0"]!=NSOrderedSame) {
                NSString *dayWords = [self dayWords:mydic finishDay:days];
                count.text = [NSString stringWithFormat:@"日背诵量: %@",dayWords];
                NSString *finishTime = [self getFinishDate:days];
                date.text = [NSString stringWithFormat:@"预计完成时间: %@",finishTime];
            }else{
                count.text = [NSString stringWithFormat:@"日背诵量: 25"];
                date.text = [NSString stringWithFormat:@"预计完成时间: %@",[self SuggestDate:mydic]];
            }
            
            date.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            date.textColor = [UIColor blackColor];
            date.font = [UIFont fontWithName:@"Helvetica" size:18];
            [setFinishTime addSubview:date];
            
            count.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            count.textColor = [UIColor blackColor];
            count.font = [UIFont fontWithName:@"Helvetica" size:18];
            [setFinishTime addSubview:count];
            
            setFinishTime.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ImageViewClick)];
            [setFinishTime addGestureRecognizer:singleTap];
            [singleTap release];
            
            [setFinishTime setHidden:YES];
            [left_table_background addSubview:setFinishTime];
            [viewArray2 addObject:setFinishTime];
            
            
            
            
            
            
            // 背诵当个词库
            UIButton *skim=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            skim.frame=CGRectMake(10,self.view.frame.size.height-170,self.view.frame.size.width-20,40);
            skim.tag = (long)mydic;
            skim.backgroundColor = buttonColor;
            skim.layer.cornerRadius = 3.0f;
            skim.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
            [skim setTitle:@"开始背诵" forState:UIControlStateNormal];
            [skim setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [skim setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
            [skim addTarget:self action:@selector(skimDic:) forControlEvents:UIControlEventTouchUpInside];
            [skim setHidden:YES];
            [left_table_background addSubview:skim];
            [viewArray2 addObject:skim];
            
            
            
            // 删除
            UIButton *delete=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            delete.frame=CGRectMake(10,self.view.frame.size.height-120,self.view.frame.size.width-20,40);
            delete.tag = (long)mydic;
            delete.backgroundColor = buttonColor;
            delete.layer.cornerRadius = 3.0f;
            delete.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
            [delete setTitle:@"删除" forState:UIControlStateNormal];
            [delete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [delete setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
            [delete addTarget:self action:@selector(Delete:) forControlEvents:UIControlEventTouchUpInside];
            [delete setHidden:YES];
            [left_table_background addSubview:delete];
            [viewArray2 addObject:delete];
            //-------隐藏--结束------
            
        }else{// 已经下载了
            
            // 显示词库信息
            UILabel *dictionary_information = [[UILabel alloc]initWithFrame:CGRectMake(10, 80, self.view.frame.size.width-20, self.view.frame.size.height-370)];
            NSString *dicDetial;
            if ([mydic.dictionary_detail compare:nil]==NSOrderedSame) {
                dicDetial = @" ";
            }else{
                dicDetial = mydic.dictionary_detail;
            }
            dictionary_information.text = [NSString stringWithFormat:@"%@",dicDetial];
            [dictionary_information setNumberOfLines:0];
            dictionary_information.lineBreakMode = UILineBreakModeWordWrap;
            dictionary_information.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            dictionary_information.textColor = [UIColor blackColor];
            dictionary_information.font = [UIFont fontWithName:@"Helvetica" size:15];
            [left_table_background addSubview:dictionary_information];
            [viewArray2 addObject:dictionary_information];
            
            
            
            
            // 设置完成时间
            UIImageView *setFinishTime = [[UIImageView alloc] initWithFrame:CGRectMake(10,self.view.frame.size.height-280,self.view.frame.size.width-20,90)];
            UIImage *time = [UIImage imageNamed:@"set_finish_time"];
            [setFinishTime initWithImage:time];
            
            //获得日期和词汇量
            DataBase *dateAndWord = [[DataBase alloc]init];
            NSString *days = [dateAndWord getCompleteTime:mydic];// 获得完成天数
            
            // 时间
            date = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 8.0, 260.0, 35.0)];
            
            //日背诵量
            count = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 50.0, 260.0, 35.0)];
            
            if ([days compare:nil]!=NSOrderedSame && [days compare:@""]!=NSOrderedSame && [days compare:@"0"]!=NSOrderedSame) {
                NSString *dayWords = [self dayWords:mydic finishDay:days];
                count.text = [NSString stringWithFormat:@"日背诵量: %@",dayWords];
                NSString *finishTime = [self getFinishDate:days];
                date.text = [NSString stringWithFormat:@"预计完成时间: %@",finishTime];
            }else{
                count.text = [NSString stringWithFormat:@"日背诵量: 25"];
                date.text = [NSString stringWithFormat:@"预计完成时间: %@",[self SuggestDate:mydic]];
            }
            
            date.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            date.textColor = [UIColor blackColor];
            date.font = [UIFont fontWithName:@"Helvetica" size:18];
            [setFinishTime addSubview:date];
            
            count.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            count.textColor = [UIColor blackColor];
            count.font = [UIFont fontWithName:@"Helvetica" size:18];
            [setFinishTime addSubview:count];
            
            setFinishTime.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ImageViewClick)];
            [setFinishTime addGestureRecognizer:singleTap];
            [singleTap release];
            
            [left_table_background addSubview:setFinishTime];
            [viewArray2 addObject:setFinishTime];
            
            
            
            
            UIButton *skim=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            skim.frame=CGRectMake(10,self.view.frame.size.height-170,self.view.frame.size.width-20,40);
            skim.tag = (long)mydic;
            skim.backgroundColor = buttonColor;
            skim.layer.cornerRadius = 3.0f;
            skim.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
            [skim setTitle:@"开始背诵" forState:UIControlStateNormal];
            [skim setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [skim setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
            [skim addTarget:self action:@selector(skimDic:) forControlEvents:UIControlEventTouchUpInside];
            [left_table_background addSubview:skim];
            [viewArray2 addObject:skim];
            
            
            
            
            UIButton *delete=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            delete.frame=CGRectMake(10,self.view.frame.size.height-120,self.view.frame.size.width-20,40);
            delete.tag = (long)mydic;
            delete.backgroundColor = buttonColor;
            delete.layer.cornerRadius = 3.0f;
            delete.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
            [delete setTitle:@"删除" forState:UIControlStateNormal];
            [delete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [delete setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
            [delete addTarget:self action:@selector(Delete:) forControlEvents:UIControlEventTouchUpInside];
            [left_table_background addSubview:delete];
            [viewArray2 addObject:delete];
            
        }
    }
    [iphoneTableDetailView addSubview:left_table_background];
    [self.view addSubview:iphoneTableDetailView];
    
    NSArray *array1 = [self.view subviews];
    for (int i = 0; i < [array1 count]; i++) {
        NSLog(@"1--%d-%@",i,[array1 objectAtIndex:i]);
    }
    
    NSArray *array2 = [[array1 objectAtIndex:1] subviews];
    for (int i = 0; i < [array2 count]; i++) {
        NSLog(@"2--%d-%@",i,[array2 objectAtIndex:i]);
    }
    
    NSArray *array3 = [[array2 objectAtIndex:1] subviews];
    for (int i = 0; i < [array3 count]; i++) {
        NSLog(@"3--%d-%@",i,[array3 objectAtIndex:i]);
    }
}

-(void)back_to_first:(id)sender{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [iphoneTableDetailView removeFromSuperview];
        [leftBackgroundView setHidden:NO];
    }else{
        [iphoneTableDetailView removeFromSuperview];
        [iphoneBackgroundView setHidden:NO];
    }
    
    
    if (view) {
        [view removeFromSuperview];
    }
    
}

// 创建词库详情视图_pad
-(void) DataPackgeDetailPad:(int) dicId uderDic:(UserDictionary*)mydic{
    
    dic = mydic;
    
    UIColor* buttonColor = [UIColor colorWithRed:204.0/255 green:41.0/255 blue:19.0/255 alpha:1.0f];
    
    num = dicId;
    NSString *dicNum = [NSString stringWithFormat:@"%d",num];
    [[NSUserDefaults standardUserDefaults] setObject:dicNum forKey:@"dicNum"];
    
    // 设置背景view
    iphoneTableDetailView = [[UIView alloc]initWithFrame:CGRectMake(0,0,300,self.view.frame.size.height)];
    iphoneTableDetailView.backgroundColor = [UIColor whiteColor];
    
    
    UIColor* backgroundColor = [UIColor colorWithRed:204.0/255 green:41.0/255 blue:19.0/255 alpha:1.0f];
    
    
    // 产生顶部bar
    UIView *top_bar_background = [[UIView alloc]initWithFrame:CGRectMake(0,0,300,65.5)];
    top_bar_background.backgroundColor=backgroundColor;
    
    
    
    // bar上面的title
    UILabel *bar_title = [[UILabel alloc]initWithFrame:CGRectMake(120.0, 30, 180, 30)];
    bar_title.font = [UIFont fontWithName:@"Avenir-Black" size:20.0f];
    bar_title.text = @"记忆盒子";
    [bar_title setTextColor:[UIColor whiteColor]];
    [top_bar_background addSubview:bar_title];
    
    
    
    // 顶部上面bar back button
    UIButton *top_refresh=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    top_refresh.frame=CGRectMake(10,30,50,30);
    UIImage *setting_image =[UIImage imageNamed:@"bac_top"];
    [top_refresh setBackgroundImage:setting_image forState:UIControlStateNormal];
    [top_refresh addTarget:self action:@selector(back_to_first:) forControlEvents:UIControlEventTouchUpInside];
    [top_bar_background addSubview:top_refresh];
    
    [iphoneTableDetailView addSubview:top_bar_background];
    
    
    // 详情背景
    UIView *left_table_background = [[UIView alloc]initWithFrame:CGRectMake(0,65.5,300,self.view.frame.size.height-70)];
    left_table_background.backgroundColor=[UIColor whiteColor];
    
    
    //显示词库名
    UILabel *dictionary_name = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 10.0,280, 30.0)];
    dictionary_name.text = [NSString stringWithFormat:@"%@",mydic.dictionary_name];
    dictionary_name.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    dictionary_name.textColor = [UIColor blackColor];
    dictionary_name.font = [UIFont fontWithName:@"Avenir-Black" size:20];
    [left_table_background addSubview:dictionary_name];
    [viewArray2 addObject:dictionary_name];
    
    
    // 词库总数量
    UILabel *dictionary_count = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 40.0, 280, 20.0)];
    dictionary_count.text = [NSString stringWithFormat:@"词库总数量:%@",mydic.count];
    dictionary_count.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    dictionary_count.textColor = [UIColor blackColor];
    dictionary_count.font = [UIFont fontWithName:@"Helvetica" size:17];
    [left_table_background addSubview:dictionary_count];
    [viewArray2 addObject:dictionary_count];
    
    
    //已完成数量
    UILabel *dictionary_finished_num = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 60.0, 280, 20.0)];
    DataBaseHelper *helper = [[DataBaseHelper alloc]init];
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    int finishWords = [helper getWordDayNotEqualZero:mydic.dictionary_id userId:user_id];
    dictionary_finished_num.text = [NSString stringWithFormat:@"已完成数量:%d",finishWords];
    dictionary_finished_num.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    dictionary_finished_num.textColor = [UIColor blackColor];
    dictionary_finished_num.font = [UIFont fontWithName:@"Helvetica" size:17];
    [left_table_background addSubview:dictionary_finished_num];
    [viewArray2 addObject:dictionary_finished_num];
    
    /*
     *判断词库文件夹路径是否存在
     */
    NSString *documentsDirectory2 = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    NSFileManager *fileManager2 = [NSFileManager defaultManager];
    NSString *filePath2 =  [documentsDirectory2 stringByAppendingPathComponent:mydic.dictionary_id];
    BOOL isDirExist = [fileManager2 fileExistsAtPath:filePath2];
    
    
    // 判断是否购买
    if (![mydic.buy compare:@"1"] == NSOrderedSame && !isDirExist) {// 没有购买
        // 购买
        UIButton *BuyDicBut=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        BuyDicBut.frame=CGRectMake(15,300,270,43);
        BuyDicBut.tag = (long)mydic;
        NSString *price = [NSString stringWithFormat:@"￥%@",mydic.dictionary_price];
        [BuyDicBut setTitle:price forState:UIControlStateNormal];
        
        BuyDicBut.backgroundColor = buttonColor;
        BuyDicBut.layer.cornerRadius = 3.0f;
        BuyDicBut.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:20.0f];
        [BuyDicBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [BuyDicBut setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
        [BuyDicBut addTarget:self action:@selector(Purchase:) forControlEvents:UIControlEventTouchUpInside];
        
        [left_table_background addSubview:BuyDicBut];
        [viewArray2 addObject:BuyDicBut];
        
        
        // 显示词库信息
        UILabel *dictionary_information = [[UILabel alloc]initWithFrame:CGRectMake(25, 350, 250, 200)];
        NSString *dicDetial;
        if ([mydic.dictionary_detail compare:nil]==NSOrderedSame) {
            dicDetial = @" ";
        }else{
            dicDetial = mydic.dictionary_detail;
        }
        dictionary_information.text = [NSString stringWithFormat:@"%@",dicDetial];
        [dictionary_information setNumberOfLines:0];
        dictionary_information.lineBreakMode = UILineBreakModeWordWrap;
        dictionary_information.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        dictionary_information.textColor = [UIColor blackColor];
        dictionary_information.font = [UIFont fontWithName:@"Helvetica" size:20];
        [left_table_background addSubview:dictionary_information];
        [viewArray2 addObject:dictionary_information];
        
        //-------隐藏--start----
        // 下载
        UIButton *download=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        download.frame=CGRectMake(15,300,270,43);
        download.tag = (long)mydic;
        download.backgroundColor = buttonColor;
        download.layer.cornerRadius = 3.0f;
        download.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
        [download setTitle:@"下载" forState:UIControlStateNormal];
        [download setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [download setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
        [download addTarget:self action:@selector(downloaddic:) forControlEvents:UIControlEventTouchUpInside];
        [download setHidden:YES];
        [left_table_background addSubview:download];
        [viewArray2 addObject:download];
        
        
        // 设置发音
        UIImageView *set_tone = [[UIImageView alloc] initWithFrame:CGRectMake(15,200,273,50)];
        UIImage *tone = [UIImage imageNamed:@"set_tone"];
        [set_tone initWithImage:tone];
        
        DataBase *pronucations = [[DataBase alloc]init];
        NSString *pronucation = [pronucations getCurrentDictionaryPronounce:mydic];
        // 转换器-设置发音
        UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(190, 12, 20, 10)];
        [set_tone setHidden:YES];
        if ([pronucation compare:@"US"] == NSOrderedSame) {
            [switchButton setOn:YES];
        }else{
            [switchButton setOn:NO];
        }
        [switchButton addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
        [set_tone addSubview:switchButton];
        [set_tone bringSubviewToFront:switchButton];
        [set_tone setUserInteractionEnabled:YES];
        [left_table_background addSubview:set_tone];
        [viewArray2 addObject:set_tone];
        
        // 设置完成时间
        UIImageView *setFinishTime = [[UIImageView alloc] initWithFrame:CGRectMake(15,270,273,90)];
        UIImage *time = [UIImage imageNamed:@"set_finish_time"];
        [setFinishTime initWithImage:time];
        
        //获得日期和词汇量
        DataBase *dateAndWord = [[DataBase alloc]init];
        NSString *days = [dateAndWord getCompleteTime:mydic];
        
        // 时间
        date = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 8.0, 260.0, 35.0)];
        
        //日背诵量
        count = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 50.0, 260.0, 35.0)];
        
        if ([days compare:nil]!=NSOrderedSame && [days compare:@""]!=NSOrderedSame && [days compare:@"0"]!=NSOrderedSame) {
            NSString *dayWords = [self dayWords:mydic finishDay:days];
            count.text = [NSString stringWithFormat:@"日背诵量: %@",dayWords];
            NSString *finishTime = [self getFinishDate:days];
            date.text = [NSString stringWithFormat:@"预计完成时间: %@",finishTime];
        }else{
            count.text = [NSString stringWithFormat:@"日背诵量: 25"];
            date.text = [NSString stringWithFormat:@"预计完成时间: %@",[self SuggestDate:dic]];
        }
        
        date.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        date.textColor = [UIColor blackColor];
        date.font = [UIFont fontWithName:@"Helvetica" size:18];
        [setFinishTime addSubview:date];
        
        count.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        count.textColor = [UIColor blackColor];
        count.font = [UIFont fontWithName:@"Helvetica" size:18];
        [setFinishTime addSubview:count];
        
        setFinishTime.userInteractionEnabled = YES;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ImageViewClick)];
        [setFinishTime addGestureRecognizer:singleTap];
        [singleTap release];
        
        [setFinishTime setHidden:YES];
        [left_table_background addSubview:setFinishTime];
        [viewArray2 addObject:setFinishTime];
        
        // 背诵当个词库
        UIButton *skim=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        skim.frame=CGRectMake(15,600,270,55);
        skim.tag = (long)mydic;
        skim.backgroundColor = buttonColor;
        skim.layer.cornerRadius = 3.0f;
        skim.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
        [skim setTitle:@"开始背诵" forState:UIControlStateNormal];
        [skim setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [skim setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
        [skim addTarget:self action:@selector(skimDic:) forControlEvents:UIControlEventTouchUpInside];
        [skim setHidden:YES];
        [left_table_background addSubview:skim];
        [viewArray2 addObject:skim];
        
        
        
        // 删除
        UIButton *delete=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        delete.frame=CGRectMake(15,690,270,43);
        delete.tag = (long)mydic;
        delete.backgroundColor = buttonColor;
        delete.layer.cornerRadius = 3.0f;
        delete.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
        [delete setTitle:@"删除" forState:UIControlStateNormal];
        [delete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [delete setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
        [delete addTarget:self action:@selector(Delete:) forControlEvents:UIControlEventTouchUpInside];
        [delete setHidden:YES];
        [left_table_background addSubview:delete];
        [viewArray2 addObject:delete];

        
        
        //-------隐藏--结束------
        
    }else{// 已经购买
        /*
         *获得文件名
         */
        NSString *dicName = [NSString stringWithFormat:@"%@.zip",mydic.dictionary_id];
        
        /*
         *判断词库文件夹路径是否存在
         */
        NSString *documentsDirectory2 = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
        NSFileManager *fileManager2 = [NSFileManager defaultManager];
        NSString *filePath2 =  [documentsDirectory2 stringByAppendingPathComponent:mydic.dictionary_id];
        BOOL isDirExist = [fileManager2 fileExistsAtPath:filePath2];
        
        /*
         *获得压缩文件是否存在
         */
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:dicName];
        BOOL isFileExist = [fileManager fileExistsAtPath:filePath];
        if((!isFileExist)&&(!isDirExist)) //如果压缩文件不存在并且词库文件夹不在
        {
            
            // 显示词库信息
            UILabel *dictionary_information = [[UILabel alloc]initWithFrame:CGRectMake(10, 80,280, self.view.frame.size.height-800)];
            NSString *dicDetial;
            if ([mydic.dictionary_detail compare:nil]==NSOrderedSame) {
                dicDetial = @" ";
            }else{
                dicDetial = mydic.dictionary_detail;
            }
            dictionary_information.text = [NSString stringWithFormat:@"%@",dicDetial];
            [dictionary_information setNumberOfLines:0];
            dictionary_information.lineBreakMode = UILineBreakModeWordWrap;
            dictionary_information.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            dictionary_information.textColor = [UIColor blackColor];
            dictionary_information.font = [UIFont fontWithName:@"Helvetica" size:15];
            [left_table_background addSubview:dictionary_information];
            [viewArray2 addObject:dictionary_information];
            
            
            
            
            // 下载
            UIButton *download=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            download.frame=CGRectMake(10,320,280,43);
            download.tag = (long)mydic;
            download.backgroundColor = buttonColor;
            download.layer.cornerRadius = 3.0f;
            download.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
            [download setTitle:@"下载" forState:UIControlStateNormal];
            [download setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [download setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
            [download addTarget:self action:@selector(downloaddic:) forControlEvents:UIControlEventTouchUpInside];
            
            [left_table_background addSubview:download];
            [viewArray2 addObject:download];
            
            
            
            // 设置完成时间
            UIImageView *setFinishTime = [[UIImageView alloc] initWithFrame:CGRectMake(10,320,280,90)];
            UIImage *time = [UIImage imageNamed:@"set_finish_time"];
            [setFinishTime initWithImage:time];
            
            //获得日期和词汇量
            DataBase *dateAndWord = [[DataBase alloc]init];
            NSString *days = [dateAndWord getCompleteTime:mydic];
            
            // 时间
            date = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 8.0, 260.0, 35.0)];
            
            //日背诵量
            count = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 50.0, 260.0, 35.0)];
            
            if ([days compare:nil]!=NSOrderedSame && [days compare:@""]!=NSOrderedSame && [days compare:@"0"]!=NSOrderedSame) {
                NSString *dayWords = [self dayWords:mydic finishDay:days];
                count.text = [NSString stringWithFormat:@"日背诵量: %@",dayWords];
                NSString *finishTime = [self getFinishDate:days];
                date.text = [NSString stringWithFormat:@"预计完成时间: %@",finishTime];
            }else{
                count.text = [NSString stringWithFormat:@"日背诵量: 25"];
                date.text = [NSString stringWithFormat:@"预计完成时间: %@",[self SuggestDate:mydic]];
            }
            
            date.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            date.textColor = [UIColor blackColor];
            date.font = [UIFont fontWithName:@"Helvetica" size:18];
            [setFinishTime addSubview:date];
            
            count.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            count.textColor = [UIColor blackColor];
            count.font = [UIFont fontWithName:@"Helvetica" size:18];
            [setFinishTime addSubview:count];
            
            setFinishTime.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ImageViewClick)];
            [setFinishTime addGestureRecognizer:singleTap];
            [singleTap release];
            
            [setFinishTime setHidden:YES];
            [left_table_background addSubview:setFinishTime];
            [viewArray2 addObject:setFinishTime];
            
            
            
            
            
            
            // 背诵当个词库
            UIButton *skim=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            skim.frame=CGRectMake(10,470,280,40);
            skim.tag = dicId;
            skim.backgroundColor = buttonColor;
            skim.layer.cornerRadius = 3.0f;
            skim.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
            [skim setTitle:@"开始背诵" forState:UIControlStateNormal];
            [skim setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [skim setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
            [skim addTarget:self action:@selector(skimDic:) forControlEvents:UIControlEventTouchUpInside];
            [skim setHidden:YES];
            [left_table_background addSubview:skim];
            [viewArray2 addObject:skim];
            
            
            
            // 删除
            UIButton *delete=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            delete.frame=CGRectMake(10,550,280,40);
            delete.tag = (long)mydic;
            delete.backgroundColor = buttonColor;
            delete.layer.cornerRadius = 3.0f;
            delete.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
            [delete setTitle:@"删除" forState:UIControlStateNormal];
            [delete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [delete setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
            [delete addTarget:self action:@selector(Delete:) forControlEvents:UIControlEventTouchUpInside];
            [delete setHidden:YES];
            [left_table_background addSubview:delete];
            [viewArray2 addObject:delete];
            //-------隐藏--结束------
            
        }else{// 已经下载了
            
            // 显示词库信息
            UILabel *dictionary_information = [[UILabel alloc]initWithFrame:CGRectMake(10, 80,280, self.view.frame.size.height-800)];
            NSString *dicDetial;
            if ([mydic.dictionary_detail compare:nil]==NSOrderedSame) {
                dicDetial = @" ";
            }else{
                dicDetial = mydic.dictionary_detail;
            }
            dictionary_information.text = [NSString stringWithFormat:@"%@",dicDetial];
            [dictionary_information setNumberOfLines:0];
            dictionary_information.lineBreakMode = UILineBreakModeWordWrap;
            dictionary_information.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            dictionary_information.textColor = [UIColor blackColor];
            dictionary_information.font = [UIFont fontWithName:@"Helvetica" size:15];
            [left_table_background addSubview:dictionary_information];
            [viewArray2 addObject:dictionary_information];
            
            
            
            
            // 设置完成时间
            UIImageView *setFinishTime = [[UIImageView alloc] initWithFrame:CGRectMake(10,320,280,90)];
            UIImage *time = [UIImage imageNamed:@"set_finish_time"];
            [setFinishTime initWithImage:time];
            
            //获得日期和词汇量
            DataBase *dateAndWord = [[DataBase alloc]init];
            NSString *days = [dateAndWord getCompleteTime:mydic];// 获得完成天数
            
            // 时间
            date = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 8.0, 260.0, 35.0)];
            
            //日背诵量
            count = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 50.0, 260.0, 35.0)];
            
            if ([days compare:nil]!=NSOrderedSame && [days compare:@""]!=NSOrderedSame && [days compare:@"0"]!=NSOrderedSame) {
                NSString *dayWords = [self dayWords:mydic finishDay:days];
                count.text = [NSString stringWithFormat:@"日背诵量: %@",dayWords];
                NSString *finishTime = [self getFinishDate:days];
                date.text = [NSString stringWithFormat:@"预计完成时间: %@",finishTime];
            }else{
                count.text = [NSString stringWithFormat:@"日背诵量: 25"];
                date.text = [NSString stringWithFormat:@"预计完成时间: %@",[self SuggestDate:mydic]];
            }
            
            date.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            date.textColor = [UIColor blackColor];
            date.font = [UIFont fontWithName:@"Helvetica" size:18];
            [setFinishTime addSubview:date];
            
            count.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            count.textColor = [UIColor blackColor];
            count.font = [UIFont fontWithName:@"Helvetica" size:18];
            [setFinishTime addSubview:count];
            
            setFinishTime.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ImageViewClick)];
            [setFinishTime addGestureRecognizer:singleTap];
            [singleTap release];
            
            [left_table_background addSubview:setFinishTime];
            [viewArray2 addObject:setFinishTime];
            
            
            
            
            UIButton *skim=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            skim.frame=CGRectMake(10,470,280,40);
            skim.tag = dicId;
            skim.backgroundColor = buttonColor;
            skim.layer.cornerRadius = 3.0f;
            skim.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
            [skim setTitle:@"开始背诵" forState:UIControlStateNormal];
            [skim setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [skim setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
            [skim addTarget:self action:@selector(skimDic:) forControlEvents:UIControlEventTouchUpInside];
            [left_table_background addSubview:skim];
            [viewArray2 addObject:skim];
            
            
            
            
            UIButton *delete=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            delete.frame=CGRectMake(10,550,280,40);
            delete.tag = (long)mydic;
            delete.backgroundColor = buttonColor;
            delete.layer.cornerRadius = 3.0f;
            delete.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
            [delete setTitle:@"删除" forState:UIControlStateNormal];
            [delete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [delete setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
            [delete addTarget:self action:@selector(Delete:) forControlEvents:UIControlEventTouchUpInside];
            [left_table_background addSubview:delete];
            [viewArray2 addObject:delete];
            
        }
    }
    [iphoneTableDetailView addSubview:left_table_background];
    [self.view addSubview:iphoneTableDetailView];
}






// =================下拉刷新方法开始================
#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	_reloading = YES;
}

- (void)doneLoadingTableViewData{
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark EGORefreshTableHeaderDelegate Methods

//下拉到一定距离，手指放开时调用
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    [self iphone_releaseDialog]; // 注销dialog
	[self reloadTableViewDataSource];
    
    //停止加载，弹回下拉
	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:0.5];
    
    if (_barView == nil) {
        UIImage *img = [[UIImage imageNamed:@"timeline_new_status_background2.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
        _barView = [[UIImageView alloc] initWithImage:img];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            _barView.frame = CGRectMake(1, 70, 298, 40);
        }else{
            _barView.frame = CGRectMake((self.view.frame.size.width-298)/2, 70, 298, 40);
        }
        [self.view addSubview:_barView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.tag = 100;
        label.font = [UIFont systemFontOfSize:16.0f];
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        [_barView addSubview:label];
    }
    UILabel *label = (UILabel *)[_barView viewWithTag:100];
    if (![tool isExistenceNetwork]) {
        label.text = [NSString stringWithFormat:@"没有网络"];
    }else{
        label.text = [NSString stringWithFormat:@"同步数据"];
    }
    [label sizeToFit];
    CGRect frame = label.frame;
    frame.origin = CGPointMake((_barView.frame.size.width - frame.size.width)/2, (_barView.frame.size.height - frame.size.height)/2);
    label.frame = frame;
    
    
    [self performSelector:@selector(updateUI) withObject:nil afterDelay:2.0];
    
}

- (void)updateUI {
    [UIView animateWithDuration:0.6 animations:^{
        CGRect frame = _barView.frame;
        frame.origin.y = 5;
        _barView.frame = frame;
    } completion:^(BOOL finished){
        if (finished) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:1.0];
            [UIView setAnimationDuration:0.6];
            CGRect frame = _barView.frame;
            frame.origin.y = -40;
            _barView.frame = frame;
            [UIView commitAnimations];
            [_barView removeFromSuperview];
        }
    }];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"msgcome" ofType:@"wav"];
    NSURL *url = [NSURL fileURLWithPath:path];
    SystemSoundID soundId;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &soundId);
    AudioServicesPlaySystemSound(soundId);
    
    tool = [[tools alloc]init];
    // 同步网络数据
    if ([tool isExistenceNetwork]) {
        // 获得有二级结构的词库列表
        doubleStruct = [api getUserHadBuyDictionary2SencondList:userName userId:user.user_id];
        [tool saveArray:doubleStruct saveKey:@"doubleStruct"];// 保存二级菜单数组
        
        dicPause = [[NSMutableArray alloc]init];
        
        for (int i =0; i < [doubleStruct count]; i++) {
            secondList *list = [doubleStruct objectAtIndex:i];
            for (int j = 0; j < [list.secondLevel count]; j++) {
                UserDictionary *secondDic = [list.secondLevel objectAtIndex:j];
                DataBaseHelper *PrecentNum = [[DataBaseHelper alloc]init];
                BOOL use = [PrecentNum isDictionaryPause:secondDic.dictionary_id userId:user.user_id];
                if (use) {
                    [dicPause addObject:@"1"];
                }else{
                    [dicPause addObject:@"0"];
                }
            }
        }
        // -------获得保存的词库和 词库的启用状态信息-------start
        [tool saveArray:dicPause saveKey:@"usedDicsArray"];// 保存词库的使用和停止状态
        
        NSString *user_name = (NSString*)[tool readObkect:@"user_name"];
        NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
        // 获得有二级结构的词库列表
        doubleStruct = [api getUserHadBuyDictionary2SencondList:user_name userId:user_id];
        [tool saveArray:doubleStruct saveKey:@"doubleStruct"];// 保存二级菜单数组
        
        //重新加载数据
//        for (int i = 0; i<[viewArray1 count]; i++) {
//            [[viewArray1 objectAtIndex:i] removeFromSuperview];
//        }
        [_tableView removeFromSuperview];
        [self DataPackgeList];
        [self.view bringSubviewToFront:_barView];
    }else{
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"刷新失败" message:@"网络原因导致刷新失败，请联网后重试！" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil,nil];
        [alert show];
    }
    
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	return _reloading; // should return if data source model is reloading
}

//取得下拉刷新的时间
- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	return [NSDate date]; // should return date data source was last changed
}

//===============下拉刷新方法结束===========






// ==============二级菜单相应事件===========start
// 设置完成时间图片监听事件
-(void)ImageViewClick{
    
    UserDictionary *OneDic = [tool indexToDic];

    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
       view = [[UIView alloc] initWithFrame:CGRectMake(0, 340, 300, 400)];
    }else{
        view = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-220, 320, self.view.frame.size.height-260)];

    }
    [view setBackgroundColor:[UIColor whiteColor]];
    
    
    
    // 初始化UIDatePicker
    UIDatePicker *datePicker;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, -20, 300, 230)];
    }else{
        datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, -20, 320, 230)];
    }
    
    // 设置时区
    [datePicker setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    // 设置当前显示时间
    NSDate *date1 = [NSDate date];
    date1 = [date1 dateByAddingTimeInterval: 14*3600*24];
    // 设置当前时间为最小时间
    [datePicker setMinimumDate:date1];
    
    NSDate *date2 = [NSDate date];
    DataBase *dateAndWord = [[DataBase alloc]init];
    NSString *days1 = [dateAndWord getCompleteTime:OneDic];
    [dateAndWord release];
    int days2 = [days1 intValue];
    date2 = [date2 dateByAddingTimeInterval: days2*3600*24];//增加天数
    
    [datePicker setDate:date2 animated:YES];
    
    // 设置UIDatePicker的显示模式
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    // 当值发生改变的时候调用的方法
    [datePicker addTarget:self action:@selector(timeChange:) forControlEvents:UIControlEventValueChanged];
    [view addSubview:datePicker];
    
    
    
    
    UIColor* buttonColor = [UIColor colorWithRed:204.0/255 green:41.0/255 blue:19.0/255 alpha:1.0f];
    // 确定修改
    UIButton *modify=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        modify.frame=CGRectMake(20,175,260,40);
    }else{
        modify.frame=CGRectMake(20,175,280,40);
    }
    modify.tag = (long)view;
    modify.backgroundColor = buttonColor;
    modify.layer.cornerRadius = 3.0f;
    modify.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
    [modify setTitle:@"确定修改" forState:UIControlStateNormal];
    [modify setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [modify setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
    [modify addTarget:self action:@selector(ModifySure:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:modify];
    
    
    [self.view addSubview:view];
}

// 时间选择响应方法
-(void)timeChange:(UIDatePicker *)datePicker{
    UserDictionary *ModifyDic = [tool indexToDic];

    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"newUser"];
    NSDateFormatter *formatter = [[NSDateFormatter  alloc]init];
    formatter.dateFormat = @"yyyy年MM月dd日";
    NSString *text = [formatter stringFromDate:datePicker.date];
    [formatter release];
    NSString *choiceDate = [NSString stringWithFormat:@"%@",text];
    NSString *finishdate = [NSString stringWithFormat:@"预计完成时间: %@",choiceDate];
    date.text = finishdate; // 设置显示的完成时间
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:text forKey:@"setfinishDate"]; // 保存设置的完成时间
    
    int words = [self DayRemember:text modifyDic:ModifyDic];
    NSString *stringInt = [NSString stringWithFormat:@"%d",words];
    NSString *dayWordsNum = [NSString stringWithFormat:@"日背诵量:%@",stringInt];
    count.text = dayWordsNum;
    [userDefaults setObject:stringInt forKey:@"setdayWords"];
    
    Algorithm *alg = [[Algorithm alloc]init];
    [alg DeleteWordFromArray:ModifyDic];
    
    [newDownloadDictionaryArray addObject:ModifyDic];
}

// 计算每日背诵量
-(int)DayRemember:(NSString *)finishDate modifyDic:(UserDictionary *) ModifyDic{
    DataBase *wordCount =[[DataBase alloc]init];
    NSString *AllWordscount = [wordCount getDictionaryWordCount:ModifyDic];
    [wordCount release];
    int intString = [AllWordscount intValue];
    
    //获得时间差
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
    NSDate *date1=[dateFormatter dateFromString:finishDate];
    NSDate * date2=[NSDate date];
    NSCalendar * cal=[NSCalendar currentCalendar];
    NSUInteger unitFlags=NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit;
    NSDateComponents * conponent= [cal components:unitFlags fromDate:date2];
    NSInteger year=[conponent year];
    NSInteger month=[conponent month];
    NSInteger day=[conponent day];
    NSString * nsDateString= [NSString stringWithFormat:@"%d年%d月%d日",year,month,day];
    NSDate *date3=[dateFormatter dateFromString:nsDateString];
    [dateFormatter release];
    NSTimeInterval time=[date1 timeIntervalSinceDate:date3];
    
    int days=((int)time)/(3600*24);
    
    int words = 25;
    
    if (days < 14) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"警告" message:@"请务必选择大于14天的背诵时间，以帮助你更好的使用我们的记忆方法!" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }else{
        if (days <= 14) {
            days = 15;
        }
        
        words = intString/(days - 14);
        if (words < 0) {
            words = 25;
        }
        
        // 设置词库背诵天数
        NSString *finishDays = [NSString stringWithFormat:@"%d",days];
        DataBase *dateAndWord = [[DataBase alloc]init];
        [dateAndWord setDictionaryCompleteTime:finishDays dictioanry:ModifyDic];
        [dateAndWord release];
    }
    return words;
}

// 初始完成日期和词汇量
-(NSString*)SuggestDate:(UserDictionary *)userDic{
    DataBase *wordCount =[[DataBase alloc]init];
    NSString *AllWordscount = [wordCount getDictionaryWordCount:dic];// 得到该词库的所有单词数量
    int intString = [AllWordscount intValue];
    
    int days = intString/25+14;
    
    //设置新的时间
    NSDate * senddate=[NSDate date];
    senddate = [senddate dateByAddingTimeInterval: days*3600*24];//增加天数
    NSCalendar * cal=[NSCalendar currentCalendar];
    NSUInteger unitFlags=NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit;
    NSDateComponents * conponent= [cal components:unitFlags fromDate:senddate];
    NSInteger year=[conponent year];
    NSInteger month=[conponent month];
    NSInteger day=[conponent day];
    NSString *nsDateString= [NSString stringWithFormat:@"%4d年%2d月%d日",year,month,day];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:nsDateString forKey:@"setfinishDate"];
    
    NSString *stringInt = [NSString stringWithFormat:@"%d",25];
    [userDefaults setObject:stringInt forKey:@"setdayWords"];
    
    // 给词库添加完成天数
    [wordCount setDictionaryCompleteTime:@"25" dictioanry:userDic];
    [wordCount release];
    return nsDateString;
}

// 计算完成日期
-(NSString*)getFinishDate:(NSString *) finishDays{
    int days = [finishDays intValue];
    //设置新的时间
    NSDate * senddate=[NSDate date];
    senddate = [senddate dateByAddingTimeInterval: days*3600*24];//增加天数
    NSCalendar * cal=[NSCalendar currentCalendar];
    NSUInteger unitFlags=NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit;
    NSDateComponents * conponent= [cal components:unitFlags fromDate:senddate];
    NSInteger year=[conponent year];
    NSInteger month=[conponent month];
    NSInteger day=[conponent day];
    NSString *nsDateString= [NSString stringWithFormat:@"%4d年%2d月%d日",year,month,day];
    return nsDateString;
}

// 根据天数计算每天背诵词汇量
-(NSString*)dayWords:(UserDictionary*) mydic finishDay: (NSString *)finish_day{
    DataBase *wordCount =[[DataBase alloc]init];
    NSString *AllWordscount = [wordCount getDictionaryWordCount:mydic];//得到词库所有单词数量
    [wordCount release];
    int intString = [AllWordscount intValue];
    int finishday = [finish_day intValue];// 完成的天数
    if (finishday == 0) {
        return 0;
    }else{
        if (finishday <= 15) {
            finishday = 15;
        }
        int day = intString/(finishday-14); // 日背诵量 = (总单词数量)/(计划完成的天数-14)
        NSString *days = [NSString stringWithFormat:@"%d",day];
        return days;
    }
}

// 修改时间确定界面
-(void) ModifySure:(id)sender{
    [view removeFromSuperview];
}

// 词库状态改变监听事件
-(void)DicState:(id)sender{
    UserDictionary *indexDic = (UserDictionary*)[sender tag];
    DataBaseHelper *UseDic = [[DataBaseHelper alloc]init];
    BOOL used = [UseDic isDictionaryPause:indexDic.dictionary_id userId:user.user_id];
    if (used) {
        UIImage *refresh_image =[UIImage imageNamed:@"left_list_logo"];
        [sender setBackgroundImage:refresh_image forState:UIControlStateNormal];
        [UseDic unPauseDictionary:indexDic.dictionary_id userId:user.user_id];
    }else{
        UIImage *refresh_image =[UIImage imageNamed:@"dic_unusered"];
        [sender setBackgroundImage:refresh_image forState:UIControlStateNormal];
        [UseDic pauseDictionary :indexDic.dictionary_id userId:user.user_id];
    }
}

// 刷新列表响应事件
-(void)refreshTable:(id) sender{
    NSString *user_name = (NSString*)[tool readObkect:@"user_name"];
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    NSLog(@"--name = %@",user_name);
    NSLog(@"---id = %@",user_id);
    // 获得有二级结构的词库列表
    doubleStruct = [api getUserHadBuyDictionary2SencondList:user_name userId:user_id];
    [tool saveArray:doubleStruct saveKey:@"doubleStruct"];// 保存二级菜单数组
    
    [_tableView removeFromSuperview];
    [self DataPackgeList];
    [self.view bringSubviewToFront:_barView];
}




// 下载前提醒网络使用
-(void)downloaddic:(id) sender{
    record = sender;
    //设置提示窗口
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"下载提示" message:@"程序将使用你的网络下载数据包!" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:@"确定" ,nil];
    alert.tag = 1;
    [alert show];
    [alert release];
}

// 下载事件
-(void)Download{
    _doneAnything = true;
    for (int i = 0; i<[viewArray1 count]; i++) {
        [[viewArray1 objectAtIndex:i] removeFromSuperview];
    }
    tools *tool2 = [[tools alloc]init];
    if ([tool2 isExistenceNetwork]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"afterBuy"];
        /*
         *判断词库文件夹压缩包和路径是否存在
         */
        [record setHidden:YES];
        
        UIProgressView * progress = [ [ UIProgressView   alloc ]
                                     initWithFrame:CGRectMake(20,380,260,43)];
        progress.progressViewStyle= UIProgressViewStyleDefault;
        [self.view addSubview:progress];
        [viewArray2 addObject:progress];
        [progress release];
        UILabel *process = [[UILabel alloc]initWithFrame:CGRectMake(20,340,260,43)];
        NSString *process2 = [NSString stringWithFormat:@"下载中...%.1f%%",[progress progress]];
        process.text =process2;
        [self.view addSubview:process];
        [viewArray2 addObject:process];
        [process release];
        
        dic = [tool indexToDic];
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
        NSFileManager *fileManager = [NSFileManager defaultManager];//文件
        
        NSString *dicName = [NSString stringWithFormat:@"%@.zip",dic.dictionary_id];
        NSString *filePath2 =  [documentsDirectory stringByAppendingPathComponent:dicName];
        BOOL isDirExist = [fileManager fileExistsAtPath:filePath2];
        
        NSString *stringInt = [NSString stringWithFormat:@"%@",dic.dictionary_id];
        NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:stringInt];
        BOOL filedDirExist = [fileManager fileExistsAtPath:filePath];

        if (filedDirExist||isDirExist) {
            
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"downLoading"];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:stringInt forKey:@"downLoadNum"];
            /*
             *调用下载词库接口
             */
            [self Loading];
            NSArray *ui = [self.view subviews];
            NSString *stringInt3 = [NSString stringWithFormat:@"%@",dic.dictionary_id];
            [api downloadDictionary:stringInt3 processView:progress Label:process myArray:ui];
        }
        
    }else{
        //设置提示窗口
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"网络提示" message:@"请联网后下载" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    [tool2 release];
}

// 加载圈
-(void)Loading{
    //显示加载等待框
    self.progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.progressHUD];
    [_progressHUD release];
    [self.view bringSubviewToFront:self.progressHUD];
    self.progressHUD.delegate = self;
    //    self.progressHUD.labelText = @"同步中...";
    [self.progressHUD show:YES];
}

// 加载圈
-(void)Loading2{
    //显示加载等待框
    self.progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.progressHUD];
    [_progressHUD release];
    [self.view bringSubviewToFront:self.progressHUD];
    self.progressHUD.delegate = self;
    self.progressHUD.labelText = @"初始化本日单词...";
    [self.progressHUD show:YES];
}

-(void)EnableDictionary2:(id)sender{
    DataBaseHelper *help = [[DataBaseHelper alloc]init];
    [help sync:user.user_id];
    [help release];
    [self finishLoading];
    
}

//取消加载
-(void)finishLoading{
    if (self.progressHUD){
        [self.progressHUD removeFromSuperview];
        [self.progressHUD release];
        self.progressHUD = nil;
    }
}

// 删除事件
-(void)Delete:(id) sender{
    UserDictionary *secondDic = [tool indexToDic];
    NSString *dicName = [NSString stringWithFormat:@"你将删除词库:%@",secondDic.dictionary_name];
    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"警告" message:dicName delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定",nil];
    [alert show];
    [alert release];
}

//下载成功回调函数
-(void)downLoadSuccess:(NSArray*)viewArray{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"downLoading"];

    [self ImportData];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:nil forKey:@"setfinishDate"];// 设置默认完成时间为空
    
    NSString *wordNum = [NSString stringWithFormat:@"%d",25];//设置默认背诵词汇量为25.
    [userDefaults setObject:wordNum forKey:@"setdayWords"];
    
    DataBase *database = [[DataBase alloc]init];
    NSString *AllWordscount = [database getDictionaryWordCount:dic];
    
    //---------------
    //获得日期和词汇量
    NSString *days = [database getCompleteTime:dic];// 获得完成天数
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        NSArray *dicArray = [[viewArray objectAtIndex:2] subviews];
        NSArray *thirdArray = [[dicArray objectAtIndex:1] subviews];
        // 更新词库总数
        UILabel *lable = [thirdArray objectAtIndex:1];
        lable.text = [NSString stringWithFormat:@"词库总数量:%@",AllWordscount];
        
        
        
        // 更新时间设置
        UIImageView *TimeimageView = [thirdArray objectAtIndex:5];
        [TimeimageView setHidden:NO];
        NSArray *array = [TimeimageView subviews];
        
        if ([days compare:nil]!=NSOrderedSame && [days compare:@""]!=NSOrderedSame && [days compare:@"0"]!=NSOrderedSame) {
            NSString *dayWords = [self dayWords:dic finishDay:days];
            UILabel *day_word = [array objectAtIndex:1];
            day_word.text = [NSString stringWithFormat:@"日背诵量: %@",dayWords];
            NSString *finishTime = [self getFinishDate:days];
            UILabel *finish_time = [array objectAtIndex:0];
            finish_time.text = [NSString stringWithFormat:@"预计完成时间: %@",finishTime];
        }
        
        //---------------
        
        // 更新背诵按钮
        UIButton * button = [thirdArray objectAtIndex:6];
        [button setHidden:NO];
        
        // 更新删除按钮
        UIButton * button2 = [thirdArray objectAtIndex:7];
        [button2 setHidden:NO];
        
        // 屏蔽
        UILabel *lable2 = [viewArray objectAtIndex:4];
        [lable2 removeFromSuperview];
        
        UIProgressView *progress = [viewArray objectAtIndex:3];
        [progress removeFromSuperview];
        
        MBProgressHUD *progress2 = [viewArray objectAtIndex:5];
        [progress2 removeFromSuperview];
    }else{

        NSArray *dicArray = [[viewArray objectAtIndex:1] subviews];

        NSArray *thirdArray = [[dicArray objectAtIndex:1] subviews];

        // 更新词库总数
        UILabel *lable = [thirdArray objectAtIndex:1];
        lable.text = [NSString stringWithFormat:@"词库总数量:%@",AllWordscount];
        
        
        // 更新时间设置
        UIImageView *TimeimageView = [thirdArray objectAtIndex:5];
        [TimeimageView setHidden:NO];
        NSArray *array = [TimeimageView subviews];
        
        if ([days compare:nil]!=NSOrderedSame && [days compare:@""]!=NSOrderedSame && [days compare:@"0"]!=NSOrderedSame) {
            NSString *dayWords = [self dayWords:dic finishDay:days];
            UILabel *day_word = [array objectAtIndex:1];
            day_word.text = [NSString stringWithFormat:@"日背诵量: %@",dayWords];
            NSString *finishTime = [self getFinishDate:days];
            UILabel *finish_time = [array objectAtIndex:0];
            finish_time.text = [NSString stringWithFormat:@"预计完成时间: %@",finishTime];
        }
        
        //---------------
        
        // 更新背诵按钮
        UIButton * button = [thirdArray objectAtIndex:6];
        [button setHidden:NO];
        
        // 更新删除按钮
        UIButton * button2 = [thirdArray objectAtIndex:7];
        [button2 setHidden:NO];
        
        // 屏蔽
        UILabel *lable2 = [viewArray objectAtIndex:3];
        [lable2 removeFromSuperview];
        
        UIProgressView *progress = [viewArray objectAtIndex:2];
        [progress removeFromSuperview];
        
        MBProgressHUD *progress2 = [viewArray objectAtIndex:4];
        [progress2 removeFromSuperview];
    }
    
    

}

// 下载成功后立刻解析xml数据到数据库中
-(void)ImportData{
    
    /*
     *解压数据包
     */
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    ServiceApi *api =[[ServiceApi alloc]init];
    dic = [tool indexToDic];
    NSString *stringInt = [NSString stringWithFormat:@"%@",dic.dictionary_id];
    
    [api upZip:stringInt];
    [api release];
    /*
     *导入数据到数据库
     */
    
    DataBaseHelper *db = [[DataBaseHelper alloc]init];
    //        [db initDictoinaryTable];//初始化表结构
    [db importWordXMLToDB:dic userId:user_id];//把id为1的词库插入数据库
    
    //默认开启词库
    [db unPauseDictionary:dic.dictionary_id userId:user_id];
    
    [db release];
    [tool refreshDicsStatus];
    [self finishLoading];
    [newDownloadDictionaryArray addObject:dic];
}

//下载失败回调函数
-(void)downLoadFailed:(NSArray*)viewArray{
    
    if ([viewArray count] == 22) {
        MBProgressHUD *progress2 = [viewArray objectAtIndex:4];
        [progress2 removeFromSuperview];
    }else{
        // 屏蔽
        UILabel *lable2 = [viewArray objectAtIndex:3];
        [lable2 setHidden:YES];
        
        UIProgressView *progress = [viewArray objectAtIndex:2];
        [progress setHidden:YES];
        
        MBProgressHUD *progress2 = [viewArray objectAtIndex:4];
        [progress2 removeFromSuperview];
    }
    dic = [tool indexToDic];
    NSString *stringInt = [NSString stringWithFormat:@"%@",dic.dictionary_id];
    [api removeDictionary:stringInt];
    
    
//    //设置提示窗口
//    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"下载提示" message:@"下载失败" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定",nil];
//    alert.tag = 3;
//    [alert show];
//    [alert release];
//    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"downLoading"];
}


// ==============二级菜单相应事件===========end





// ===============购买=============start
// 购买请求响应事件
-(void)Purchase:(id) sender{
    _doneAnything = true;
    for (int i = 0; i<[viewArray1 count]; i++) {
        [[viewArray1 objectAtIndex:i] removeFromSuperview];
    }
    dic = [tool indexToDic];
    if ([tool isExistenceNetwork]) {
        [sender setTitle:@"交易处理中..." forState:UIControlStateNormal];
        NSLog(@"选择第 个产品,产品product id = %@",dic.dictionary_proid);
        //处理购买词库
        
        NSArray *ui2 = [self.view subviews];
        [[CBiOSStoreManager sharedInstance] initialStore];
        [[CBiOSStoreManager sharedInstance] buy:dic.dictionary_proid myArray:ui2 isBuy:1];
    }else{
        //设置提示窗口
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"网络提示" message:@"请联网后购买" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

// 恢复购买响应事件
-(void)reBuy:(id)sender{
    [sender setTitle:@"交易处理中..." forState:UIControlStateNormal];
    UserDictionary *productId = (UserDictionary*)[sender tag];
    NSArray *ui2 = [self.view subviews];
    [[CBiOSStoreManager sharedInstance] initialStore];
    [[CBiOSStoreManager sharedInstance] buy:productId.dictionary_proid myArray:ui2 isBuy:0];
}

// 购买成功回调函数
-(void)changeDicToBuyState:(NSArray *)ViewArray{
    
    // 在后台进行已购买标注
    isPurchase = true;
    NSLog(@"购买成功！");
    ServiceApi *buyDics = [[ServiceApi alloc]init];
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    dic = [tool indexToDic];
    [buyDics buyDictionary:dic UserId:user_id];
    [buyDics release];
    
    
    
    
    for (int i = 0; i < [ViewArray count]; i++) {
        NSLog(@"1--%d-%@",i,[ViewArray objectAtIndex:i]);
    }
    
    NSArray *dicArray = [[ViewArray objectAtIndex:1] subviews];
    for (int i = 0; i < [dicArray count]; i++) {
        NSLog(@"2--%d-%@",i,[dicArray objectAtIndex:i]);
    }
    NSArray *thirdArray = [[dicArray objectAtIndex:1] subviews];
    for (int i = 0; i < [thirdArray count]; i++) {
        NSLog(@"3--%d-%@",i,[thirdArray objectAtIndex:i]);
    }
    
    
    // 更新购买按钮
    UIButton * button = [thirdArray objectAtIndex:4];
    [button setHidden:YES];
//    
//    // 更新
//    UILabel * lable = [ViewArray objectAtIndex:13];
//    [lable setHidden:YES];
    
    // 显示下载按钮
    UIButton * shim = [thirdArray objectAtIndex:5];
    [shim setHidden:NO];
    
    NSString *user_name = (NSString*)[tool readObkect:@"user_name"];
    // 获得有二级结构的词库列表
    doubleStruct = [api getUserHadBuyDictionary2SencondList:user_name userId:user_id];
    [tool saveArray:doubleStruct saveKey:@"doubleStruct"];// 保存二级菜单数组
}

-(void)iphone_back_to_first:(id)sender{
    [iphoneTableDetailView removeFromSuperview];
    
    for (int i = 0; i<[viewArray2 count]; i++) {
        [[viewArray2 objectAtIndex:i] removeFromSuperview];
    }
    for (int i = 0; i<[viewArray1 count]; i++) {
        [[viewArray1 objectAtIndex:i] setHidden:NO];
    }
    [viewArray2 removeAllObjects];
    if (_doneAnything) {
        [self DataPackgeList];
    }
    
    if (view) {
        [view removeFromSuperview];
    }
}

// ================购买============end




//=================背诵============start
// 浏览词库
-(void)skimDic:(id)sender{

    UserDictionary *tagDic = [tool indexToDic];

    // 保存词库编号的标签
    NSString *dicNum = [NSString stringWithFormat:@"%d",num];
    [[NSUserDefaults standardUserDefaults] setObject:dicNum forKey:@"dicNum"];
    
    // 保存了这个词库每天要背诵的单词数量
    [[NSUserDefaults standardUserDefaults] setObject:@"one-dic" forKey:@"noe-dic"];
    
    // call method to initialize the word for today
    DateUnits *lag = [[DateUnits alloc]init];
    int day = [lag CalculateDateLag]; // 'day' is dely days
    [lag SaveLoginDate];
    Init *init = [[Init alloc]init];
    NSMutableArray *array = [init GetTodayWords];
    
    if (day >0 || [array count]==0) {
        [self Loading2];
        NSThread* myThread = [[NSThread alloc] initWithTarget:self
                                                     selector:@selector(myThreadMainMethod:)
                                                       object:nil];
        [myThread start];  // Actually create the thread
        
    }else{
        if ([newDownloadDictionaryArray count] > 0) {
            Algorithm *alg = [[Algorithm alloc]init];
            [alg UpdateDailyWordArray:newDownloadDictionaryArray];
            
            NSString *dailyplan = [[NSString alloc]initWithFormat:@"%f",[alg CountDailyPlan]];
            tools *tool = [[tools alloc]init];
            [tool saveObject:dailyplan saveKey:@"dailyplan"];
        }

        ReciteView* menuVc = [self.storyboard instantiateViewControllerWithIdentifier:@"ReciteView"];
        menuVc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;	// 淡入淡出.
        [self presentModalViewController:menuVc animated:YES]; //新视图压入到栈中
        
        // 2 获得现在还剩下多少单词
        NSMutableArray *wordsArray2 = [init GetTodayWords];
        NSMutableArray *newWordArray = [[NSMutableArray alloc]init];
        for (Word *wstr in wordsArray2) {
            if ([wstr.word_dictionary compare:tagDic.dictionary_name]==NSOrderedSame) {
                [newWordArray addObject:wstr];
            }
        }
        NSString *intString = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[newWordArray count]];
        [[NSUserDefaults standardUserDefaults] setObject:intString forKey:@"plansize"];
        
    }
}

-(void)myThreadMainMethod:(id)sender{
    Init *init = [[Init alloc]init];
    [init InitTodayWords];// get and save today's word method
    
    // 2 获得现在还剩下多少单词
    NSMutableArray *wordsArray2 = [init GetTodayWords];
    NSMutableArray *newWordArray = [[NSMutableArray alloc]init];
    dic = [tool indexToDic];
    for (Word *wstr in wordsArray2) {
        if ([wstr.word_dictionary compare:dic.dictionary_name]==NSOrderedSame) {
            [newWordArray addObject:wstr];
        }
    }
    NSString *intString = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[newWordArray count]];
    [[NSUserDefaults standardUserDefaults] setObject:intString forKey:@"plansize"];
    
    Algorithm *alg = [[Algorithm alloc]init];
    NSString *dailyplan = [[NSString alloc]initWithFormat:@"%f",[alg CountDailyPlan]];
    tools *tool = [[tools alloc]init];
    [tool saveObject:dailyplan saveKey:@"dailyplan"];
    [self finishLoading];
    
    ReciteView* menuVc = [self.storyboard instantiateViewControllerWithIdentifier:@"ReciteView"];
    menuVc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;	// 淡入淡出.
    [self presentModalViewController:menuVc animated:YES];
    
}

-(void)myThreadMainMethod2:(id)sender{
    Init *init = [[Init alloc]init];
    [init InitTodayWords];// get and save today's word method
    
    // 2 获得现在还剩下多少单词
    NSMutableArray *wordsArray2 = [init GetTodayWords];
    NSString *intString = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[wordsArray2 count]];
    [[NSUserDefaults standardUserDefaults] setObject:intString forKey:@"plansize"];
    
    Algorithm *alg = [[Algorithm alloc]init];
    NSString *dailyplan = [[NSString alloc]initWithFormat:@"%f",[alg CountDailyPlan]];
    tools *tool = [[tools alloc]init];
    [tool saveObject:dailyplan saveKey:@"dailyplan"];
    [self finishLoading];
    
    ReciteView *ickImageViewController = [[ReciteView alloc] init];
    [self presentModalViewController:ickImageViewController animated:NO];
    [ickImageViewController release];
    
}

//=================背诵============end

// 弹出框事件监听
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([alertView tag] == 1) {
        if (buttonIndex == 0) {
            
        }else if(buttonIndex == 1){
            [self Download];
        }
    }else if([alertView tag] == 2){
        
    }else if (buttonIndex == 3){
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            [iphoneTableDetailView removeFromSuperview];
            [leftBackgroundView setHidden:NO];
        }else{
            [iphoneTableDetailView removeFromSuperview];
            [iphoneBackgroundView setHidden:NO];
        }
    }else{
        if (buttonIndex == 0) {
            
        }else if (buttonIndex == 1){
        _doneAnything = true;
        for (int i = 0; i<[viewArray1 count]; i++) {
            [[viewArray1 objectAtIndex:i] removeFromSuperview];
        }
        UserDictionary *dic2 = [tool indexToDic];
        
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
        NSFileManager *fileManager = [NSFileManager defaultManager];//文件
        
        NSString *dicName = [NSString stringWithFormat:@"%@.zip",dic2.dictionary_id];
        NSString *filePath2 =  [documentsDirectory stringByAppendingPathComponent:dicName];
        BOOL isDirExist = [fileManager fileExistsAtPath:filePath2];
        
        NSString *stringInt = [NSString stringWithFormat:@"%@",dic2.dictionary_id];
        NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:stringInt];
        BOOL filedDirExist = [fileManager fileExistsAtPath:filePath];
        
        NSString *dBfilePath =  [documentsDirectory stringByAppendingPathComponent:@"db.sqlite"];
        BOOL isDbExist = [fileManager fileExistsAtPath:dBfilePath];
        
        if (filedDirExist||isDirExist) {
            Algorithm *alg = [[Algorithm alloc]init];
            [alg DeleteWordFromArray:dic2];
            
            NSString *stringInt = [NSString stringWithFormat:@"%@",dic2.dictionary_id];
            [api removeDictionary:stringInt];
            
            if (isDbExist) {
                DataBaseHelper *api2 =[[DataBaseHelper alloc]init];
                [api2 deleteDictionaryInDB:stringInt];
                
                NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
                //暂停词库
                [api2 pauseDictionary:dic2.dictionary_id userId:user_id];
                [api2 release];
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:@"0" forKey:@"dwNum"];
                [userDefaults removeObjectForKey:@"todayArray"];
                [userDefaults removeObjectForKey:@"wrongWords"];
                [userDefaults removeObjectForKey:@"now_use_dictionary"];
                [userDefaults removeObjectForKey:@"finished"];
                
            }
            
        }else{
            
        }
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                [iphoneTableDetailView removeFromSuperview];
                [leftBackgroundView setHidden:NO];
            }else{
                [iphoneTableDetailView removeFromSuperview];
                [iphoneBackgroundView setHidden:NO];
            }
//        [self DataPackgeList];
    }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


// 禁止屏幕旋转
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
//    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
//}
//- (BOOL)shouldAutorotate{
//         return NO;
//}
- (NSUInteger)supportedInterfaceOrientations{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        return UIInterfaceOrientationMaskLandscapeRight;//只支持这一个方向(正常的方向)
    }else{
        return UIInterfaceOrientationMaskPortrait;//只支持这一个方向(正常的方向)
    }
    
}

@end
