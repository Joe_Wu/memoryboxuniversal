//
//  LoginView.m
//  MemoryBoxUniversal
//
//  Created by YangJoe on 14-5-16.
//  Copyright (c) 2014年 YangJoe. All rights reserved.
//

#import "LoginView.h"
#import "ViewController.h"
#import "RegisterView.h"
#import "HomeView.h"
#import "tools.h"
#import "ServiceApi.h"
#import "DataBaseHelper.h"

@interface LoginView ()

@end

@implementation LoginView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIColor* buttonColor = [UIColor colorWithRed:222.0/255 green:59.0/255 blue:47.0/255 alpha:1.0f];
    
    self.LoginBut.backgroundColor = buttonColor;
    self.LoginBut.layer.cornerRadius = 3.0f;
    self.LoginBut.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
    [self.LoginBut setTitle:@"登陆" forState:UIControlStateNormal];
    [self.LoginBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.LoginBut setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
    
    self.RegisterBut.backgroundColor = buttonColor;
    self.RegisterBut.layer.cornerRadius = 3.0f;
    self.RegisterBut.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:18.0f];
    [self.RegisterBut setTitle:@"注册" forState:UIControlStateNormal];
    [self.RegisterBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.RegisterBut setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
    
    //对输入框进行监听
    _userName.delegate=self;
    _passWord.delegate=self;
    
    _passWord.secureTextEntry = YES; //密码
    
    // hide the virtual keyboad
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGr];
    
    [self readNSUserDefaults]; // 获得保存的用户名和密码
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Login:(id)sender {
    
    tools *tool = [[tools alloc]init];
    BOOL isExistenceNetwork = [tool isExistenceNetwork];
    if (isExistenceNetwork) {
        NSString *responseState = [[NSString alloc]initWithFormat:@"%@",[self connectToService]];
        if ([responseState isEqualToString:@"1"]) {//登陆成功
            [self clearPreUserData];// 清除上一个用户的信息
            [self saveNSUserDefaults];// 保存用户登陆信息
            
            HomeView* menuVc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeView"];
            menuVc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;	// 淡入淡出.
            [self presentModalViewController:menuVc animated:YES];
        }else if([responseState isEqualToString:@"2.2"]){//密码错误
            //设置提示窗口
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"登陆提示" message:@"密码错误" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
            [alert show];
         
        }else {//用户名不存在
            //设置提示窗口
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"登陆提示" message:@"用户名不存在" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
            [alert show];
        }
    }else{
        UIAlertView *myalert = [[UIAlertView alloc] initWithTitle:@"警告" message:@"网络不存在" delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil,nil];
    }
}

// Connect to service when login
-(NSString *)connectToService{
    NSError *error;
    NSString *useName = [[NSString alloc]initWithFormat:@"%@",_userName.text];
    NSString *passWord = [[NSString alloc]initWithFormat:@"%@",_passWord.text];
    NSString *url = [[NSString alloc]initWithFormat:@"%@userLogin/user_name/%@/user_password/%@",ApiUrl,useName,passWord];//api 登陆接口
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSDictionary *weatherDic = [NSJSONSerialization JSONObjectWithData:response  options:NSJSONReadingMutableLeaves error:&error];//Json字典
    return [weatherDic objectForKey:@"state"];//解析出返回的状态参数
}
// remove all data when change user
-(void)clearPreUserData{
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    NSString *userName = [userDefaultes stringForKey:@"userName"];
    
    // 如果是新的用户，就清除历史信息。
    if ([userName compare:_userName.text]!=NSOrderedSame) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults removeObjectForKey:@"todayArray"];
        [userDefaults removeObjectForKey:@"GoOverWord"];
        [userDefaults removeObjectForKey:@"doubleStruct"];
        [userDefaults removeObjectForKey:@"user_name"];
        [userDefaults removeObjectForKey:@"user_id"];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"newUser"];
        tools *tool = [[tools alloc]init];
        [tool deleteAllData];
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"newUser"];
    }
}

// save user info to NSUserDefaults
-(void)saveNSUserDefaults {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:_userName.text forKey:@"userName"];
    [userDefaults setObject:_passWord.text forKey:@"passWord"];
}

// Read user info from NSUserDefaults
-(void)readNSUserDefaults {
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    NSString *userName = [userDefaultes stringForKey:@"userName"];
    NSString *passWord = [userDefaultes stringForKey:@"passWord"];
    _userName.text = userName;
    _passWord.text = passWord;
}

-(void)viewTapped:(UITapGestureRecognizer*)tapGr
{
    [_userName resignFirstResponder];
    [_passWord resignFirstResponder];
}

// change the location when inputing
- (void)moveView:(UITextField *)textField leaveView:(BOOL)leave{
    float screenHeight,keyboardHeight;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        screenHeight = 1496; //屏幕尺寸，如果屏幕允许旋转，可根据旋转动态调整
        keyboardHeight = 748; //键盘尺寸，如果屏幕允许旋转，可根据旋转动态调整
    }else{
        screenHeight = 1136; //屏幕尺寸，如果屏幕允许旋转，可根据旋转动态调整
        keyboardHeight = 430; //键盘尺寸，如果屏幕允许旋转，可根据旋转动态调整
    }
    
    float statusBarHeight,NavBarHeight,tableCellHeight,textFieldOriginY,textFieldFromButtomHeigth;
    int margin;
    statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height; //屏幕状态栏高度
    NavBarHeight = self.navigationController.navigationBar.frame.size.height; //获取导航栏高度
    
    UITableViewCell *tableViewCell=(UITableViewCell *)textField.superview;
    tableCellHeight = tableViewCell.frame.size.height; //获取单元格高度
    
    CGRect fieldFrame=[self.view convertRect:textField.frame fromView:tableViewCell];
    textFieldOriginY = fieldFrame.origin.y; //获取文本框相对本视图的y轴位置。
    
    //计算文本框到屏幕底部的高度（屏幕高度-顶部状态栏高度-导航栏高度-文本框的的相对y轴位置-单元格高度）
    textFieldFromButtomHeigth = screenHeight - statusBarHeight - NavBarHeight - textFieldOriginY - tableCellHeight;
    
    if(!leave) {
        if(textFieldFromButtomHeigth < keyboardHeight) { //如果文本框到屏幕底部的高度 < 键盘高度
            margin = keyboardHeight - textFieldFromButtomHeigth; // 则计算差距
            keyBoardMargin_ = margin; //keyBoardMargin_ 为成员变量，记录上一次移动的间距,用户离开文本时恢复视图高度
        } else {
            margin= 0;
            keyBoardMargin_ = 0;
        }
    }
    
    float movementDuration = 0.3f; // 动画时间
    
    int movement;
    UIInterfaceOrientation deviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (deviceOrientation == 0) {
        movement = (leave ? keyBoardMargin_ : -margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }else{
        movement = (leave ? -keyBoardMargin_ : margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }
    
    [UIView beginAnimations: @"textFieldAnim" context: nil]; //添加动画
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        self.view.frame = CGRectOffset(self.view.frame, movement/8, 0);
    }else{
        self.view.frame = CGRectOffset(self.view.frame, 0, -movement/2);
    }
    [UIView commitAnimations];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self moveView:textField leaveView:NO];
}

- (void)textFieldDidEndEditing:(UITextField *)textField;{
    [self moveView:textField leaveView:YES];
}


 //禁止屏幕旋转
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        return (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft);
    }else{
        return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
    }
    
}
//- (BOOL)shouldAutorotate{
//    return NO;
//}
//- (NSUInteger)supportedInterfaceOrientations{
//    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
//        return UIInterfaceOrientationMaskLandscapeLeft;//只支持这一个方向(正常的方向)
//    }else{
//        return UIInterfaceOrientationMaskPortrait;//只支持这一个方向(正常的方向)
//    }
//}

@end
